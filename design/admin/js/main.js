$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});
function imageIsLoaded(e) {
    $('#myImg').attr('src', e.target.result);
};
// $(function () {
//     $(":file").change(function () {
//         if (this.files && this.files[0]) {
//             var reader = new FileReader();
//             reader.onload = imageIsLoaded;
//             reader.readAsDataURL(this.files[0]);
//         }
//     });
// });
// function imageIsLoaded(e) {
//     $('#myImg1').attr('src', e.target.result);
// };
// pichart
$(document).ready(function() {                    
    var chart = new CanvasJS.Chart("chartContainer",
    {
        title:{
            // text: "How my time is spent in a week?",
            // fontFamily: "arial black"
        },
        animationEnabled: true,
        legend: {
            verticalAlign: "bottom",
            horizontalAlign: "center"
        },
        theme: "theme1",
        data: [
        {        
            type: "pie",
            indexLabelFontFamily: "Garamond",       
            indexLabelFontSize: 20,
            indexLabelFontWeight: "bold",
            startAngle:0,
            indexLabelFontColor: "MistyRose",       
            indexLabelLineColor: "darkgrey", 
            indexLabelPlacement: "inside", 
            toolTipContent: "{name}: {y}hrs",
            showInLegend: true,
            indexLabel: "#percent%", 
            dataPoints: [
            {  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
            {  y: 44, name: "Time At Home", legendMarkerType: "square"},
            {  y: 12, name: "Time Spent Out", legendMarkerType: "circle"}
            ]
        }
        ]
    });
chart.render();
 // bardigram
 var chart = new CanvasJS.Chart("chartContainer1",
 {
    theme: "theme3",
    animationEnabled: true,
    title:{
        // text: "Crude Oil Reserves Vs Production, 2011",
        // fontSize: 30
    },
    toolTip: {
        shared: true
    },          
    axisY: {
        title: "billion of barrels"
    },
    axisY2: {
        title: "million barrels/day"
    },          
    data: [ 
    {
        type: "column", 
        name: "Proven Oil Reserves (bn)",
        legendText: "Proven Oil Reserves",
        showInLegend: true, 
        dataPoints:[
        {label: "Saudi", y: 262},
        {label: "Venezuela", y: 211},
        {label: "Canada", y: 175},
        {label: "Iran", y: 137},
        {label: "Iraq", y: 115},
        {label: "Kuwait", y: 104},
        {label: "UAE", y: 97.8},
        {label: "Russia", y: 60},
        {label: "US", y: 23.3},
        {label: "China", y: 20.4}


        ]
    },
    {
        type: "column", 
        name: "Oil Production (million/day)",
        legendText: "Oil Production",
        axisYType: "secondary",
        showInLegend: true,
        dataPoints:[
        {label: "Saudi", y: 11.15},
        {label: "Venezuela", y: 2.5},
        {label: "Canada", y: 3.6},
        {label: "Iran", y: 4.2},
        {label: "Iraq", y: 2.6},
        {label: "Kuwait", y: 2.7},
        {label: "UAE", y: 3.1},
        {label: "Russia", y: 10.23},
        {label: "US", y: 10.3},
        {label: "China", y: 4.3}


        ]
    }
    
    ],
    legend:{
        cursor:"pointer",
        itemclick: function(e){
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }
},
});

chart.render();
/* Toggling header menus */
$("#welcome").click(function () {
    $("#welcome-menu").slideToggle("fast");
    $(this).toggleClass("activated-welcome");
    return false;
});

$("#help").click(function () {
    $("#help-menu").slideToggle("fast");
    $(this).toggleClass("activated-help");
    return false;
});
// $('.panelTrigger').outside('click', function() {
//     $('.panelContainer').stop(true, true).slideUp('fast');
// });                

                /* 
                 * Button hovering effects 
                 * Note: we are not using pure css using :hover because :hover applies to even disabled elements.
                 * The pseudo class :enabled is not supported in IE < 9.
                 */                
                 $(document).on({
                    mouseenter: function () {
                        $(this).addClass('hover');                        
                    },
                    mouseleave: function () {
                        $(this).removeClass('hover');                        
                    }

                }, 'input[type=button], input[type=submit], input[type=reset]'); 

                 /* Fading out main messages */
                 $(document).on({
                    click: function() {
                        $(this).parent('div.message').fadeOut("slow");
                    }
                }, '.message a.messageCloseButton');                

                 /* Toggling search form: Begins */
                //$(".toggableForm .inner").hide(); // Disabling this makes search forms to be expanded by default.

                $(".toggableForm .toggle").click(function () {
                    $(".toggableForm .inner").slideToggle('slow', function() {
                        if($(this).is(':hidden')) {
                            $('.toggableForm .tiptip').tipTip({content:'Expand for Options'});
                        } else {
                            $('.toggableForm .tiptip').tipTip({content:'Hide Options'});
                        }
                    });
                    $(this).toggleClass("activated");
                });
                /* Toggling search form: Ends */

                /* Enabling/disabling form fields: Begin */
                
                $('form.clickToEditForm input, form.clickToEditForm select, form.clickToEditForm textarea').attr('disabled', 'disabled');
                // $('form.clickToEditForm input.calendar').datepicker('disable');
                $('form.clickToEditForm input[type=button]').removeAttr('disabled');
                
                $('form input.editButton').click(function(){
                    $('form.clickToEditForm input, form.clickToEditForm select, form.clickToEditForm textarea').removeAttr('disabled');
                    $('form.clickToEditForm input.calendar').datepicker('enable');
                });
                
                /* Enabling/disabling form fields: End */
                var chart = new CanvasJS.Chart("chartContainer",
                {
                    // title:{
                    //     text: "How my time is spent in a week?",
                    //     fontFamily: "arial black"
                    // },
                    animationEnabled: true,
                    legend: {
                        verticalAlign: "bottom",
                        horizontalAlign: "center"
                    },
                    theme: "theme1",
                    data: [
                    {        
                        type: "pie",
                        indexLabelFontFamily: "Garamond",       
                        indexLabelFontSize: 20,
                        indexLabelFontWeight: "bold",
                        startAngle:0,
                        indexLabelFontColor: "MistyRose",       
                        indexLabelLineColor: "darkgrey", 
                        indexLabelPlacement: "inside", 
                        toolTipContent: "{name}: {y}hrs",
                        showInLegend: true,
                        indexLabel: "#percent%", 
                        dataPoints: [
                        {  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
                        {  y: 44, name: "Time At Home", legendMarkerType: "square"},
                        {  y: 12, name: "Time Spent Out", legendMarkerType: "circle"}
                        ]
                    }
                    ]
                });
chart.render();
$("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
});

});
// end of navbar
// quicklinks
// end of quicklinks

