<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Up_pagination
 *
 * @author prabin
 */
class Up_pagination extends MX_Controller {

//put your code here
    function __construct() {
        parent::__construct();
        $this->load->library('pagination');
    }

    function set_pagination_config($count, $module_url) {
        $config['total_rows'] = $count;
        $config['base_url'] = $module_url;
        $config['first_url'] = $module_url . '/1';
        $config['num_links'] = 1;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '<li><a href="javascript:void(0);"><strong>';
        $config['cur_tag_close'] = '</strong></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&gt;&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        return $config;
    }

}
