<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common_functions
 *
 * @author prabin
 */
class Common_functions extends MX_Controller {
//protected $MODULE = null;
    public function __construct() {
        parent::__construct();
        $this->load->model('modules/Mdl_moduleslist');
    }

    function check_permission($group_id,$module_name) {
        if ($group_id != 1) {
            $roles = $this->Mdl_permissions->unserialize_role_array($group_id);
            $roles_combined = implode(" ", $roles);

            $module_id = $this->Mdl_moduleslist->get_id_from_modulename($module_name);
            if (strpos($roles_combined, 'e' . $module_id)) {
                $permission['edit'] = TRUE;
            }
            if (strpos($roles_combined, 'a' . $module_id)) {
                $permission['add'] = TRUE;
            }
            if (strpos($roles_combined, 'v' . $module_id)) {
                $permission['view'] = TRUE;
            }
            if (strpos($roles_combined, 'd' . $module_id)) {
                $permission['delete'] = TRUE;
            }
            return $permission;
        } else {
            $permission['edit'] = TRUE;
            $permission['add'] = TRUE;
            $permission['delete'] = TRUE;
            $permission['view'] = TRUE;
            return $permission;
        }
    }

    function get_data_from_db($update_id, $select, $model) {
        $query = $this->$model->get_where_dynamic($update_id, $select);
//        print_r($query->result_array());die;
        foreach ($query->result() as $key => $value) {
            foreach ($value as $k => $v) {
                $data[$k] = $v;
            }
        }
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }

    function get_dropdown($order_by, $select, $params, $model) {
        $dropdowns = $this->$model->get_where_dynamic('', $select, $order_by, '', '', $params, '')->result();
        if ($model == 'Mdl_navigation') {
            $dropdownlist[0] = ''; //for making parent value null as default
        }
        if ($model == 'mdl_groups') {
            $dropdownlist[0] = 'select group';
        }
        $s = explode(',', $select);
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->$s[0]] = $dropdown->$s[1];
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function delete_attachment($id, $model, $module) {
        $query = $this->$model->get_where_dynamic($id);
        foreach ($query->result() as $key => $value) {
            foreach ($value as $k => $v) {
                $data[$k] = $v;
            }
        }
        if ($data['attachment'] != NULL) {
            unlink($_SERVER['DOCUMENT_ROOT'] . '/upvedacms_v3/uploads/' . $module . '/' . $data['attachment']);
            $message = ' Attachment was also deleted successfully!!!';
        } else {
            $message = ' There was not any attachment for this module!!';
        }
        return $message;
    }

//    function do_upload($id, $data,$module) {
//        $config['upload_path'] = "./uploads/" . $module . "/";
//        $config['file_name'] = md5($id);
//        $config['overwrite'] = TRUE;
//        $config['allowed_types'] = "gif|jpg|jpeg|png";
//        $config['max_size'] = "20480"; //that's 20MB
//        $config['max_width'] = "1907";
//        $config['max_height'] = "1280";
//
//        $this->load->library('upload', $config);
////        print_r($config);die;
//
//        if (!$this->upload->do_upload()) {
//            echo 'File cannot be uploaded';
//            $datas = array('error' => $this->upload->display_errors());
//        } else {
//            echo 'File has been uploaded';
//            $datas = array('upload_data' => $this->upload->data());
//        }
//
//        return $datas;
//    }
     function do_upload($id, $data_lang, $module) {
        $this->load->library('upload');

        $files = $_FILES;
        
       
//        echo '<pre>',print_r($data_lang),'</pre>';
//        echo '<pre>',print_r($files),'</pre>';die;
        $i=0;
        foreach ($data_lang as $row) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i ];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i ];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i ];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i ];

            $this->upload->initialize($this->set_upload_options($id, $row));
            $this->upload->do_upload();
            
            $temp = $this->upload->data();
            $ext = $temp['file_ext'];
            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */            
            
            //load the library
//            $this->load->library('image_lib',$config);
//            // Set your config up
//            $this->image_lib->initialize($this->get_config_120X120($id, $value, $ext));            
//            if ( ! $this->image_lib->resize())
//            {
//                echo $this->image_lib->display_errors();
//            }
//            // Clear the config
//            $this->image_lib->clear();
//            var_dump($ext)
//            
//            
//            //load the library
//            $this->load->library('image_lib',$config);
//            // Set your config up
//            $this->image_lib->initialize($this->get_config_74X74($id, $value, $ext));            
//            if ( ! $this->image_lib->resize())
//            {
//                echo $this->image_lib->display_errors();
//            }
//            // Clear the config
//            $this->image_lib->clear();
//            
//            
////            //load the library
////            $this->load->library('image_lib');
////            // Set your config up
////            $this->image_lib->initialize($this->get_config_370X370($id, $value, $ext));            
////            if ( ! $this->image_lib->resize())
////            {
////                echo $this->image_lib->display_errors();
////            }
////            // Clear the config
////            $this->image_lib->clear();
//            
//            /* CODE TO RESIZE AND CREATE COPY OF THE IMAGE */
//            
            $datas[$i] = array('upload_data' => $this->upload->data());
            $i++;
        }
        return $datas;
    }
     private function set_upload_options($id, $value) {
//  upload an image options
        $config = array();
        $config['upload_path'] = './uploads/document/';
        //$config['allowed_types'] = 'gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '20480';
        $config['overwrite'] = FALSE;
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['file_name'] = $id . '_' . $value;

        return $config;
    }
     function get_config_120X120($id, $value, $ext){
//            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/pages/'.$id . '_' . $value.$ext;
            $config['create_thumb'] = FALSE;
            $config['new_image'] = './uploads/pages_120X120/'.$id . '_' . $value.$ext;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 120;
            $config['height'] = 120;
            return $config;
    }
    function get_config_74X74($id, $value, $ext){
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/pages/'.$id . '_' . $value.$ext;
            $config['create_thumb'] = FALSE;
            $config['new_image'] = './uploads/pages_74X74/'.$id . '_' . $value.$ext;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 74;
            $config['height'] = 74;
            return $config;
    }
    
    function get_config_370X370($id, $value, $ext){
            $config['image_library'] = 'gd2';
            $config['source_image'] = './uploads/pages/'.$id . '_' . $value.$ext;
            $config['create_thumb'] = FALSE;
            $config['new_image'] = './uploads/pages_370X370/'.$id . '_' . $value.$ext;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 370;
            $config['height'] = 370;
            return $config;
    }

}
