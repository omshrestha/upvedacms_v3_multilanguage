<div class="bs-docs-section">
    <div class="bs-glyphicons">
        <ul class="bs-glyphicons-list">
            <a href="<?php echo base_url();?>admin/pages">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Pages</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/sample">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Sample</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/sample_attachment">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Sample Attachment</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/category">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Category</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/document_type">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Document Type</span>
            </li>
            </a>
            <a href="<?php echo base_url();?>admin/document">
            <li>
                <span class="glyphicon glyphicon-plus"></span>
                <span class="glyphicon-class">Document</span>
            </li>
            </a>
            <div class="clearfix"></div>
        </ul>
    </div>
</div>