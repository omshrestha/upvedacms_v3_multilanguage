<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_dynamic extends Mdl_crud {

    protected $_table = 'up_dynamic';
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function _insert($data) {
        $table = $this->_table;
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();
        return $id;
    }

    function _update($id, $data) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->update($table, $data);
        return $id;
    }

    function _delete($id) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($table);
        return $id;
    }

    function get_front($order_by) {
        $table = $this->_table;
        $this->db->select('up_dynamic.*');
        $this->db->select('up_users_groups.name as group_name');
        $this->db->join('up_users_groups', 'up_users_groups.id = up_dynamic.address');
        $this->db->order_by($order_by);
        $query = $this->db->get($table)->result_array();
        return $query;
    }

    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_dynamic'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment'];
        return $nextId;
    }

    function get_dynamic() {
        $table = $this->_table;
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }

    function get_dynamic_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('dynamic')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get($order_by) {
        $table = $this->_table;
        $this->db->order_by($order_by, 'desc');
        $query = $this->db->get($table)->result_array();
        return $query;
    }

    function get_with_limit($limit, $offset, $order_by) {
        $table = $this->_table;
        $this->db->limit($limit, $offset);
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where($id) {
        $table = $this->_table;
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_dynamicdetail($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_custom($col, $value) {
        $table = $this->_table;
        $this->db->where($col, $value);
        $query = $this->db->get($table);
        return $query;
    }

    function count_where($column, $value) {
        $table = $this->_table;
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function count_all() {
        $table = $this->_table;
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

}
