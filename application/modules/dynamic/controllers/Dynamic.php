<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic extends MX_Controller
{
        function __construct() {
	parent::__construct();
	} 
        function index()
	{	
            $data['query'] = $this->get('id');
            $data['view_file']="front";
            $this->load->module('template');
            $this->template->front($data);
	}
        function details()
	{ 
                $slug = $this->uri->segment(3);
                $query=$this->get_boddetails($slug);
                foreach ($query->result() as $row) 
                {
                  $data['name'] = $row->name;
                  $data['attachment'] = $row->attachment;
                  $data['email'] = $row->email;
                  $data['phone_no'] = $row->phone_no;
                  $data['gender'] = $row->gender;
                  $data['description'] = $row->description;
                }
                
                $data['view_file']="details";
                $this->load->module('template');
                $this->template->front($data);
	}
        function get($order_by)
        {
            $this->load->model('mdl_bod');
            $query = $this->mdl_bod->get_front($order_by);
            return $query;
	}
        function get_boddetails($slug){
	$this->load->model('mdl_bod');
	$query = $this->mdl_bod->get_boddetail($slug);
	return $query;
	}
}

	
        
