<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/dynamic";
    private $MODULE = "dynamic";
    private $group_id;
    private $model_name = 'mdl_dynamic';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('mdl_dynamic');
        $this->load->model('groups/mdl_groups');
        $this->load->model('settings/Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('dynamic'); //module name is dynamic here	
    }

    function index() {
        $main_table_params = 'id,fname,lname,tech,email,address,status';

        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
            $count = $this->mdl_dynamic->count($params);
        }
         if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
//        print_r($params);
//        $count = $this->Mdl_banner->count();
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['result'] = $this->mdl_dynamic->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['group_array'] = json_encode($this->common_functions->get_dropdown('id', 'id,name', '', 'mdl_groups'));
        $data['group_array_2'] = $this->common_functions->get_dropdown('id', 'id,name', '', 'mdl_groups');
        $data['permission'] = $this->common_functions->check_permission($this->group_id);

        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
//         $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function action() {
        if (isset($_GET) && count($_GET)) {
//            var_dump($_GET);die;
            // whats the action ??
            $action = $_GET['action'];
            unset($_GET['action']);

            if ($action == "save") {
                $escapedPost = $this->get_data_from_post();
//                var_dump($data);die;
                $res = $this->_insert($escapedPost);
//			$res = $obj->save($escapedPost);

                if ($res) {
                    $escapedPost["success"] = "1";
                    $escapedPost["id"] = $res;
                    echo json_encode($escapedPost);
                } else
                    echo json_encode(array("success" => "0", "action" => "save"));
            }else if ($action == "del") {
                $id = $_GET['rid'];
                $res = $this->_delete($id);
                if ($res)
                    echo json_encode(array("success" => "1", "id" => $id));
                else
                    echo json_encode(array("success" => "0", "action" => "delete"));
            }
            else if ($action == "update") {
                $id = $_GET['rid'];
                $escapedPost = $this->get_data_from_post();
                unset($escapedPost['rid']);
//                $attach = $this->get_attachment_from_db($id);
//                $uploadattachment = $this->do_upload($id);
//                $data['attachment'] = $uploadattachment['upload_data']['file_name'];
//                if (empty($data['attachment'])) {
//                    $data['attachment'] = $attach['attachment'];
//                }
                $id = $this->_update($id, $escapedPost);
                if ($id)
                    echo json_encode(array_merge(array("success" => "1", "id" => $id), $escapedPost));
                else
                    echo json_encode(array("success" => "0", "action" => "update"));
            }
            else if ($action == "updatetd") {
                $id = $_GET['rid'];
                $escapedPost = $this->get_data_from_post();
                unset($escapedPost['rid']);
                $id = $this->_update($id, $escapedPost);
                if ($id)
                    echo json_encode(array_merge(array("success" => "1", "id" => $id), $escapedPost));
                else
                    echo json_encode(array("success" => "0", "action" => "updatetd"));
            }
        }
    }

    function get_data_from_post() {
        $data = $this->input->get();

//        $data['fname'] = $this->input->get('fname', TRUE);
//        $data['address'] = $this->input->get('address', TRUE);
//        $data['lname'] = $this->input->get('lname', TRUE);
//        $data['email'] = $this->input->get('email', TRUE);
//        $data['tech'] = $this->input->get('tech', TRUE);
//        $data['status'] = $this->input->get('status', TRUE);

        return $data;
    }

//    function get_data_from_db($update_id) {
//        $query = $this->get_where($update_id);
//        foreach ($query->result() as $row) {
//            $data['name'] = $row->name;
//            $data['address'] = $row->address;
//            $data['phone_no'] = $row->phone_no;
//            $data['email'] = $row->email;
//            $data['attachment'] = $row->attachment;
//            $data['gender'] = $row->gender;
//            $data['description'] = $row->description;
//            $data['status'] = $row->status;
//        }
//
//        if (!isset($data)) {
//            $data = "";
//        }
//        return $data;
//    }
//    function get_attachment_from_db($update_id) {
//        $query = $this->get_where($update_id);
//        foreach ($query->result() as $row) {
//            $data['attachment'] = $row->attachment;
//        }
//        return $data;
//    }

    function create() {
        $update_id = $this->uri->segment(4);
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'name,address,phone_no,email,gender,description,status';
                $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model);
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }

        $data['update_id'] = $update_id;

        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $delete_id = $this->uri->segment(4);

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/dynamic');
        } else {
            $this->mdl_dynamic->_delete($delete_id);
            redirect('admin/dynamic');
        }
    }

    function submit() {
        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
            $this->form_validation->set_rules('qualification', 'Qualification', 'required|xss_clean');
            $this->form_validation->set_rules('position', 'Position', 'required|xss_clean');
            $this->form_validation->set_rules('institute', 'Institute', 'required|xss_clean');
            $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_dynamic.name]'); //unique_validation check while creating new
            $this->form_validation->set_rules('qualification', 'Qualification', 'required|xss_clean');
            $this->form_validation->set_rules('position', 'Position', 'required|xss_clean');
            $this->form_validation->set_rules('institute', 'Institute', 'required|xss_clean');
            $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
        }
        /* end of validation rule */

        $data = $this->get_data_from_post();

        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

//            $attach = $this->get_attachment_from_db($update_id);
//            $uploadattachment = $this->do_upload($update_id);
//            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
//            if (empty($data['attachment'])) {
//                $data['attachment'] = $attach['attachment'];
//            }

            $this->_update($update_id, $data);
        } else {

//            $nextid = $this->get_id();
//            $uploadattachment = $this->do_upload($nextid);
//            $data['attachment'] = $uploadattachment['upload_data']['file_name'];

            $this->_insert($data);
        }

        redirect('admin/dynamic');
    }

//    function do_upload($id) {
//        $config['upload_path'] = "./uploads/dynamic/";
//        $config['file_name'] = $id;
//        $config['overwrite'] = TRUE;
//        $config['allowed_types'] = "gif|jpg|jpeg|png";
//        $config['max_size'] = "20480"; //that's 20MB
//        $config['max_width'] = "1907";
//        $config['max_height'] = "1280";
//
//        $this->load->library('upload', $config);
//
//
//        if (!$this->upload->do_upload()) {
//            //echo 'File cannot be uploaded';
//            $datas = array('error' => $this->upload->display_errors());
//        } else {
//            echo 'File has been uploaded';
//            $datas = array('upload_data' => $this->upload->data());
//        }
//
//        return $datas;
//    }

    function get_id() {
        $id = $this->mdl_dynamic->get_id();
        return $id;
    }

    function get($order_by) {
        $query = $this->mdl_dynamic->get($order_by);
        return $query;
    }

    function get_where($id) {
        $query = $this->mdl_dynamic->get_where($id);
        return $query;
    }

    function _insert($data) {
        $id = $this->mdl_dynamic->_insert($data);
        return $id;
    }

    function _update($id, $data) {
        $id = $this->mdl_dynamic->_update($id, $data);
        return $id;
    }

    function _delete($id) {
        $id = $this->mdl_dynamic->_delete($id);
        return $id;
    }

}
