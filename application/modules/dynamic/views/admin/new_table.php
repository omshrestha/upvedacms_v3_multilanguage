<!--/**for table sort feature**/-->

<script>
    var base_url = '<?php echo base_url() ?>' + 'design/admin/dynamic/';
    // Column names must be identical to the actual column names in the database, if you dont want to reveal the column names, you can map them with the different names at the server side.
    var columns = new Array("fname", "lname", "tech", "email", "address", "status");
    var placeholder = new Array("Enter First Name", "Enter Last Name", "Enter Technology", "Enter Email", "", "");
    var inputType = new Array("text", "text", "text", "text", "select_key", "select");
    var table = "tableDemo";
    var data = JSON.parse('<?php echo $group_array; ?>');
//         var selectOpt = [];
//            for (var group_array in data) {
//                selectOpt.push(data[group_array]);
//            }
    var selectOpt = new Array("Live", "Draft");
    var selectOpt1 = new Array("test1", "test2");


    // Set button class names 
    var savebutton = "ajaxSave";
    var deletebutton = "ajaxDelete";
    var editbutton = "ajaxEdit";
    var updatebutton = "ajaxUpdate";
    var cancelbutton = "cancel";

    var saveImage = base_url + "images/save.png"
    var editImage = base_url + "images/edit.png"
    var deleteImage = base_url + "images/remove.png"
    var cancelImage = base_url + "images/back.png"
    var updateImage = base_url + "images/save.png"

    // Set highlight animation delay (higher the value longer will be the animation)
    var saveAnimationDelay = 3000;
    var deleteAnimationDelay = 1000;

    // 2 effects available available 1) slide 2) flash
    var effect = "flash";

</script>

<div class="widget box" id="replaceTable"> 

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i>Dynamic</h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content" style="overflow-x:auto;"> 
        <table id="myTable" class="table table-responsive datatable tableDemo">
            <!--<thead>-->
            <tr class="ajaxTitle"> 
                    <!--<th data-class="check-box">Sn</th>-->
                <th data-class="check-box">First Name</th>
                <th data-class="check-box">Last Name</th>
                <th data-class="check-box">Technology</th>
                <th data-class="check-box">Email</th>
                <th data-class="check-box">Address</th>
                <th data-class="expand">Status</th>
<!--                   <th data-class="expand">Address2</th>-->
                <th data-class="check-box">Action</th>
            </tr>
            <!--</thead>-->

            <?php
            $records = $result;
            if (count($records->result())) {
                $i = 1;
                $eachRecord = 0;
                foreach ($records->result() as $res) {
                    ?>
                    <tr id="<?= $res->id; ?>">
                            <!--<td><?= $i++; ?></td>-->
                        <td class="fname"><?= $res->fname; ?></td>
                        <td class="lname"><?= $res->lname; ?></td>
                        <td class="tech"><?= $res->tech; ?></td>
                        <td class="email"><?= $res->email; ?></td>
                        <td class="address"><?= $res->address; ?></td>
                        <td class="status"><?= $res->status; ?></td>
        <!--                        <td class="address2"><?= $eachRecord['address2']; ?></td>-->
                        <td>
                            <a href="javascript:;" id="<?= $res->id; ?>" class="ajaxEdit"><img src="" class="eimage"></a>
                            <a href="javascript:;" id="<?= $res->id; ?>" class="ajaxDelete"><img src="" class="dimage"></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>               

        </table>
        <div id="pagination">
            <div class="col-sm-2">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="col-sm-6">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>

</div><!--end of class="widget box"-->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-latest.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery.tablesorter.js"></script> -->
<!--dynamic module js-->
<script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-1.11.0.min.js"></script>	
<script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-ui.js"></script>	
<script src="<?php echo base_url(); ?>design/admin/dynamic/js/script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/s<earchPagination.js"></script>
<script>
    $(document).ready(function ()
    {

        $("#myTable").tablesorter({sortList: [[0, 0], [1, 0]]});
    }
    );
</script>