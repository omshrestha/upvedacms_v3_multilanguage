-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2016 at 12:02 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rerlfinal`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_dynamic`
--

CREATE TABLE IF NOT EXISTS `up_dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `tech` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `test` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `status` enum('Live','Draft') NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `up_dynamic`
--

INSERT INTO `up_dynamic` (`id`, `fname`, `lname`, `tech`, `email`, `address`, `test`, `address2`, `attachment`, `status`) VALUES
(1, 'Jeena', 'Budachhetri', 'Developer', 'Jeena@jena.com', '2', '', '', '', 'Live'),
(2, 'hg', 'jgjhgh', 'gjhg', 'ghjgjh', '1', '', '', '', 'Draft'),
(3, 'asa', 'asa', 'assas', 'dsf@dffd.dfd', '1', '', '', '', 'Draft'),
(4, 'zxz', 'zxz', 'xzxz', 'sds', '2', '', '', '', 'Live'),
(5, 'hkj', 'hkjh', 'kj', 'kjh', '1', '', '', '', 'Live'),
(6, 'nbn', 'bnb', 'nb', 'nnb', '1', '', '', '', 'Live'),
(7, 'jjh', 'j', 'hjh', 'jh', '2', '', '', '', 'Draft'),
(8, 'kjhkjh', 'hk', 'hjkh', 'jkh', '1', '', '', '', 'Live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
