<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_crud extends CI_Model {

    protected $_table = null;
    protected $_primary_key = null;

    function __construct() {
        parent::__construct();
    }

    function get_where_dynamic($id = NULL, $main_table_params = NULL, $order_by = NULL, $start = NULL, $limit = NULL, $params = NULL, $join_params = NULL) {
        if ($params != NULL) {
            foreach ($params as $key => $value) {
                $each_param[$key] = explode("=>", $value);
            }
            foreach ($each_param as $key => $value) {
                if ($value[1] != '' || $value[1] != 0) {
                    if ($value[0] == "varchar") {
                        $this->db->like($key, $value[1]);
                    } else if ($value[0] == "integer" && $value[1] != 0) {
                        $this->db->where($key, $value[1]);
                    }
                }
            }
        }
        if ($main_table_params != NULL) {
            $select = explode(',', $main_table_params);
            foreach ($select as $s) {
                $this->db->select($this->_table . '.' . $s);
            }
        }
        if ($join_params != NULL) {
            $this->db->select($join_params['select_params']);
            $join_table = explode(',', $join_params['join_table']);
            $join_condition = explode(',', $join_params['join_condition']);
            for ($i = 0; $i < count($join_condition); $i++) {
                $this->db->join($join_table[$i], $join_condition[$i],'inner join');
            }
        }
        if ($order_by != NULL) {
            $this->db->order_by($order_by, 'ASC');
        }
        if ($limit != NULL || $start != NULL) {
            $this->db->limit($limit, $start);
        }
        if ($id != NULL) {
            $this->db->where($this->_primary_key, $id);
        }
        $query = $this->db->get($this->_table);
        return $query;
    }

    function count($params = NULL) {
        $table = $this->_table;
        if ($params != NULL) {
            foreach ($params as $key => $value) {
                $each_param[$key] = explode("=>", $value);
            }
            foreach ($each_param as $key => $value) {
                if ($value[1] != '' || $value[1] != 0) {
                    if ($value[0] == "varchar") {
                        $this->db->like($key, $value[1]);
                    } else if ($value[0] == "integer" && $value[1] != 0) {
                        $this->db->where($key, $value[1]);
                    }
                }
            }
        }
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function _insert($data) {
        $table = $this->_table;
        $this->db->insert($table, $data);
    }

    function _update($id, $data) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->update($table, $data);
    }

    function _delete($id) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($table);
    }

    function get_max() {
        $table = $this->_table;
        $this->db->select_max($this->_primary_key);
        $query = $this->db->get($table);
        $row = $query->row();
        $id = $row->id;
        return $id;
    }

    function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }

    function get_page_live() {
        $table = $this->_table;
        $this->db->where('status', 'live');
        $query = $this->db->get($table);
        return $query;
    }

}
