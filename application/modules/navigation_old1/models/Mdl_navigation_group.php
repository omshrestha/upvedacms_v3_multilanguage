<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_navigation_group extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}

	
	function get_table_group() {
	$table = "up_navigation_group";
	return $table;
	}
	
	function get_group($order_by){
	$table = $this->get_table_group();
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_group_title($group_id){//to get navigation title of specific group
	$table = $this->get_table_group();
	$this->db->select('title');
	$this->db->where('id', $group_id);
	$query=$this->db->get($table);
	$title_arr = $query->result_array();
	if(empty($title_arr)){return NULL;}
	return $title_arr[0]['title'];
	}
	
	function check_group_existence($id){
	$table = $this->get_table_group();
	$this->db->where('id', $id);
	$query=$this->db->get($table);	
	$arr = $query->result_array();
	if(empty($arr)){return FALSE;}
	else{return TRUE;}
	}
	
	function get_where_group($id){
	$table = $this->get_table_group();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert_group($data){
	$table = $this->get_table_group();
	$this->db->insert($table, $data);
	}
	
	function _update_group($id, $data){
	$table = $this->get_table_group();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete_group($id){
	$table = $this->get_table_group();
	$this->db->where('id', $id);
	$this->db->delete($table);
	
	$this->db->where('group_id', $id);		//deleting all the navigation of the 
	$this->db->delete('up_navigation');		//particular navigation group.
	}

}