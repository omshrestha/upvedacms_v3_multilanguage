<?php // die('kjhjgjjyftyfd');  ?>
<script type="text/javascript">
    function ajaxFunctionName() {
        // alert("change"); 
        $('#lang').click(function(event) {
            var slug = event.target.id;
            // alert(slug);
            // var slug = event.target.id;  
            //  alert(slug);
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>pages',
                data: "slug=" + slug,
                success: function()
                {
                    //alert(result);
                    window.location.reload(true);
                }

            });
        });
    }
</script>
<html class="no-js">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php
        if (isset($title)) {
            ?>
            <title><?php echo $title; ?></title>
            <meta name="title" content="<?php echo $title; ?>">
            <meta name="keywords" content="<?php echo $search_keys; ?>">
            <meta name="meta_description" content="<?php echo $meta_description; ?>">
            <?php
        } elseif (isset($seo)) {
            ?>
            <title><?php echo $seo[0]['title']; ?></title>
            <meta name="title" content="<?php echo $seo[0]['title']; ?>">
            <meta name="keywords" content="<?php echo $seo[0]['search_keys']; ?>">
            <meta name="meta_description" content="<?php echo $seo[0]['meta_description']; ?>">



            <?php
        } else {
            ?>

            <title> <?php echo 'Contact Us' ?></title>
            <meta name="title" content="<?php echo 'Contact Us' ?>">
            <meta name="keywords" content="<?php echo 'Contact Us , Nagatour, Tour In Nepal' ?>">
            <meta name="meta_description" content="<?php echo 'Contact Us , Nagatour, Tour In Nepal' ?>">
        <?php } ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--social icon-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-social.css">
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/font-awesome.css">
        <!--social icon end-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['favicon']; ?>">

<!--<link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/normalize.css">-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.css">
        <link href="<?php echo base_url(); ?>bootstrap/css/chosen.css" rel="stylesheet" type="text/css"/>  
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-theme.min.css">-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/responsive-slider.css">
        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>
        <!--<script src="<?php echo base_url(); ?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>-->
        <!--<script src="<?php echo base_url(); ?>design/frontend/js/bootstrap.min.js"></script>-->
        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>
       <!--<script src="<?php echo base_url(); ?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>-->
        <script src="<?php echo base_url(); ?>design/frontend/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>bootstrap/js/chosen.jquery.js"></script>
        <script src="<?php echo base_url(); ?>bootstrap/js/unicode.js"></script>
    </head>
    <body>
        <div class="container">    
            <div id="heading_top">
                <div class="row">
                    <div class="col-lg-2"> 
                        <a title="Upveda Technology Pvt. Ltd, Nepal" href="#"><img src="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['logo']; ?>" alt="SERDeN logo" height="80" style="text-align:center;"></a>
                    </div>
                    <div class="col-lg-7">
                        <div class="logo_info_title">
                            Upveda Technology CMS Template
                        </div>	
                        <div class="logo_info">
                            A CMS built for your ease.
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="language_list col-lg-3">
                            <!--                         code by j@y  for language section -->

                            <ul id="lang">
                                <?php
                                $count = count($language_list);
                                for ($i = 0; $i < $count; $i++) {
                                    ?>

                                    <li class="icon btn btn-active" type= "button" onclick="ajaxFunctionName()" id="<?php echo $language_list[$i]->short_language; ?>"><?php echo $language_list[$i]->short_language; ?></li>

<?php } ?>
                            </ul>
                            <!--             /* end of language selection   */-->

                        </div>
                    </div>

                    <div class="col-lg-2">
                        <ul class="icons">
                            <li class="icon"><a href="http://twitter.com"><img src="<?php echo base_url(); ?>design/frontend/img/twitter.jpg"></a></li>
                            <li class="icon"><a href="http://facebook.com"><img src="<?php echo base_url(); ?>design/frontend/img/facebook.png"></a></li>
                            <li class="icon"><a href="https://plus.google.com"><img src="<?php echo base_url(); ?>design/frontend/img/google.jpg"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!--navigation bar------------------------------------------------------------>

            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>



                <div class="navbar-collapse collapse" style="height: 1px;">
                    <ul class="nav navbar-nav">

                        <?php foreach ($headernav as $header) {//still need to work on slug   ?> 
                            <?php
                            if (empty($header['children'])) {
                                echo '<li>'; /* this is for no-child */
                            } else {
                                ?>                    	
                                <li class="dropdown">
                                <?php }/* this is for dropdown */ ?>

                                <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                    <?php
                                    if ($header['navtype'] == 'URL') {
                                        $prefix = '';
                                    } else {
                                        $prefix = base_url();
                                    }
                                    ?>
                                    <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title'] ?>                                    <b class="caret"></b>                                
                                    <?php }/* end of if */ else { /* this is for no-child */
                                        ?>                            
                                        <?php
                                        if ($header['navtype'] == 'URL') {
                                            $prefix = '';
                                        } else {
                                            $prefix = base_url();
                                        }
                                        ?>
                                        <a href="<?php echo $prefix . $header['href'] ?>" target="<?php echo $header['target'] ?>"><?php echo $header['title'] ?>
    <?php } ?>                           
                                    </a>

                                        <?php if (!empty($header['children'])) {/* this is for dropdown */ ?> 
                                        <ul class="dropdown-menu">
                                                <?php foreach ($header['children'] as $child) { ?>  
                                                <li>                                            
                                                    <?php
                                                    if ($child['navtype'] == 'URL') {
                                                        $prefix = '';
                                                    } else {
                                                        $prefix = base_url();
                                                    }
                                                    ?>
                                                    <a href="<?php echo $prefix . $child['href']; ?>" target="<?php echo $child['target'] ?>"><?php echo $child['title']; ?></a>
                                                </li>
                                        <?php }/* end of child foreach */ ?>
                                        </ul>
    <?php } ?>  

                            </li>
<?php }/* end of parent foreach */ ?>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--banner-------------------------------------------------------------------->

            <?php
            $first_bit = $this->uri->segment(1);
            if ($first_bit == "" || $first_bit == "home") {
                ?>


                <!--============================================start of responsive slider============================-->

                <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                    <div class="slides" data-group="slides">
                        <ul>
    <?php foreach ($banner as $row) { //displaying all banner    ?> 
                                <li>
                                    <div class="slide-body" data-group="slide">
                                        <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>" style="width:1140px;">
                                    </div>
                                </li>
    <?php } ?>
                        </ul>
                    </div>
                    <a class="slider-control left" href="#" data-jump="prev"><</a>
                    <a class="slider-control right" href="#" data-jump="next">></a>

                </div>

                <script src="<?php echo base_url(); ?>design/public/js/responsive-slider.js"></script>
                <script src="<?php echo base_url(); ?>design/public/js/jquery.event.move.js"></script>

                <!--==============================================end of responsive slider============================-->   

<?php } else { ?>



                <!-- start of the content ------------------------------------------>
                <div id="content">
                    <?php
                    if (isset($click)) {
                        $slug = $this->uri->segment(1);
                        ?>   

                        <div class="container" style="min-height:600px;">
                            <div class="breadcrumb_div hidden-xs">  
                                <ul id="breadcrumbs" class="breadcrumb">  
                                    <li>  
                                        <i class="icon-home"></i>  
            <!--                            <a href="<?php // echo base_url();     ?>">Home</a> -->
                                    </li>  
                                    <?php
                                    //making bread crumbs
                                    if ($this->uri->segment(1) != '') {
                                        $breadcrumb_url = $this->uri->segment(1);
                                        $breadcrumb_title = ucfirst($title);
                                        ?>
                                        <!--                                                    <li class="current"> 
                                                                                                <a href="<?php echo base_url() . '' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                                                                            </li> 	-->
                                    <?php } ?>

        <?php if ($this->uri->segment(2)) { ?>
                                        <li class="current"> 
                                                                    <!--<a><?php echo ucfirst($this->uri->segment(2)) ?></a>--> 
                                        </li> 	
        <?php }//end of breadcrumbs  ?>

                                </ul>  
                            </div>

                            <div class="col-lg-8">  
                                <h2 style="color:#e0730c; margin-top: 10px;">
                                    <?php
                                    if ($this->uri->segment(3)) {
                                        $title_new = $this->uri->segment(3);
                                        $title = urldecode($title_new);
                                    } else {
                                        $title = $this->uri->segment(1);
                                    }
                                    if (strpos($title, '_') != false) {
                                        $title_array = explode('_', $title);
                                    } else {
                                        $title_array = explode('-', $title);
                                    }

                                    foreach ($title_array as $tarray) {
                                        echo ucwords(strtolower($tarray));
                                        echo ' ';
                                    }
                                    //   echo $title_echo;
                                    ?>
                                </h2>

                                <?php
                                if (!isset($view_file)) {
                                    $view_file = "";
                                }
                                if (!isset($module)) {
                                    $module = $this->uri->segment(1);
                                }
                                if (($view_file != '') && ($module != '')) {
                                    $path = $module . "/" . $view_file;
                                    $this->load->view($path);
                                } else {
                                    $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                                    echo nl2br($new_description); //put your designs here
//                        echo($option);
                                    ?>
                                    <?php if (($option) == 'yes' && $attachment != '') { ?>
                                        <img src="<?php echo base_url(); ?>uploads/pages/<?php echo $attachment ?>" style="width:100%; margin:10px 0px;">
                                    <?php } ?>
                                    <?php
                                }

                                //echo '<pre>';
                                //print_r($this->session);
                                //die();
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="support">
                        <h4>Important Links</h4>
                        <div class="link">
                            <ul class="list-group"><li class="list-group-item"> <a href="#">Seminar</a></li>
                                <li class="list-group-item"><a href="#">Conference</a></li>
                                <li class="list-group-item"><a href="#">Portfolio</a></li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
    <?php if (!isset($click)) { ?>  

                    <div class="col-lg-12" style="padding:0px;">       
                        <?php
                        if (!isset($view_file)) {
                            $view_file = "";
                        }
                        if (!isset($module)) {
                            $module = $this->uri->segment(1);
                        }
                        if (($view_file != '') && ($module != '')) {
                            $path = $module . "/" . $view_file;
                            $this->load->view($path);
                        } else {
                            $new_description = str_replace("../../../", "./", $description); //replacing image location as root location to display image
                            echo nl2br($new_description); //put your designs here
                        }
                        ?>
                    </div>

    <?php } ?>


                <!--<div class="clearfix"></div>--> 
                <!-- for only one save button -->
                <div          <?php
                $lang_count = 0;
                foreach ($lang as $language) {

                    $lang_count++;
                }
                ?>            
                    <!-- end -->

                    <div class="col-lg-3">
                        <div class="support">
                            <h4> Web Services</h4>
                            <div class="row">
                                <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url(); ?>design/frontend/img/fac1.jpg" width="90px" style="float:left; padding:10px;"> With latest trends in Web Services, we provide fast turnaround time with best quality at reasonable price. Static websites, database Driven, WordPress, Joomla, Drupal, CMS, Custom Programming etc.....  </div>
                                <a href="#" style="float:right; color:#06F; margin-right:12px;">Read More...</a> </div>
                        </div>
                        <div class="support">
                            <div class="gallery">
                                <h4> Gallery</h4>
                                <div class="row">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac1.jpg" width="32%">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac2.jpg" width="32%">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac3.jpg" width="32%">
                                </div>
                                <div class="row">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac4.jpg" width="32%">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac5.jpg" width="32%">
                                    <img src="<?php echo base_url(); ?>design/frontend/img/fac6.jpg" width="32%">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
<?php } ?>

            <div class="clearfix"></div>


            <div class="row"> 
                <div class="col-md-12"> 
                    <div class="tabbable tabbable-custom">
                        <!--                code by for Tab-->
                        <ul class="nav nav-tabs">
                            <?php
                            $i = 1;
                            foreach ($lang as $language) {
                                ?>   
                                <li id="<?php echo $i ?>" class="<?php echo($i == 1) ? 'active' : ''; ?>"><a href="#tab_1_<?php echo $i; ?>" data-toggle="tab"><?php echo $language; ?></a></li> 
                                <?php
                                $i++;
                            }
                            ?>            
                            <!-- end of Tab --> 
                        </ul>
                        <!--form start -->  
                    </div>
                    <div class="widget box"> 
                        <div class="widget-header">
                        </div> 
                        <div class="widget-content">

                            <div class="tab-content">
                                <?php
                                $i = 1;
                                foreach ($lang as $key => $language) {
                                    ?>
                                    <!-- content div with tab -->
                                    <div role="tabpanel" class="tab-pane <?php echo($i == 1) ? 'active' : ''; ?>" id="tab_1_<?php echo $i; ?>">
                                        <!--</div>-->
                                         <?php // echo form_open('template/search'.$i, 'class="form-horizontal row-border" '); ?>
                                        <div class="col-md-2">
                                            <label class="col-md-10 control-label">Title</label> 
                                            <div class="col-md-12">
                                                <?php
                                                if (!isset(${'title_' . $language})) {
                                                    ${'title_' . $language} = '';
                                                }
                                                if ($i == 1) {
                                                echo form_input('title_' . $language, ${'title_' . $language}, 'class="form-control" id=title_'.$i.' ');
                                                }else{
                                                echo form_input('title_' . $language, ${'title_' . $language}, 'class="form-control" onkeypress = "return setUnicode(event,this);" id=title_'.$i.' ');    
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <input name="language_<?php echo $language;?>" type="hidden" value="<?php echo $key;?>" id="language_<?php echo $i;?>">
                                        <?php // form_input(array('name' => 'language_'.$language,$key, 'type'=>'hidden', 'id' =>'language_'.$i));?>
                                            <?php // echo form_hidden('language_'.$language,$key,'class="form-control" id=language_'.$i.' ');
                     ?>
                                        <div class="col-md-2">
                                            <label class="col-md-10 control-label">Category</label> 
                                            <div class="col-md-12">
                                                <?php
                                 
                                                if ($i == 1) {
                                                    $options = $category_array_english;
                                                    echo form_dropdown('category_' . $language, $options,'', 'class="form-control chosen-select-deselect" id=category_id_1');
                                                } else {
                                                    $options = $category_array_nepali;
                                                    echo form_dropdown('category_' . $language, $options,'', 'class="form-control  chosen-select-deselect" id=category_id_2 data-lang="Nepali"');
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="col-md-10 control-label" style="width:100%">Document Type</label> 
                                            <div class="col-md-12">
                                                <?php
                                               
                                                if ($i == 1) {
                                                    $options = $document_type_array_english;
                                                    echo form_dropdown('document_type_' . $language, $options,'', 'class="form-control chosen-select-deselect" id=document_type_1');
                                                } else {
                                                    $options = $document_type_array_nepali;
                                                    echo form_dropdown('document_type_' . $language, $options,'', 'class="form-control chosen-select-deselect" data-lang="Nepali" id=document_type_2');
                                                }
                                                ?>      


                                            </div> 

                                        </div>
                                        <button type="button" id="search<?php echo $i;?>"  class="btn btn-primary" style="margin-top:20px; letter-spacing: 1px;" onclick="search_record('<?php echo $i; ?>');"> Search </button>
<?php // echo form_submit(array('id'=>'search'.$i ,  'name'=>'search'.$language, 'value'=> 'Search', 'class'=>'btn btn-primary','style'=>'margin-top:20px; letter-spacing: 1px;' ));?>
                                        <!--</form>-->
                                    </div> 
    <?php
    $i++;
}
?>
                            </div>
                            
<div id="replaceTable">
    </div>



                        </div>


                    </div> 
                </div> 
            </div>
        </div>


    </div>
</div>
</div>
<div class="clearfix"></div>
<?php if (!isset($click) || $first_bit == 'home') { ?>
    <div class="footer">
        <div class="row">
            <div class="col-lg-8">
                <div class="footernav">
                    <ul class="nav">
                            <?php foreach ($footernav as $footer) {//still need to work on slug ?> 
                            <li>
                                <?php
                                if ($footer['navtype'] == 'URL') {
                                    $prefix = '';
                                } else {
                                    $prefix = base_url();
                                }
                                ?>
                                <a href="<?php echo $prefix . $footer['href']; ?>" target="<?php echo $footer['target']; ?>"><?php echo $footer['title']; ?></a>
                            </li>
    <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <p>Developed By <a href="http://upvedatech.com"><img src="<?php echo base_url(); ?>design/frontend/img/logo.png" width="150" height="70"></a></p>
            </div>
        </div>
        <p>All rights reserved! &copy; <?php echo date("Y"); ?></p>
    </div>
<?php } ?>
<!--</div>-end-content--->


<!--</div>end of container-->
<script src="<?php echo base_url(); ?>design/frontend/js/plugins.js"></script> 
<script src="<?php echo base_url(); ?>design/frontend/js/main.js"></script> 
<script src="<?php echo base_url(); ?>design/frontend/js/jquery.event.move.js"></script>
<script src="<?php echo base_url(); ?>design/frontend/js/responsive-slider.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
<script>
    (function(b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function() {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
</script>

<script>
    jQuery(function($) {
        $('.navbar .dropdown').hover(function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

        }, function() {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

        });

        $('.navbar .dropdown > a').click(function() {
            location.href = this.href;
        });

    });
</script>
<script>
    $(document).ready(function() {
//       $('.chosen-select-deselect').chosen();
        //$(".chosen-select-deselect").chosen({ allow_single_deselect: true });
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });
</script>

<script>

    function search_record(i){
    var title=$("#title_"+i).val();
    var language_id=$("#language_"+i).val();
    var category_id=$("#category_id_"+i).val();
    var document_type_id=$("#document_type_"+i).val();
    var base_url='<?php // echo $base_url;?>';
    $.ajax({
        type:'POST',
        url:base_url+'template/search',
        data:"title=" + title+ "&category_id=" + category_id+ "&document_type_id=" +document_type_id+ "&language_id=" +language_id,
        success:function(data)
        {
            $('#replaceTable').html(data); 
        }
    });
}
</script>
<script>
    $('#category_id_1').change(function ()
    {
        var category_id = $('#category_id_1').val();
        var base_url = '<?php echo base_url(); ?>'
        $.ajax({
            url: base_url + "admin/document_type/get_document_type_english",
            type: 'POST',
            data: "category_id=" + category_id,
            dataType: 'JSON',
            success: function (result) {
                $("#document_type_1").empty();

                $.each(result, function (k, v) {
                    $("#document_type_1").append($('<option>', {
                        value: k,
                        text: v
                    }));

                });
                $("#document_type_1").trigger("chosen:updated");
            }
        });

    });
    $('#category_id_2').change(function ()
    {
        //    $(this).parent().attr('id');
        var category_id = $('#category_id_2').val();
//        var sewa_id = $('#sewa').val();
        var base_url = '<?php echo base_url(); ?>'
        $.ajax({
            url: base_url + "admin/document_type/get_document_type_nepali",
            type: 'POST',
            data: "category_id=" + category_id,
            dataType: 'JSON',
            success: function (result) {
                $("#document_type_2").empty();

                $.each(result, function (k, v) {
                    $("#document_type_2").append($('<option>', {
                        value: k,
                        text: v
                    }));

                });
                $("#document_type_2").trigger("chosen:updated");
            }
        });

    });
</script>
</body>
</html>