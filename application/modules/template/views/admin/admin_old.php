<!DOCTYPE html>
<html lang="en">  
    <head>  
        <meta charset="utf-8"> 	 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>      
        <title>Dashboard</title> 
        <!--dynamic module css-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/dynamic/css/style.css">


        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $favicon; ?>">
        <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/main.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/plugins.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/responsive.css" rel="stylesheet" type="text/css"/>  
        <link href="<?php echo base_url(); ?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>  
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css">  
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

        <!--dynamic module js-->
        <script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-1.11.0.min.js"></script>	
        <script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-ui.js"></script>	
        <script src="<?php echo base_url(); ?>design/admin/dynamic/js/script.js"></script>	


        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/libs/jquery-1.10.2.min.js"></script> 
          <!--<script src="<?php echo base_url(); ?>design/admin/dynamic/js/jquery-1.11.0.min.js"></script>-->	

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/libs/lodash.compat.min.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/event.swipe/jquery.event.move.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/event.swipe/jquery.event.swipe.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/libs/breakpoints.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/fileinput/fileinput.js"></script>  <!----> 
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/validation/jquery.validate.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/respond/respond.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/cookie/jquery.cookie.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/sparkline/jquery.sparkline.min.js"></script> 

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/flot/jquery.flot.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/flot/jquery.flot.tooltip.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/flot/jquery.flot.resize.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/flot/jquery.flot.time.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/flot/jquery.flot.growraf.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>  

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/daterangepicker/moment.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/blockui/jquery.blockUI.min.js"></script>  

        these are for pagination under table 
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/datatables/DT_bootstrap.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/datatables/responsive/datatables.responsive.js"></script>
         

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/fullcalendar/fullcalendar.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/noty/jquery.noty.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/noty/layouts/top.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/noty/themes/default.js"></script>  

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/uniform/jquery.uniform.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/select2/select2.min.js"></script> 

        <script type="text/javascript" src="<?php echo base_url(); ?>plugins/nestable/jquery.nestable.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/app.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/plugins.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/plugins.form-components.js"></script>  

        <script>
            $(document).ready(function () {
                App.init();
                Plugins.init();
                FormComponents.init()
            });
        </script>  

        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/demo/form_validation.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/custom.js"></script>  

        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/demo/ui_nestable_list.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/demo/pages_calendar.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/demo/charts/chart_filled_blue.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/demo/charts/chart_simple.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/jquery.tablesorter.min.js"></script>

    </head> 

    <body>  
        <header class="header navbar navbar-fixed-top" role="banner">  
            <div class="container">  
                <ul class="nav navbar-nav">  
                    <li class="nav-toggle"> 
                        <a href="javascript:void(0);" title=""> 
                            <i class="icon-reorder"></i> 
                        </a> 
                    </li>  
                </ul>  
                <a class="navbar-brand" href="<?php echo base_url(); ?>">  
                    <img src="<?php echo base_url(); ?>design/admin/img/logo.png" alt="logo" style="height:30px;"/> 
                </a> 

                <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">  
                    <i class="icon-reorder"></i>  
                </a>  
                <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">  
                    <li>  
                        <a href="<?php echo base_url(); ?>admin/dashboard"> Dashboard </a>  
                    </li>  
                </ul>  

                <ul class="nav navbar-nav navbar-right">  
                    <li class="dropdown user">  
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">  
                            <i class="icon-male"></i>  
                            <span class="username"><?php echo strtok($user_display_name, " "); //display name of a logged in user                ?></span>  
                            <i class="icon-caret-down small"></i>  
                        </a>  
                        <ul class="dropdown-menu">  
                            <li> 
                                <a href="<?php echo base_url(); ?>admin_login/logout"> 
                                    <i class="icon-key"></i> Log Out 
                                </a> 
                            </li>  
                        </ul>  
                    </li>  
                </ul>  
            </div>            
        </header>  

        <div id="container">  
            <div id="sidebar" class="sidebar-fixed">  
                <div id="sidebar-content">  
                    <ul id="nav">  
                        <li class="current">  
                            <a href="<?php echo base_url(); ?>admin/dashboard">  
                                <i class="icon-dashboard"></i> Dashboard  
                            </a>  
                        </li>  

                        <?php
                        if (!empty($sidebar)) {
                            foreach ($sidebar as $list) {
                                if (file_exists(APPPATH . "modules/" . $list . "/views/admin/sidebar.php")) {//this if statement is to check if there is a display page/file(i.e. sidebar.php) in admin view of any module
                                    $this->load->view($list . "/admin/sidebar"); //this is sidebar							
                                    //$sidebar = "$modulename/admin/sidebar"; //this is the format to load sidebar
                                    //the first part is modulename, second part is admin...which means admin folder from views folder and third is sidebar.php
                                }
                            }
                        }
                        ?>


                    </ul>   
                </div>  
                <div id="divider" class="resizeable"></div>  
            </div>  
            <div id="content">
                <div class="container">  
                    <div class="crumbs">  
                        <ul id="breadcrumbs" class="breadcrumb">  
                            <li>  
                                <i class="icon-home"></i>  
                                <a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a> 
                            </li>  
                            <?php
                            //making bread crumbs
                            if ($this->uri->segment(2) != 'dashboard') {
                                $breadcrumb_url = $this->uri->segment(2);
                                $breadcrumb_title = ucfirst($this->uri->segment(2));
                                ?>
                                <li class="current"> 
                                    <a href="<?php echo base_url() . 'admin/' . $breadcrumb_url; ?>" title=""><?php echo $breadcrumb_title; ?></a> 
                                </li> 							
                            <?php } ?>

                            <?php if ($this->uri->segment(3)) { ?>
                                <li class="current"> 
                                    <a><?php echo ucfirst($this->uri->segment(3)) ?></a> 
                                </li> 							
                            <?php }//end of breadcrumbs ?>

                        </ul>  
                        <ul class="crumb-buttons">  
                            <li> 
                                <a href="#" title=""> 
                                    <i class="icon-user"></i>
                                    <span>Hello, <?php echo strtok($user_display_name, " "); ?>!</span>
                                </a> 
                            </li>
                        </ul>                    
                    </div> 

                    <div class="row"> 
                        <div class="col-md-12">  
                            <div style="padding-top:25px;"></div>
                            <!--this is where body part goes-->  

                            <?php
                            if (!isset($view_file)) {
                                $view_file = "";
                            }
                            if (!isset($module)) {
                                $module = $this->uri->segment(2);
                            }
                            if (($view_file != '') && ($module != '')) {
                                $path = $module . "/" . $view_file;
                                $this->load->view($path);
                            }
                            ?>     
                            <!--this is where body part ends--> 
                        </div>
                    </div>                       

                </div>  
            </div>  
        </div> 
        <script type="text/javascript">
            if (location.host == "envato.stammtec.de" || location.host == "themes.stammtec.de") {
                var _paq = _paq || [];
                _paq.push(["trackPageView"]);
                _paq.push(["enableLinkTracking"]);
                (function () {
                    var a = (("https:" == document.location.protocol) ? "https" : "http") + "://analytics.stammtec.de/";
                    _paq.push(["setTrackerUrl", a + "piwik.php"]);
                    _paq.push(["setSiteId", "17"]);
                    var e = document, c = e.createElement("script"), b = e.getElementsByTagName("script")[0];
                    c.type = "text/javascript";
                    c.defer = true;
                    c.async = true;
                    c.src = a + "piwik.js";
                    b.parentNode.insertBefore(c, b);
                });
            }
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/searchPagination.js"></script>
    </body>  
</html>