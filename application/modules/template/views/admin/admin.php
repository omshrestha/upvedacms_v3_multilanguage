<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="title" content="">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/all.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/font-awesome.min.css">
</head>
<body>
  <header>
    <div class="header">
      <div class="header-top">
        <div class="col-sm-4 col-md-2">
          <h1 id="logo" class="div-margin">
            <img src="<?php echo base_url(); ?>design/admin/images/logo.png" class="img-responsive">
          </h1>
        </div>
        <div class="col-md-5 div-margin">
          <h4 class="heading">नेपाल सरकार</h4>
          <h4 class="heading">गृह मंत्रालय</h4>
          <h4 class="heading">District Administration Automation System (DAAS)</h4>
        </div>
        <div class="col-md-5 div-margin">
          <div class="pull-right">
            <span class="nepali-date">
             <iframe scrolling="no" border="0" frameborder="0" marginwidth="0" marginheight="0" allowtransparency="true" src="http://www.ashesh.com.np/linknepali-time.php?time_only=no&font_color=333333&aj_time=yes&font_size=12&line_brake=0&bikram_sambat=0&nst=no&api=302156g059" width="125" height="45"></iframe>
           </span>
         </div>
         <div class="clearfix"></div>
         <div class="dropdown pull-right">
          <!-- <button class="btn btn-default dropdown-toggle fa fa-user" type="button" data-toggle="dropdown"> -->
          <h5 class="pull-right log-out" type="button" data-toggle="dropdown">welcome user 
            <i class="fa fa-user" aria-hidden="true" ></i>
          </h5>
          <!-- <span class="caret"></span></button> -->
          <ul class="dropdown-menu pull-right log-out-style">
            <li><a href="#">log out</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="header-bottom">
      <div class="menu div-margin">
        <ul>
          <li class="current"><a href="#" id="menu_admin_viewAdminModule" class="firstLevelMenu"><b>Admin</b></a>
            <ul>
              <li class="selected"><a href="#" id="menu_admin_UserManagement" class="arrow">User Management</a>
                <ul>
                  <li><a href="#" id="menu_admin_viewSystemUsers">Users</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_admin_Job" class="arrow">Job</a>
                <ul>
                  <li><a href="innerpage.html" id="menu_admin_viewJobTitleList">Job Titles</a></li>
                  <li><a href="#" id="menu_admin_viewPayGrades">Pay Grades</a></li>
                  <li><a href="#" id="menu_admin_employmentStatus">Employment Status</a></li>
                  <li><a href="#" id="menu_admin_jobCategory">Job Categories</a></li>
                  <li><a href="#" id="menu_admin_workShift">Work Shifts</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_admin_Organization" class="arrow">Organization</a>
                <ul>
                  <li><a href="#" id="menu_admin_viewOrganizationGeneralInformation">General Information</a></li>
                  <li><a href="#" id="menu_admin_viewLocations">Locations</a></li>
                  <li><a href="#" id="menu_admin_viewCompanyStructure">Structure</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_admin_Qualifications" class="arrow">Qualifications</a>
                <ul>
                  <li><a href="#" id="menu_admin_viewSkills">Skills</a></li>
                  <li><a href="#" id="menu_admin_viewEducation">Education</a></li>
                  <li><a href="#" id="menu_admin_viewLicenses">Licenses</a></li>
                  <li><a href="#" id="menu_admin_viewLanguages">Languages</a></li>
                  <li><a href="#" id="menu_admin_membership">Memberships</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_admin_nationality">Nationalities</a>
              </li>   
              <li><a href="#" id="menu_admin_Configuration" class="arrow">Configuration</a>
                <ul>
                  <li><a href="#" id="menu_admin_listMailConfiguration">Email Configuration</a></li>
                  <li><a href="#" id="menu_admin_viewEmailNotification">Email Subscriptions</a></li>
                  <li><a href="#" id="menu_admin_localization">Localization</a></li>
                  <li><a href="#" id="menu_admin_viewModules">Modules</a></li>
                </ul> <!-- third level -->
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu_pim_viewPimModule" class="firstLevelMenu"><b>PIM</b></a>
            <ul>
              <li><a href="#" id="menu_pim_Configuration" class="arrow">Configuration</a>
                <ul>
                  <li><a href="#" id="menu_pim_configurePim">Optional Fields</a></li>
                  <li><a href="#" id="menu_pim_listCustomFields">Custom Fields</a></li>
                  <li><a href="#" id="menu_admin_pimCsvImport">Data Import</a></li>
                  <li><a href="#" id="menu_pim_viewReportingMethods">Reporting Methods</a></li>
                  <li><a href="#" id="menu_pim_viewTerminationReasons">Termination Reasons</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_pim_viewEmployeeList">Employee List</a>
              </li>   
              <li><a href="#" id="menu_pim_addEmployee">Add Employee</a>
              </li>   
              <li><a href="#" id="menu_core_viewDefinedPredefinedReports">Reports</a>
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu_leave_viewLeaveModule" class="firstLevelMenu"><b>Leave</b></a>
            <ul>
              <li><a href="#" id="menu_leave_Entitlements" class="arrow">Entitlements</a>
                <ul>
                  <li><a href="#" id="menu_leave_addLeaveEntitlement">Add Entitlements</a></li>
                  <li><a href="#" id="menu_leave_viewLeaveEntitlements">Employee Entitlements</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_leave_Reports" class="arrow">Reports</a>
                <ul>
                  <li><a href="#" id="menu_leave_viewLeaveBalanceReport">Leave Entitlements and Usage Report</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_leave_Configure" class="arrow">Configure</a>
                <ul>
                  <li><a href="#" id="menu_leave_defineLeavePeriod">Leave Period</a></li>
                  <li><a href="#" id="menu_leave_leaveTypeList">Leave Types</a></li>
                  <li><a href="#" id="menu_leave_defineWorkWeek">Work Week</a></li>
                  <li><a href="#" id="menu_leave_viewHolidayList">Holidays</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_leave_viewLeaveList">Leave List</a>
              </li>   
              <li><a href="#" id="menu_leave_assignLeave">Assign Leave</a>
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu_time_viewTimeModule" class="firstLevelMenu"><b>Time</b></a>
            <ul>
              <li><a href="#" id="menu_time_Timesheets" class="arrow">Timesheets</a>
                <ul>
                  <li><a href="#" id="menu_time_viewEmployeeTimesheet">Employee Timesheets</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_attendance_Attendance" class="arrow">Attendance</a>
                <ul>
                  <li><a href="#" id="menu_attendance_viewAttendanceRecord">Employee Records</a></li>
                  <li><a href="#" id="menu_attendance_configure">Configuration</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_time_Reports" class="arrow">Reports</a>
                <ul>
                  <li><a href="#" id="menu_time_displayProjectReportCriteria">Project Reports</a></li>
                  <li><a href="#" id="menu_time_displayEmployeeReportCriteria">Employee Reports</a></li>
                  <li><a href="#" id="menu_time_displayAttendanceSummaryReportCriteria">Attendance Summary</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_admin_ProjectInfo" class="arrow">Project Info</a>
                <ul>
                  <li><a href="#" id="menu_admin_viewCustomers">Customers</a></li>
                  <li><a href="#" id="menu_admin_viewProjects">Projects</a></li>
                </ul> <!-- third level -->
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu_recruitment_viewRecruitmentModule" class="firstLevelMenu"><b>Recruitment</b></a>
            <ul>
              <li><a href="#" id="menu_recruitment_viewCandidates">Candidates</a>
              </li>   
              <li><a href="#" id="menu_recruitment_viewJobVacancy">Vacancies</a>
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu__Performance" class="firstLevelMenu"><b>Performance</b></a>
            <ul>
              <li><a href="#" id="menu_performance_Configure" class="arrow">Configure</a>
                <ul>
                  <li><a href="#" id="menu_performance_searchKpi">KPIs</a></li>
                  <li><a href="#" id="menu_performanceTracker_addPerformanceTracker">Trackers</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_performance_ManageReviews" class="arrow">Manage Reviews</a>
                <ul>
                  <li><a href="#" id="menu_performance_searchPerformancReview">Manage Reviews</a></li>
                </ul> <!-- third level -->
              </li>   
              <li><a href="#" id="menu_performanceTracker_viewEmployeePerformanceTrackerList">Employee Trackers</a>
              </li>   
            </ul> <!-- second level -->                        
          </li>
          <li><a href="#" id="menu_dashboard_index" class="firstLevelMenu"><b>Dashboard</b></a>
            <ul>
              <li></li>
            </ul> <!-- second level -->                        
          </li>
        </ul> <!-- first level -->
      </div>
    </div>
  </div>
</header>
<main>
  <div class="col-md-12 content-title">
   <?php
   if (!isset($view_file)) {
    $view_file = "";
  }
  if (!isset($module)) {
    $module = $this->uri->segment(2);
  }
  if (($view_file != '') && ($module != '')) {
    $path = $module . "/" . $view_file;
    $this->load->view($path);
  }
  ?>

</div>
</main>
<footer>
  <div class="footer-style">
    <p>copyright @ 2016</p>
  </div>
</footer>
</div>


</div>
<script src="<?php echo base_url(); ?>design/admin/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>design/admin/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/nav.min.js" ></script>

<!-- // <script src="js/script.js"></script> -->
<!-- <script src="js/index.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/canvasjs.js"></script>
<script src="<?php echo base_url(); ?>design/admin/js/main.js"></script>

</body>
</html>