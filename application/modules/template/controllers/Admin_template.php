<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_template extends MX_Controller {

    public function __construct() {
        parent::__construct();
        //always check if session userdata value "logged_in" is not true
        if (!$this->session->userdata("logged_in"))/* every logged_in user get privilage to access dashboard */ {
            redirect('admin');
        }

        $this->load->library('pagination');
        $this->load->library('Up_pagination');
    }

    function admin($data) {
//        var_dump($data);
//        die();
        $user_id = $this->session->userdata['user_id'];
        $data['user_display_name'] = $this->get_username_from_user_id($user_id);
        $data['favicon'] = $this->get_attachment('favicon');
        $data['logo'] = $this->get_attachment('logo');
        $group_id = $this->session->userdata['group_id']; //this usually represent what group the currently logged in user belong to
        $data['sidebar'] = $this->get_all_sidebar($group_id);

        $data['modules'] = $this->get_lists_of_modules('id');
        // these three values are created and used in sidebar page of each...
//		print_r($data['modules']); die();

        $data['pages'] = $this->get_lists_of_pages('id');       // ...module, page and navigation to display the sidbebar content... 
        $data['navigation'] = $this->get_lists_of_navigation_group('id');  // ...and use the loop from given variable to display results

        $this->load->view('admin/admin', $data);
    }

    function get_all_sidebar($group_id) {
        if ($group_id == '1') {
            $data['permissions'] = 'access_all';
        } //if admin we want it to access it all
        else {
            $data['permissions'] = $this->unserialize_role_array($group_id);
        } //if not we only want permissions what we are set to		
        //print_r($data['permissions']); die();
        if (empty($data['permissions'])) {
            return NULL;
        } elseif ($data['permissions'] == 'access_all')/* for admins */ {
            $modulelists = $this->get_all_slug_from_module();
            return $modulelists;
        } else {/* if there is an array */
            $i = 1;
            //print_r($data['permissions']);die();
            foreach ($data['permissions'] as $module_id) {
                if (strlen($module_id > 2)) {
                    $module_id = substr($module_id, 0, 1);
                    $modulelists[$i] = $this->get_slug_from_moduleid($module_id);
                    $i++;
                }
            }
//                                print_r($module_id);
//                                var_dump($modulelists); die();
            return $modulelists;
        }
    }

    function get_username_from_user_id($user_id) {
        $this->load->model('users/mdl_users');
        $username = $this->mdl_users->get_username_from_user_id($user_id);
        return $username;
    }

    function get_all_slug_from_module() {
        $this->load->model('modules/mdl_moduleslist');
        $module_array = $this->mdl_moduleslist->get_all_slug_from_module();
        return $module_array;
    }

    function get_slug_from_moduleid($module_id) {
        $this->load->model('modules/mdl_moduleslist');
        $module_name = $this->mdl_moduleslist->get_slug_from_moduleid($module_id);
        return $module_name;
    }

    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_attachment($name) {
        $this->load->model('settings/mdl_settings');
        $query = $this->mdl_settings->get_where_dynamic('', $name);
        $result = $query->result_array();
        return $result[0][$name];
    }

    function get_lists_of_modules_data($order_by, $start, $limit) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_where_dynamic('', '', $order_by, $start, $limit);
        return $query->result_array();
    }

    function get_lists_of_modules($order_by) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_where_dynamic('', '', $order_by);
        return $query->result_array();
    }

    function get_lists_of_pages($order_by) {
        $this->load->model('pages/mdl_pages');
        $query = $this->mdl_pages->get_where_dynamic('', '', $order_by);
        return $query->result_array();
    }

    function get_lists_of_navigation_group($order_by) {
        $this->load->model('navigation/mdl_navigation_group');
        $query = $this->mdl_navigation_group->get_group($order_by)->result();
        return $query;
    }

}
