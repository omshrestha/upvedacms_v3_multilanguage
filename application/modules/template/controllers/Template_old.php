<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template extends MX_Controller {

    function __construct() {
        parent::__construct();
        if($this->session->userdata('language') == "eng")
		{
			$this->lang->load('english', 'english');
		}
		else if($this->session->userdata('language')=="nep")
		{
			$this->lang->load('nepali', 'nepali');
		}
                else {
                $this->lang->load('english', 'english');
                }
    }
    function get_data_from_post() {
        $data['lang'] = $this->get_language_tab();
//        $update_id = $this->input->post('update_id_english', TRUE);
//        if (is_numeric($update_id)) {
//            $attach = $this->get_attachment_from_db($update_id);
//            $data['upd_date'] = date("Y-m-d");
//        } else {
//            $data['ent_date'] = date("Y-m-d");
//            $data['upd_date'] = NULL;
//        }
         foreach ($data['lang'] as $key => $language) {
//             $data['attachment_'.$language] = $this->input->post('userfile', TRUE);
             $data['category_'.$language] = $this->input->post('category_'.$language, TRUE);
             $data['document_type_'.$language] = $this->input->post('document_type_'.$language, TRUE);
        $data['title_'.$language] = $this->input->post('title_'.$language, TRUE);
//        $data['description_'.$language] = $this->input->post('description_'.$language, TRUE);
//        $data['slug_' . $language] = $slug = strtolower(url_title($data['title_english']));
//        $data['meta_description_'.$language] = $this->input->post('meta_description_'.$language, TRUE);
//        $data['meta_key_'.$language] = $this->input->post('meta_key_'.$language, TRUE);
         $data['language_id_' . $language] = $this->input->post('language_' . $language, TRUE);
//          $data['update_id_' . $language] = $this->input->post('update_id_' . $language, TRUE);
//        $data['status_'.$language] = $this->input->post('status_'.$language, TRUE);
//        $update_id = $this->input->post('update_id', TRUE);
         }
//          var_dump($data);die;
        return $data;
    }
//    function search1(){
////        $data = $this->get_data_from_post();
//        $this->load->model('document/Mdl_document');
//        $data['category_id'] = $this->input->post('category_english', TRUE);
//             $data['document_type_id'] = $this->input->post('document_type_english', TRUE);
//        $data['title'] = $this->input->post('title_english', TRUE);
//         $data['language_id'] = $this->input->post('language_english', TRUE);
//         $query=$this->Mdl_document->get_search_data($data);
//        var_dump($query);die('hello');
////        die('english');
//    }
    function search(){
//        die('slkdkjsfk');
        $this->load->model('document/Mdl_document');
//        $query=$this->Mdl_document->get_search_data($data);
        $data['category_id'] = $this->input->post('category_id', TRUE);
             $data['document_type_id'] = $this->input->post('document_type_id', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
         $data['language_id'] = $this->input->post('language_id', TRUE);
         $data['query']=$this->Mdl_document->get_search_data($data);
         $this->load->view('table',$data);
    }
function get_language_tab() {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language_tab();
        return $query;
    }
    function front($data) {
//        die('jhghfhgf');
//$data['lang'] = $this->get_language_tab();
         $data = $this->get_data_from_post();
  $data['document_type_array_english']=$this->get_english_document_type();
        $data['document_type_array_nepali']=$this->get_nepali_document_type();
         $data['category_array_english']=$this->get_english_category();
        $data['category_array_nepali']=$this->get_nepali_category();
         $language_id=$data['language_id_english'];
//         print_r($language_id);die;
          if(!$this->session->userdata('language')){
                   $data['language']= $selected_language = 'eng';
                }
                else{
                    $data['language']=$selected_language=$this->session->userdata['language'];
                }
                $selected_language_id = $this->get_selected_language_id($selected_language);
                $data['language_list']=$this->get_language_list();
        $nav = $this->session->userdata('navtype');
        $page = $this->get_page_live();
        foreach ($page as $row) {
            $page_name [] = $row['slug'];
        }
        $module_name = $this->uri->segment(1);
        $slug = $this->uri->segment(3);
        if (!empty($slug)) {
            $data['seo'] = $this->get_metadata_search_keys($module_name, $slug);
        } else {
            if ($module_name != '' && in_array($module_name, $page_name) == '' && $module_name != 'contactus') {
                $data['seo'] = $this->get_metadata_search_module($module_name);
            }
        }
        $data['site_settings'] = $this->get_site_settings();
        $data['banner'] = $this->get_banner();
        $data['headernav'] = $this->get_header('1',$language_id); //this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise
//		var_dump($data['headernav']);die;
        $data['footernav'] = $this->get_footer('2',$language_id);
        //similar as above but 2 is for footer nav
//        die('khk');
        $this->load->view('front', $data);
    }
    
    function get_english_category(){
    $this->load->model('category/mdl_category');
    return $this->mdl_category->get_category_english_dropdown();
}
function get_nepali_category(){
    $this->load->model('category/mdl_category');
    return $this->mdl_category->get_category_nepali_dropdown();
}
 function get_english_document_type(){
    $this->load->model('document_type/mdl_document_type');
    return $this->mdl_document_type->get_english_dropdown();
}
function get_nepali_document_type(){
    $this->load->model('document_type/mdl_document_type');
    return $this->mdl_document_type->get_nepali_dropdown();
}

    function get_site_settings() {
        $this->load->model('settings/mdl_settings');
        $query = $this->mdl_settings->get_settings();
        $result = $query->result_array();
        return $result[0];
    }

    function get_navigation_from_navigation_name($navigation_name) {
        $this->load->model('navigation/mdl_navigation');
        $query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
        $result = $this->add_href($query->result_array());
        return $result;
    }

    function errorpage() {
        $this->load->view('404');
    }

    function userlogin($data) {
        $this->load->view('userlogin', $data);
    }

    function get_banner() {
        $this->load->model('banner/mdl_banner');
        $query = $this->mdl_banner->get_banner();
        return $query->result_array();
    }

    function get_parent($group_id) {
        $this->load->model('navigation/mdl_navigation');
        $query = $this->mdl_navigation->get_parentnav_for_frontend($group_id);
        $result = $this->add_href($query);
//        var_dump($result);die;
        return $result;
    }

    function get_child($group_id, $parent_id) {
        $this->load->model('navigation/mdl_navigation');
        $query = $this->mdl_navigation->get_childnav_for_frontend($group_id, $parent_id);
        $result = $this->add_href($query);
        return $result;
    }

    function get_header($group_id,$language_id) {
        $data['parentnav'] = $this->get_parent($group_id); //
//                var_dump($data['parentnav']);die;
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
            $children = $this->get_child($group_id, $nav['id']);
            if ($children != NULL) {
                $nav['children'] = $children;
            }
            $navigation[$i] = $nav;
            $i++;
        }
        return $navigation;
    }

    function get_footer($group_id,$language_id) {
        
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_footernav($group_id);
        $result = $this->add_href($query->result_array());
        return $result;
    }

    function add_href($result) {
        $count = count($result);
        for ($i = 0; $i < $count; $i++) {
            if ($result[$i]['navtype'] == 'Module') {
                $result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'Page') {
                $result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);
                $result[$i]['target'] = "_self";
            } elseif ($result[$i]['navtype'] == 'URI') {
                $result[$i]['href'] = $result[$i]['site_uri'];
                $result[$i]['target'] = "_self";
            } else {
                $result[$i]['href'] = $result[$i]['link_url'];
                $result[$i]['target'] = "_blank";
            }
        }
        return $result;
    }

    function get_name_from_module($module_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_module($module_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_name_from_page($page_id) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_name_from_page($page_id);
        if (isset($query['0'])) {
            return $query['0']['slug'];
        }
    }

    function get_page_live() {
        $this->load->model('pages/mdl_pages');
        $query = $this->mdl_pages->get_page_live();
        return $query->result_array();
    }

    function get_metadata_search_keys($module_name, $slug) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_keys($module_name, $slug);
        return $query->result_array();
    }

    function get_metadata_search_module($module_name) {
        $this->load->model('mdl_template');
        $query = $this->mdl_template->get_metadata_search_module($module_name);
        return $query;
    }
     function get_language_list(){
        $this->load->model('language/mdl_language');
	$query = $this->mdl_language->get('id');	
	return $query->result();   
        }
         
        function get_selected_language_id($selected_language){
        $this->load->model('language/mdl_language');
        $language_id=  $this->mdl_language->get_language_id($selected_language);
        foreach($language_id -> result() as $id){
        $selected_language_id = $id->id;}
        return  $selected_language_id;
        }
}
