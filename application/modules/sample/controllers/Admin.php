<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/sample";
    private $group_id;
    private $MODULE = 'sample';
    private $model_name = 'Mdl_sample';

    public function __construct() {
        parent::__construct();
        $this->load->model('Mdl_sample');
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('modules/Mdl_moduleslist');
        $this->load->module('admin_login/admin_login');
        $this->load->model('settings/Mdl_settings');
        $this->load->library('Common_functions');
        $this->load->library('pagination');
        $this->load->library('up_pagination');
        
        $this->admin_login->check_session_and_permission('sample'); //module name is sample here	
    }

    function login() {
        echo'Hello World';
    }

    function index() {  
        $main_table_params = 'id,title,description,status';

        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
           
        } else {
            $params = '';
        }
        $count = $this->Mdl_sample->count($params);
 
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_sample->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
//        $data['query'] = $this->Mdl_banner->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['columns'] = array('title', 'description', 'status');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
//         $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }
//     function get_language_list(){
//        $this->load->model('language/Mdl_language');
//	$query = $this->Mdl_language->get('id');	
//	return $query->result();   
//        }
//        function get_selected_language_id($selected_language){
//        $this->load->model('language/Mdl_language');
//        $language_id=  $this->Mdl_language->get_language_id($selected_language);
//        foreach($language_id -> result() as $id){
//        $selected_language_id = $id->id;}
//        return  $selected_language_id;
//        }
    function get_data_from_post() {
        $data1['lang'] = $this->get_language_tab();
        $update_id = $this->input->post('update_id_english', TRUE);
        if (is_numeric($update_id)) {
//            $attach = $this->get_attachment_from_db($update_id);
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
         foreach ($data1['lang'] as $key => $language) {
        $data['title_'.$language] = $this->input->post('title_'.$language, TRUE);
        $data['description_'.$language] = $this->input->post('description_'.$language, TRUE);
        $data['slug_' . $language] = $slug = strtolower(url_title($data['title_english']));
        $data['meta_description_'.$language] = $this->input->post('meta_description_'.$language, TRUE);
        $data['search_keys_'.$language] = $this->input->post('search_keys_'.$language, TRUE);
         $data['language_id_' . $language] = $this->input->post('language_' . $language, TRUE);
          $data['update_id_' . $language] = $this->input->post('update_id_' . $language, TRUE);
        $data['status_'.$language] = $this->input->post('status_'.$language, TRUE);
//        $update_id = $this->input->post('update_id', TRUE);
         }
//          var_dump($data);die;
        return $data;
    }

    function get_data_from_db($update_id) {
        $data1['lang'] = $this->get_language_tab();
        $query = $this->Mdl_sample->get_where_dynamic($update_id);
        
        foreach ($query->result() as $slugg) {
            $slug = $slugg->slug;
            $data['title'] = $slugg->title;
            $data['description'] = $slugg->description;
            $data['status'] = $slugg->status;
            $data['search_keys'] = $slugg->search_keys;
            $data['meta_description'] = $slugg->meta_description;
            $data['ent_date'] = $slugg->ent_date;
           
        }
//       var_dump($data1['lang']);die;
        foreach ($data1['lang'] as $key => $language) {
            $langu_id = $key;
            $update_id = $this->update_id_for_module_edit($langu_id, $slug);
//            var_dump($update_id);die;
            if (isset($update_id)) {
                $querys = $this->Mdl_sample->get_where_dynamic($update_id);
//                var_dump($querys->result());
//                die;
                  foreach ($querys->result() as $row) {
                      
                    $language_id = $row->language_id;
                     $language = $this->get_language($language_id);
                     $data['title_'.$language] = $row->title;
                     $data['description_'.$language] = $row->description;
                      $data['status_'.$language] = $row->status;
                      $data['search_keys_'.$language] = $row->search_keys;
                       $data['meta_description_'.$language] = $row->meta_description;
                       $data['ent_date_'.$language] = $row->ent_date;
                  }
                  $data['update_id_' . $language] = $update_id;
                 
                  } 
//                  else {
//                $language_id = $langu_id;
//                $language = $this->get_language($language_id);
//                 $data['slug_' . $language] = $datanew['slug'] = $slug;
//                 $data['language_id_' . $language] = $datanew['language_id'] = $language_id;
//                 $datanew['ent_date'] = date("Y-m-d");
//                $datanew['upd_date'] = NULL;
//                $update_id = $this->_insert($datanew);
//                $data['update_id_' . $language] = $update_id;
//                  }
        }
       
        

        if (!isset($data)) {
            $data = "";
        }
//        var_dump($datas);die;
        return $data;
    }


      function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
//person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,description,meta_description,search_keys,ent_date,status';
//               $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
               $data=$this->get_data_from_db($update_id);
//                var_dump($data); die;
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['lang'] = $this->get_language_tab();
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
        $group_id = $this->uri->segment(3);
        $data = $this->get_data_from_post();
        $update_id = $this->input->post('update_id_english', TRUE);
       $dataa=  $this->get_data_from_post();
        if (is_numeric($update_id)) {
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            if (isset($permission['edit'])) {
              
            $data1['lang'] = $this->get_language_tab();
           
             foreach ($data1['lang'] as $key => $language) {
                  $data_insert['title'] = $dataa['title_' . $language];
                  $data_insert['description']=$dataa['description_'.$language];
                  $data_insert['status']=$dataa['status_'.$language];
                  $data_insert['search_keys']=$data['search_keys'.$language];
                  $data_insert['slug']=$data['slug_english'];
                   $update_id = $dataa['update_id_' . $language];
//                   $this->Mdl_sample->_update($update_id_english, $data_insert);
             }
            }
//             else{
//                  $data1['lang'] = $this->get_language_tab();
//                $nextid = $this->get_id();
//                $uploadattachment = $this->do_upload($nextid, $data1['lang']);
//                 
//             }
//            print_r($permission);
//            die;

            
        } else {
            $dataa=  $this->get_data_from_post();
//            var_dump($dataa); die;
             $data1['lang'] = $this->get_language_tab();
//             var_dump($data1);die;
//                $nextid = $this->get_id();
//                $uploadattachment = $this->do_upload($nextid, $data1['lang']);
                 foreach ($data1['lang'] as $key => $language) {
                     $data_insert['title'] = $dataa['title_' . $language];
                  $data_insert['description']=$dataa['description_'.$language];
                  $data_insert['status']=$dataa['status_'.$language];
                  $data_insert['search_keys']=$dataa['search_keys_'.$language];
                  $data_insert['language_id']=$dataa['language_id_' . $language];
                  $data_insert['slug']=$data['slug_english'];
                    $this->Mdl_sample->_insert($data_insert); 
                 }
            $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
            
            if (isset($permission['add'])) {
//                var_dump($data_insert); die;
                
            }
        }

        redirect('admin/sample');
    }

    function delete() {
        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/sample');
        } else {
            $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_sample->_delete($delete_id);
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!' . $message);
            redirect('admin/sample');
        }
    }
    function get_language_tab() {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language_tab();
        return $query;
    }
    function get_language($language_id) {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language($language_id);
        foreach ($query as $row) {
            $language = $row->language;
        }
        return $language;
    }

  function get_id() {
        $this->load->model('Mdl_sample');
        $id = $this->Mdl_sample->get_id();
        return $id;
    }
      function update_id_for_module_edit($lang_id, $slug) {
        $this->load->model('Mdl_sample');
        $query = $this->Mdl_sample->update_id_for_module_edit($lang_id, $slug);
//        print_r($query->result());die;
        foreach ($query->result()as $row) {
            $update_id = $row->id;
            return $update_id;
        }
    }

}
