<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {
    private $MODULE='groups';
    private $group_id;
    private $MODULE_PATH = "admin/groups";
    private $model_name = 'Mdl_groups';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('Mdl_groups');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('groups'); //module name is groups here	
    }

    function index() {

        $main_table_params = 'id,name';
        //search parameters
//        if ($this->input->post('per_page')) {
//            $this->session->set_userdata('per_page', $this->input->post('per_page'));
//        }
        if ($this->input->post('parameters')) {

            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
            
        }
        $count = $this->Mdl_groups->count($params);
         if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

//        $count = $this->Mdl_groups->count();
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] = 5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_groups->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id,$this->MODULE);
        $data['columns'] = array('name', 'set_permission');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && (!$this->input->post('per_page')) && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
        $data['name'] = $this->input->post('name', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        return $data;
    }

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
        if ($update_id != 1) {
            $submit = $this->input->post('submit', TRUE);

            if ($submit == "Submit") {
                //person has submitted the form
                $data = $this->get_data_from_post();
            } else {
                if (is_numeric($update_id)) {
                    $select['name'] = 'name';
                    $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                }
            }

            if (!isset($data)) {
                $data = $this->get_data_from_post();
            }

            $data['update_id'] = $update_id;

            $data['view_file'] = "admin/form";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            redirect('admin/groups');
        }
    }

    function submit() {
        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_users_groups.name]'); //unique_validation check while creating new
        }
        /* end of validation rule */


        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();

            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $permission = $this->common_functions->check_permission($this->group_id);
                if (isset($permission['edit'])) {
                    $this->Mdl_groups->_update($update_id, $data);
                }

                $this->session->set_flashdata('operation', 'Updated Successfully!!!');
            } else {
                $permission = $this->common_functions->check_permission($group_id);
                if (isset($permission['add'])) {
                    $this->Mdl_groups->_insert($data);
                }

                $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
            }

            redirect('admin/groups');
        }
    }

}
