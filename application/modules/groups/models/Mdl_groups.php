<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_groups extends Mdl_crud {

    protected $_table = "up_users_groups";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function _insert($data) {
        $next_id = $this->get_id();

        $table = $this->_table;
        $this->db->insert($table, $data);


        $insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
        $permission_table = 'up_permissions';
        $this->db->insert($permission_table, $insert_null_permission_array);
    }

    function get_id() {
        $result = mysql_query("SHOW TABLE STATUS LIKE 'up_users_groups'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment'];
        return $nextId;
    }

    function get_modules_dropdown() {
        $this->db->select('id, name');
        $this->db->order_by('name');
        $dropdowns = $this->db->get('up_modules')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->name;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_groups_dropdown() {
        $this->db->select('id, name');
        $this->db->order_by($this->_primary_key, 'DESC');
        $dropdowns = $this->db->get('up_users_groups')->result();
        $dropdownlist[0] = 'select group';
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->name;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

}
