<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sample_attachment extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function index(){
            //echo 'this is it';die;
                $slug = $this->uri->segment(1);
		$data['sample_attachment']=$this->get_sample_attachment('id');
               // var_dump($data['sample_attachment']); die;
		$data['view_file']="sample_attachment_front";
		$this->load->module('template');
		$this->template->front($data);
	}

        function get($order_by){
	$this->load->model('mdl_sample_attachment');
	$query = $this->mdl_sample_attachment->get($order_by);
	return $query;
	}
        function get_sample_attachment($order_by){
	$this->load->model('mdl_sample_attachment');
	$query = $this->mdl_sample_attachment->get_sample_attachment($order_by);
	return $query;
	}
}