<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module('admin_login/admin_login');
        $this->load->model('Mdl_sample_attachment');
        $this->admin_login->check_session_and_permission('sample_attachment'); //module name is groups here	
    }

    function index() {
        $data['query'] = $this->get('id');

        $data['view_file'] = "admin/table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function get_data_from_post() {
        $data['title'] = $this->input->post('title', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));
        $data['meta_description'] = $this->input->post('meta_description', TRUE);
        $data['search_keys'] = $this->input->post('search_keys', TRUE);
        $data['status'] = $this->input->post('status', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            $data['attachment'] = $attach['attachment'];
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['attachment'] = $this->input->post('userfile', TRUE);
            //echo $data['attachment']; die;
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function get_data_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['title'] = $row->title;
            $data['description'] = $row->description;
            $data['attachment'] = $row->attachment;
            $data['status'] = $row->status;
            $data['search_keys'] = $row->search_keys;
            $data['meta_description'] = $row->meta_description;
        }

        if (!isset($data)) {
            $data = "";
        }

        return $data;
    }

    function get_attachment_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['attachment'] = $row->attachment;
        }
        return $data;
    }

    function create() {
        $update_id = $this->uri->segment(4);
        //if($update_id != 1){
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $data = $this->get_data_from_db($update_id);
                //  var_dump($data);die;
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }

        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
        // }
//		}else{
//		redirect('admin/groups');
//		}
    }

    function submit() {
        $data = $this->get_data_from_post();

        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {

            $attach = $this->get_attachment_from_db($update_id);
            $uploadattachment = $this->file_upload($update_id);
            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
            if (empty($data['attachment'])) {
                $data['attachment'] = $attach['attachment'];
            }

            $this->_update($update_id, $data);
        } else {

            $nextid = $this->get_id();
//            die($nextid);
            $uploadattachment = $this->file_upload($nextid);
            die(print_r($uploadattachment));
            $data['attachment'] = $uploadattachment['upload_data']['file_name'];

            $this->_insert($data);
        }

        redirect('admin/sample_attachment');
    }

//    function do_upload($id) {
//        $config['upload_path'] = "./uploads/sample_attachment/";
//        $config['file_name'] = $id;
//        $config['overwrite'] = TRUE;
//        $config['allowed_types'] = "gif|jpg|jpeg|png";
//        $config['max_size'] = "20480"; //that's 20MB
//        $config['max_width'] = "1907";
//        $config['max_height'] = "1280";
//
//        $this->load->library('upload', $config);
//        if (!$this->upload->do_upload()) {
//            echo 'File cannot be uploaded';
//            $datas = array('error' => $this->upload->display_errors());
//            //$datas = array('upload_data' => $this->upload->data());	
//        } else {
//            echo 'File has been uploaded';
//            var_dump($config);
//            $datas = array('upload_data' => $this->upload->data());
//        }
//        return $datas;
//    }

    function get_id() {
        $this->load->model('mdl_sample_attachment');
        $id = $this->mdl_sample_attachment->get_id();
        return $id;
    }

    function get($id) {
        $this->load->model('mdl_sample_attachment');
        $query = $this->mdl_sample_attachment->get($id);
        return $query;
    }

    function get_where($id) {
        $this->load->model('mdl_sample_attachment');
        $query = $this->mdl_sample_attachment->get_where($id);
        return $query;
    }

    function _insert($data) {
        $this->load->model('mdl_sample_attachment');
        $this->mdl_sample_attachment->_insert($data);
    }

    function _update($id, $data) {
        $this->load->model('mdl_sample_attachment');
        $this->mdl_sample_attachment->_update($id, $data);
    }

    function delete() {
        $this->load->model('mdl_sample_attachment');
        $delete_id = $this->uri->segment(4);

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/sample_attachment');
        } else {
            $this->mdl_sample_attachment->_delete($delete_id);
            redirect('admin/sample_attachment');
        }
    }
   public function file_upload($id) {
    $files = $_FILES;
    $this->load->library('upload');

    $cpt = count ( $_FILES ['userfile'] ['name'] );
    for($i = 0; $i < $cpt; $i ++) {

        $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            
            $this->upload->initialize($this->set_upload_options($id, $i));
            $this->upload->do_upload();
             $fileName = $_FILES['userfile']['name'];
                 $images[] = $fileName;
                 
//            $datas[$i] = array('upload_data' => $this->upload->data());
        
//        $_FILES ['userfile'] ['name'] = $files ['userfile'] ['name'] [$i];
//        $_FILES ['userfile'] ['name'] = $files ['userfile'] ['type'] [$i];
//        $_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
//        $_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
//        $_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
//
//        $this->upload->initialize ( $this->set_upload_options () );
//        $this->upload->do_upload ();
//         $fileName = $_FILES['userfile']['name'];
//                 $images[] = $fileName;
//                  $fileName = implode(',',$images);
//                  
////                  print_r($fileName);die();
//                   $datas[$i] = array('upload_data' => $this->upload->data());

//                   $this->Mdl_sample_attachment->upload_image($fileName);
//                   $upload_data = $this->upload->data();
//		    $file_name 	=   $upload_data['file_name'];
//		    $file_type 	=   $upload_data['file_type'];
//		    $file_size 	=   $upload_data['file_size'];
                   
           }
            $fileName = implode(',',$images);
           
             $this->Mdl_sample_attachment->upload_image($fileName);
//             return $datas;
}
//private function set_upload_options() {
//    // upload an image options
//    $config = array ();
//    $config ['upload_path'] = "./uploads/sample_attachment/";
//    $config ['allowed_types'] = 'gif|jpg|png';
//    $config ['encrypt_name'] = TRUE;
//    $config['overwrite']=FALSE;
//
//    return $config;
//}
function set_upload_options($id, $i) {

        $config = array(); 
        $config['upload_path'] = './uploads/sample_attachment/';
        $config['allowed_types'] = 'text|plain|csv|gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar|csv';
        $config['max_size'] = '20480';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['file_name'] = $id.'attachment'.$i;
        return $config;
    }

}
