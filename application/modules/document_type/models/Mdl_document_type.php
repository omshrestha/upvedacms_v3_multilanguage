<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_document_type extends Mdl_crud{
     protected $_table = "up_document_type";
    protected $_primary_key = 'id';
    

    function __construct() {
        parent::__construct();
    }

//    function get_table() {
//        $table = "up_document_type";
//        return $table;
//    }

//    function _delete($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->delete($table);
//    }

//    function get($order_by) {
//        $table = $this->get_table();
//        $this->db->order_by($order_by, 'ASC');
//        $query = $this->db->get($table);
//        return $query;
//    }

//    function get_where($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $query = $this->db->get($table);
//        return $query;
//    }

//    function get_details($slug) {
//        $table = 'up_document_type';
//        $this->db->where('slug', $slug);
//        $query = $this->db->get($table);
//        return $query;
//    }

//    function _insert($data) {
//        $next_id = $this->get_id();
//
//        $table = $this->get_table();
//        $this->db->insert($table, $data);
//
//
//        $insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
//        $permission_table = 'up_permissions';
//        $this->db->insert($permission_table, $insert_null_permission_array);
//    }

    function get_id() {
        $result = $this->db->query("SHOW TABLE STATUS LIKE 'up_document_type'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment'];
        return $nextId;
    }

//    function _update($id, $data) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->update($table, $data);
//    }
//
    function get_document_type_id($slug,$language_id){
        $table = $this->_table;
	$this->db->where('language_id', $language_id);
        $this->db->where('slug', $slug);
         $query=$this->db->get('up_document_type')->result();
         foreach ($query as $row) {
             return $row->id;
         }
    }
    function get_dropdown($category_id=NULL,$language_id) {
        $this->db->select('id,title');
        $this->db->order_by('title');
        $this->db->where('language_id',$language_id);
        if($category_id != NULL){
        $this->db->where('category_id',$category_id);
        }
        $dropdowns = $this->db->get('up_document_type')->result();
        if($language_id==3){
        $dropdownlist[0]='please select';
        }else{
        $dropdownlist[0]='कृपया छानुहोस';    
        }
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }
//      function get_nepali_dropdown($category_id=NULL) {
//        $this->db->select('id,title');
//        $this->db->order_by('title');
//        $this->db->where('language_id',4);
//        if($category_id != NULL){
//        $this->db->where('category_id',$category_id);
//        }
//        $dropdowns = $this->db->get('up_document_type')->result();
//        $dropdownlist[0]='';
//        foreach ($dropdowns as $dropdown) {
//            $dropdownlist[$dropdown->id] = $dropdown->title;
//        }
//        if (empty($dropdownlist)) {
//            return NULL;
//        }
//        $finaldropdown = $dropdownlist;
//        return $finaldropdown;
//    }

//    function get_groups_dropdown() {
//        $this->db->select('id, title');
//        //$this->db->where('id > 1');
//        $this->db->order_by('id', 'AESC');
//        $dropdowns = $this->db->get('up_document_type')->result();
//        foreach ($dropdowns as $dropdown) {
//            //$dropdownlist[0] = '-- Select Panchakarma --';    
//            $dropdownlist[$dropdown->id] = $dropdown->title;
//        }
//        if (empty($dropdownlist)) {
//            return NULL;
//        }
//        $finaldropdown = $dropdownlist;
//        return $finaldropdown;
//    }

    function get_document_type($order_by) {
        $table = $this->get_table();
        $this->db->order_by('id', 'ASC');
        $this->db->where('status', 'live');
        $query = $this->db->get($table)->result_array();
        return $query;
    }
     function update_id_for_module_edit($lang_id,$slug){
        $table = $this->_table;
	$this->db->where('language_id', $lang_id);
        $this->db->where('slug', $slug);  
        $query=$this->db->get($table);
//      var_dump($query->result());die;
	return $query;  
        }

}
