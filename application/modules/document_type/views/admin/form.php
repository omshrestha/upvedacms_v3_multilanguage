
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce_4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript"> 
     
    tinymce.init({
      selector: "textarea",
      
      // ===========================================
      // INCLUDE THE PLUGIN
      // ===========================================
    	
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages",
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools"
      ],
    	
      // ===========================================
      // PUT PLUGIN'S BUTTON on the toolbar
      // ===========================================
    	
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | image",
      toolbar3: "print preview media | forecolor backcolor emoticons",
    	
      // ===========================================
      // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
      // ===========================================
      image_advtab: true,
	
      relative_urls: false
    	
    });
     
    </script>
<!-- /TinyMCE -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/css/datepicker.css' ?>" />
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() . 'bootstrap/datepicker/less/datepicker.less' ?>" />  
<script src="<?php echo base_url() . 'bootstrap/datepicker/js/bootstrap-datepicker.js' ?>"></script>

            <div              <!-- for only one save button -->
                    <?php $lang_count=0;
                    foreach($lang as $language)
                    {?>        

                    <?php $lang_count++;   
                    } ?>            
              <!-- end -->
<div class="row"> 
	<div class="col-md-12"> 
            <div class="tabbable tabbable-custom">
              <!--                code by for Tab-->
            <ul class="nav nav-tabs">
             <?php $i=1;
                foreach($lang as $language)
                {?>   
                    <li id="<?php echo $i ?>" class="<?php echo($i==1)?'active':'';?>"><a href="#tab_1_<?php echo $i;?>" data-toggle="tab"><?php echo $language;?></a></li> 
                <?php $i++;   
                } ?>            
              <!-- end of Tab --> 
            </ul>
          <!--form start -->  
            </div>
        <div class="widget box"> 
            <div class="widget-header"> 
                
            	<h4><i class="icon-reorder"></i>Sample</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/document_type/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>  
                <div class="tab-content">
              <?php $i=1; 
              foreach($lang as $key=>$language){ ?>
              
              <!-- content div with tab -->
            <div class="tab-pane <?php echo($i==1)?'active':'';?>" id="tab_1_<?php echo $i;?>">
                
                <div class="form-group"> 
                
                        <?php //echo form_input(array('name'=>'language_'.$language,'value'=>$key,'class'=>'form-control required', 'id'=>'language_'.$language.''));?>
                        <?php echo form_hidden('language_'.$language,$key);
                     ?>
                    
                        <?php echo form_hidden('update_id_'.$language,${'update_id_'.$language});?>
                </div>
                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php if(!isset(${'title_'.$language})){ ${'title_'.$language} = '';}
                         echo form_input('title_'.$language, ${'title_'.$language}, 'class="form-control"');
                        ?>
			</div>
                    </div>
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                             <?php if(!isset(${'description_'.$language})){$selected =${'description_'.$language}='';}?>
                        	<?php echo form_textarea(array('id' => 'elm'.$i,'name' =>'description_'.$language, 'value' => ${'description_'.$language},'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class="form-control required"'));?>
                        </div> 
		</div>
                <div class="form-group"> 
                        <label class="col-md-2 control-label">Meta Description</label> 
                       <div class="col-md-10">
                             <?php if(!isset(${'meta_description_'.$language})){$selected =${'meta_description_'.$language}='';}?>
                        	<?php echo form_textarea(array('id' => 'elm1'.$i,'name' =>'meta_description_'.$language, 'value' => ${'meta_description_'.$language},'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class="form-control required"'));?>
                        </div> 
		</div>
                 <div class="form-group"> 
                        <label class="col-md-2 control-label">Meta Key</label> 
                       <div class="col-md-10">
                             <?php if(!isset(${'meta_key_'.$language})){$selected =${'meta_key_'.$language}='';}?>
                        	<?php echo form_textarea(array('id' => 'elm1'.$i,'name' =>'meta_key_'.$language, 'value' => ${'meta_key_'.$language},'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class="form-control required"'));?>
                        </div> 
		</div>
                 <div class="form-group"> 
                        <label class="col-md-2 control-label">Category</label> 
                       <!--<div class="col-sm-8 booknow_data input-append date dp" id="datepicker" data-date="" data-date-format="yyyy-mm-dd">--> 
                       <div class="col-md-10">
                                <?php 
                                $selected=${'category_'.$language};
                                if($i==1){
                                    $options = $category_array_english;
                                }else{
                                    $options=$category_array_nepali;
                                }      
                        echo form_dropdown('category_'.$language, $options, $selected,'class="form-control"');?>
                        
                                <span class="add-on">
           <!--                         <i class="icon-th"></i>-->
                                </span>
                            </div> 
		</div>
     
                    <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                         <div class="col-md-10"> 
                        <?php $selected = ${'status_'.$language};$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                        echo form_dropdown('status_'.$language, $options, $selected,'class="form-control"');?>
                    </div> 
                    </div>
                 <?php 
                    if($i==$lang_count){					
                        echo form_submit('submit','Save','class="btn btn-primary pull-right"'); //name,value...type is default submit 
                   
                        }
                ?>
                                  
            </div>
               <?php $i++;
                   
              } ?>
                </div>
                <?php echo form_close(); ?>      
                <script>
$('#datepicker').datepicker();
$('#datepicker2').datepicker();
</script>
            </div> 
        </div> 
    </div>
</div>
<!--
    <script type="text/javascript" src="<?php echo base_url()."bootstrap/datetimepicker/jquery/jquery-1.8.3.min.js"?>" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()."bootstrap/js/bootstrap.min.js"?>"></script>
<script type="text/javascript" src="<?php echo base_url()."bootstrap/datetimepicker/js/bootstrap-datetimepicker.js"?>" charset="UTF-8"></script>-->

<!--<script type="text/javascript">
    $('.form_datetime').datetimepicker({
         weekStart: 0,
         todayBtn:  1,
 	 autoclose: 1,
 	 todayHighlight: 1,
 	 startView: 2,
 	 forceParse: 0,
         showMeridian: 1
     });
</script>  -->
<script>
    $('#datepicker').datepicker();
</script> 