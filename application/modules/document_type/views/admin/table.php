<!--<script type="text/javascript">                                         
     function ajaxFunctionName() {
                 // alert("change"); 
             
               // alert(slug);
$('#lang').click(function(event) {
                var slug =event.target.id;
                 // var slug = event.target.id;  
                //  alert(slug);
            $.ajax({
                type:'POST',
                url:'<?php echo base_url(); ?>pages',
                data:"slug=" + slug,
                
                success:function()
                {
//                    alert(result);
                    window.location.reload(true);
                }

            });
            });
                }
 </script>  -->
<!--<div class="col-lg-3">
                             code by j@y  for language section 

    <ul id="lang">
<?php
$count = count($language_list);
for ($i = 0; $i < $count; $i++) {
    ?>

                <li class="icon btn btn-active" type= "button" onclick="ajaxFunctionName()" id="<?php echo $language_list[$i]->short_language; ?>"><?php echo $language_list[$i]->short_language; ?></li>

<?php } ?>
    </ul>
                 /* end of language selection   */

</div>-->
<div class="manage">
    <?php if (isset($permission['add'])) { ?>
        <input type="button" value="Add Document Type" id="create" onclick="location.href = '<?php echo base_url() ?>admin/document_type/create';"/>
<?php } ?>
</div>
<!--<div class="col-sm-2">
<?php // echo form_input('title', '', 'class="form-control searchKeys" data-type="varchar" id="title" placeholder="Title"'); ?>
</div>-->
<div class="col-sm-2">
<?php echo form_input('description', '', 'class="form-control searchKeys" data-type="varchar" id="description" placeholder="description"'); ?>
</div>
<div class="col-sm-2">
<?php echo form_button('submit', 'Search', 'class="btn btn-primary" id="search"'); ?>
</div>


<div class="widget box" id="replaceTable"> 
    <div><?php
        if (($this->session->flashdata('operation'))) {
            echo $this->session->flashdata('operation');
        }
        ?></div>

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i>Document Type</h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive  tablesorter "id="tblData"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                        <?php foreach ($columns as $column) { ?>
                        <th><?php
                            $this->lang->load('sample', 'english');
                            echo $this->lang->line(implode(' ', explode('_', $column)));
                            ?></th>
<?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
<?php $sno = 1; ?>
<?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                            <?php foreach ($columns as $column) {
                                ?><td><?php echo Ucfirst($row->$column); ?></td><?php } ?>
                        <td class="edit">
    <?php if (isset($permission['view'])) { ?><a href="<?php echo base_url() ?>admin/document_type/view/<?php echo base64_encode($row->id); ?>"><i class="icon-eye-open"></i></a><?php } ?>
                            <?php if (isset($permission['edit'])) { ?>&nbsp;/&nbsp; 
                                <a href="<?php echo base_url() ?>admin/document_type/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i>परिवर्तन गर्नुहोस</a> <?php } ?>                  

    <?php if (isset($permission['delete'])) { ?>&nbsp;/&nbsp; <a href="<?php echo base_url() ?>admin//delete/<?php echo base64_encode($row->id); ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i>खारेज गर्नुहोस</a><?php } ?>
                        </td> 
                    </tr> 

<?php } ?>                
            </tbody> 
        </table> 
        <div id="pagination">
            <div class="col-sm-2">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="col-sm-6">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>
            </ul>
        </div>
    </div>

</div>