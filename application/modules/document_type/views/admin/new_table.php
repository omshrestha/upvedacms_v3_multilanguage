
<div class="widget box" id="replaceTable"> 
<div><?php
        if (($this->session->flashdata('operation'))) {
            echo $this->session->flashdata('operation');
        }
        ?></div>
    
    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i>Sample</h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content" id="replaceTable"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <?php foreach ($columns as $column) {?>
                        
                        <th><?php 
                        $this->lang->load('sample','english');
                        echo $this->lang->line(implode(' ', explode('_', $column))); ?></th>
                    <?php } ?>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = 1; ?>
                <?php foreach ($query->result() as $row) { ?>

                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <?php foreach ($columns as $column) {
                            ?><td><?php echo Ucfirst($row->$column); ?></td><?php } ?>
                        <td class="edit">
                            <?php if (isset($permission['view'])) { ?><a href="<?php echo base_url() ?>admin/sample/view/<?php echo base64_encode($row->id); ?>"><i class="icon-eye-open"></i></a><?php } ?>
                            <?php if (isset($permission['edit'])) { ?>&nbsp;/&nbsp; 
                                <a href="<?php echo base_url() ?>admin/sample/create/<?php echo base64_encode($row->id); ?>"><i class="icon-pencil"></i></a> <?php } ?>                  

                            <?php if (isset($permission['delete'])) { ?>&nbsp;/&nbsp; <a href="<?php echo base_url() ?>admin/sample/delete/<?php echo base64_encode($row->id); ?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a><?php } ?>
                        </td> 
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
    <div id="pagination">
            <div class="col-sm-2">
                <input type='text' id="goto" class='form-control' placeholder="Goto Page Number"/>
            </div>
            <div class="col-sm-6" style="margin-left: 30px;">
                <span class='total' a="<?php echo $total_page; ?>" b="<?php echo $theUrl; ?>">Showing Page <b><?php echo $page; ?></b> of <b><?php echo $total_page; ?></b></span>
            </div>
            <ul class="pager">
                <?php
                if (!empty($page_links)) {
                    echo $page_links;
                }
                ?>

            </ul>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>design/admin/js/searchPagination.js"></script>