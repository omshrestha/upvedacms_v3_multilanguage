<?php
if(!isset($message))
{
    $message=''; 
}
else 
{
    $message=$message;
}
?>
<script>
$( document ).ready(function() {
    var error='<?php echo $message;?>';
   if(error=='1')
   {
//      alert(error);
        
        $('#error').html("Please Enter CAPTCHA Correctly!!");
         
    }
    else if(error=='0')
    {
       $('#first_name').val('');
       $('#last_name').val('');
       $('#email').val('');
       $('#emailbody').val('');
       $('#mailsubject').val('');
       $('#msg').val('');
//       $('#volunteer_duration').val('');
//       $('#dob').val('');
//       $('#experience').val('');
        $('#success').html("Message Sent Successfully!!");
    }
    
});
</script>
<div class="container">
<div class="col-lg-7 well"style="margin-top: 10px;">
    <div class="contact_us_form">
      <!-- <h3>Contact Us</h3>-->
       <h2><strong>Contact Everest Aid</strong></h2>
            <?php
            echo form_open_multipart('contactus/submit', 'id="validate-1"');
            ?>
       <div id="error" style="color: red;"></div>
       <div id="success" style="color: green;"></div>
              <div class="form-group"> <label>First Name : </label>
                <?php echo form_input('name', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter first name"');?> 
              </div>
       <div class="form-group">  <label>Last Name : </label>
                <?php echo form_input('last_name', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter last name"');?> 
              </div>
              <div class="form-group"> <label>Email : </label>
                <?php echo form_input('email', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter Email"');?> 
              </div>
              <div class="form-group"> <label>Subject: </label>
                <?php echo form_input('mailsubject', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter subject"');?> 
              </div>
              <div class="form-group"><label>Enter text message : </label>
                <?php
                   $emailbody = array(
                            'name'				=> 'msg',
                            'id'				=> 'emailbody',
                            'class'				=> 'form-control',
                            'placeholder'		=> 'Type the text to send'
                            );
                    echo form_textarea($emailbody);
                    ?>
              </div>
              <?php 
                echo $recaptcha_html;
                echo form_submit('save','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 

                ?>
              <div class="clearfix"></div>
            <?php echo form_close() ?> 
                <hr/>
    </div>
</div>
    <div class="col-lg-5">
    <div class=" contact_both">
            <div class="contact_info">
                <h5>
                    <strong style="text-align:justify; color:#5c8d1c;">
                       Everest Aid
                    </strong>
                </h5>
                <p style="font-size:16px;">Kathmandu</p> 


                <p class="author" style="text-align: justify;">
                    <strong>
                    Tel No: 01-4111111
                    Fax: 01-4111111
                    </strong>
                </p>
                <p>
                    <strong>
                    E-mail: info@everestaid.com.np
                    </strong>
                </p>

            </div>
            
        </div>
        </div>
    </div>
<script>
     $('#button').click(function()
    {
     $('#validate-1').submit();

    });
    
</script>