<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_language extends Mdl_crud {

    protected $_table = "up_language";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }   
     function get($order_by){
	$table = $this->_table;
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
        }

    function get_id() {
        $nextId = $row['Auto_increment'];
        $result = $this->_custom_query("SHOW TABLE STATUS LIKE 'up_language'");
        $row = $result->result();
        $nextId = $row[0]->Auto_increment;

        return $nextId;
    }

    function get_language($language_id){
        $table = $this->_table;
	$this->db->where('id', $language_id);
	$query=$this->db->get($table);
	return $query->result();    
        }

    function get_banner_dropdown() {
        $this->db->select('id, title');
        $this->db->order_by('title');
        $dropdowns = $this->db->get('language')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }
    function get_language_id($language){
        $table = $this->_table;
	$this->db->where('short_language', $language);
	$query=$this->db->get($table);
	return $query;
        }
        function get_language_tab(){
        $this->db->select('id, language');
        $this->db->order_by('id','ASC');
        $dropdowns = $this->db->get('up_language')->result();
        foreach ($dropdowns as $dropdown)
        {
        $dropdownlist[$dropdown->id] = $dropdown->language;
        }
        if(empty($dropdownlist)){return NULL;}
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
        }
       
}
