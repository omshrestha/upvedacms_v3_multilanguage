<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $model_name = 'Mdl_settings';
    private $update_id = 1;

    public function __construct() {
        parent::__construct();

        $this->load->library('Common_functions');
        $this->load->model('Mdl_settings');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('settings'); //module name is settings here	
    }

    function index() {
        if ($this->input->post('module_id')) {
            $module_id = $this->input->post('module_id');
        } else {
            $module_id = 1;
        }
        $select = 'site_name,meta_topic,meta_data,contact_email,date_format,frontend_enabled,unavailable_message,favicon,logo,per_page';
        $data['cms'] = $this->common_functions->get_data_from_db($this->update_id, $select, $this->model_name);
        $data['modulelist'] = $this->common_functions->get_dropdown('title', 'id,title', '', 'mdl_moduleslist');
        $data['module_id'] = $module_id;
        $column = $this->get_columns_from_module_id($module_id);
        $data['columns'] = $column['column'];
        $data['module_name'] = $column['module'];
        if (!$this->input->post('module_id')) {
            $data['view_file'] = "admin/form";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_form', $data);
        }
    }

    function get_data_from_post() {
        $data['site_name'] = $this->input->post('site_name', TRUE);
        $data['meta_topic'] = $this->input->post('meta_topic', TRUE);
        $data['meta_data'] = $this->input->post('meta_data', TRUE);
        $data['contact_email'] = $this->input->post('contact_email', TRUE);
        $data['date_format'] = $this->input->post('date_format', TRUE);
        $data['frontend_enabled'] = $this->input->post('frontend_enabled', TRUE);
        $data['unavailable_message'] = $this->input->post('unavailable_message', TRUE);
        //print_r($data);die();
        return $data;
    }

    function submit() {
        $data = $this->get_data_from_post();
        $this->Mdl_settings->_update($this->update_id, $data);
        redirect('admin/settings');
    }

    function submit_favicon() {
        $attach = $this->get_attachment_from_db('favicon');
        $uploadattachment = $this->do_upload('favicon');
        $data['favicon'] = $uploadattachment['upload_data']['file_name'];
        if (empty($data['favicon'])) {
            $data['favicon'] = $attach['favicon'];
        }
        $this->Mdl_settings->_update($this->update_id, $data);
        redirect('admin/settings/#tab_1_2');
    }

    function submit_logo() {
        $attach = $this->get_attachment_from_db('logo');
        $uploadattachment = $this->do_upload('logo');
        $data['logo'] = $uploadattachment['upload_data']['file_name'];
        if (empty($data['logo'])) {
            $data['logo'] = $attach['logo'];
        }

        $update_id = 1; //this always stays 1 because we have only 1 row for site settings		
        $this->Mdl_settings->_update($update_id, $data);
        redirect('admin/settings/#tab_1_3');
    }

    function get_attachment_from_db($name) {
        $query = $this->Mdl_settings->get_where($name);
        foreach ($query->result() as $row) {
            $data[$name] = $row->$name;
        }
        return $data;
    }

    function do_upload($name) {
        $config['upload_path'] = "./uploads/settings/";
        $config['file_name'] = $name;
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = "png|ico";
        $config['max_size'] = "5120"; //that's 5MB
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";

        $this->load->library('upload', $config);


        if (!$this->upload->do_upload()) {
            //echo 'File cannot be uploaded';
            $datas = array('error' => $this->upload->display_errors());
        } else {
            echo 'File has been uploaded';
            $datas = array('upload_data' => $this->upload->data());
        }

        return $datas;
    }

    function get_data_of_module_settings_from_post() {
        $data['per_page'] = $this->input->post('per_page', TRUE);
        $data['module_id'] = $this->input->post('module_id', TRUE);
        $column = $this->get_columns_from_module_id($data['module_id']);
        foreach ($column['column'] as $key => $value) {
            $data[$value] = $this->input->post($value, TRUE);
        }
        return $data;
    }

    function submit_module_settings() {
        $data = $this->get_data_of_module_settings_from_post();
        if ($data['module_id'] == 1) {
            if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/banner_lang.php', 'a+') or die("Unable to open file!!!!");
        } else if ($data['module_id'] == 2) {
            if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/modules_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        else if ($data['module_id'] == 3) {
            if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/navigation_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        else if($data['module_id']==4){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/pages_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        else if($data['module_id']==15){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/dynamic_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        else if($data['module_id']==5){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/groups_lang.php', 'a+') or die("Unable to open file!!!!");
        }
         else if($data['module_id']==6){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/permissions_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        else if($data['module_id']==13){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/sample_lang.php', 'a+') or die("Unable to open file!!!!");
        }
         else if($data['module_id']==17){
        if ($data['per_page'] != '') {
                $temp['per_page'] = $data['per_page'];
                $this->Mdl_settings->_update($this->update_id, $temp);
            }
            $file = fopen($_SERVER['DOCUMENT_ROOT'].'/upvedacms_v3/application/language/english/language_lang.php', 'a+') or die("Unable to open file!!!!");
        }
        
        $column = $this->get_columns_from_module_id($data['module_id']);
        foreach ($column['column'] as $key => $value) {
            if ($data[$value] != '') {
                $this->lang->load($column['module'], 'english');
                if ($data[$value] != $this->lang->line($key)) {
                    fwrite($file, '$lang["' . $value . '"]="' . $data[$value] . '";' . "\n");
                }
            }
        }
        fclose($file);
        redirect('admin/settings/#tab_1_4');
    }

    function get_columns_from_module_id($id) {
        if ($id == 1) {
            $columns['column'] = array('title' => 'title', 'sub title' => 'sub title', 'attachment' => 'attachment', 'status' => 'status');
            $columns['module'] = 'banner';
            return $columns;
        } else if ($id == 2) {
            $columns['column'] = array('modules' => 'modules');
            $columns['module'] = 'modules';
            return $columns;
        }
        elseif($id== 3){
            $columns['column']=array('title'=>'title');
            $columns['module']='navigation';
            return $columns;
            
        }
        elseif($id== 4){
            $columns['column']=array('title'=>'title','description'=>'description','attachment'=>'attachment','status'=>'status');
            $columns['module']='pages';
            return $columns;
            
        }
        elseif($id==15){
              $columns['column']=array('fname'=>'firstname','lname'=>'lastname','technology'=>'technology','email'=>'email','address'=>'address','status'=>'status');
            $columns['module']='pages';
            return $columns;
            
        }
        elseif($id==5){
            $columns['column']=array('name'=>'name','set permission'=>'set permission');
            $columns['module']='groups';
            return $columns;
            
        }
        elseif($id==6){
            $columns['column']=array('name'=>'name','set permission'=>'set permission');
            $columns['module']='permissions';
            return $columns;
            
        }
         elseif($id==13){
            $columns['column']=array('title'=>'title','description'=>'description','status'=>'status');
            $columns['module']='permissions';
            return $columns;
        
        
    }
     elseif($id==17){
            $columns['column']=array('language'=>'language','short language'=>'short language');
            $columns['module']='permissions';
            return $columns;
        
        
    }

}
}
