<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Site Settings</h4> 
                <div class="toolbar no-padding"> 
                    <div class="btn-group"> 
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span> 
                    </div> 
                </div> 
            </div> 
            <div class="widget-content" id="replace"> 
                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="tabbable tabbable-custom"> 
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">General Settings</a></li> 
                                <li><a href="#tab_1_2" data-toggle="tab">Change Favicon</a></li> 
                                <li><a href="#tab_1_3" data-toggle="tab">Change Logo</a></li> 
                                <li><a href="#tab_1_4" data-toggle="tab">Change Module Settings</a></li> 
                            </ul> 
                            <div class="tab-content"> 
                                <div class="tab-pane active" id="tab_1_1">                                 

                                    <div class="widget-content">
                                        <?php
                                        echo validation_errors('<p style="color: red;">', '</p>');
                                        echo form_open_multipart('admin/settings/submit', 'class="form-horizontal row-border" id="validate-1"');
                                        ?>                
                                        <div class="form-group">                     	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>The name of the website for page titles and for use around the site.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Site Name <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php echo form_input('site_name', $cms['site_name'], 'class="form-control required"'); ?>
                                            </div> 
                                        </div>    

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>Two or three words describing this type of company/website.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Meta Topic <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php echo form_input('meta_topic', $cms['meta_topic'], 'class="form-control required"'); ?>
                                            </div> 
                                        </div>

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>Meta Data that gives some information about this company/website.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Meta Data <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php echo form_input('meta_data', $cms['meta_data'], 'class="form-control required"'); ?>
                                            </div> 
                                        </div>

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>All e-mails from users, guests and the site will go to this e-mail address.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Contact E-mail <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php echo form_input('contact_email', $cms['contact_email'], 'class="form-control required email"'); ?>
                                            </div> 
                                        </div>

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>How should dates be displayed across the website? Using the <a target="_blank" href="http://php.net/manual/en/function.date.php">date format</a> from PHP - OR - Using the format of <a target="_blank" href="http://php.net/manual/en/function.strftime.php">strings formatted as date</a> from PHP.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Date Format <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php echo form_input('date_format', $cms['date_format'], 'class="form-control required"'); ?>
                                            </div> 
                                        </div>

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>Use this option to the user-facing part of the site on or off. Useful when you want to take the site down for maintenence</small>
                                            </div>
                                            <label class="col-md-2 control-label">Site Status <span class="required">*</span></label> 
                                            <div class="col-md-10"> 
                                                <?php
                                                echo '<label class="radio">' . form_radio('frontend_enabled', 'open', $cms['frontend_enabled'] == 'open', 'class="required"') . ' Open</label>';
                                                echo '<label class="radio">' . form_radio('frontend_enabled', 'closed', $cms['frontend_enabled'] == 'closed') . ' Closed</label>';
                                                ?>
                                            </div> 
                                        </div>

                                        <div class="form-group">                    	
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>When the site is turned off or there is a major problem, this message will show to users.</small>	
                                            </div>
                                            <label class="col-md-2 control-label">Unavailable Status <span class="required">*</span></label> 
                                            <div class="col-md-10">                         	
                                                <?php
                                                echo form_textarea(array('name' => 'unavailable_message', 'value' => $cms['unavailable_message'], 'rows' => '10', 'cols' => '5', 'class' => 'form-control required'));
                                                ?>           
                                            </div> 
                                        </div>

                                        <div class="form-actions"> 
                                            <?php
                                            echo form_submit('submit', 'Save', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                                            ?>
                                        </div>                 

                                        <?php echo form_close(); ?>                
                                    </div>

                                </div> 
                                <div class="tab-pane" id="tab_1_2">       
                                    <div class="col-md-12">  
                                        <?php
                                        echo validation_errors('<p style="color: red;">', '</p>');
                                        echo form_open_multipart('admin/settings/submit_favicon', 'class="form-horizontal row-border" id="validate-1"');
                                        ?>                             


                                        <div class="form-group">

                                            <div class="col-md-3"> 
                                                <div class="list-group"> 
                                                    <li class="list-group-item no-padding"> 
                                                        <img src="<?php echo base_url(); ?>uploads/settings/<?php echo $cms['favicon'] ?>" alt="favicon" style='height:32px'> 
                                                    </li> 
                                                </div> 
                                            </div> 


                                            <label class="col-md-2 control-label">Favicon <span class="required">*</span></label> 
                                            <div class="col-md-7"> 
                                                <?php
                                                $update_id = 1;
                                                $attachment = '12.jpg';
                                                if (!empty($update_id)) {

                                                    $attach_prop = array(
                                                        'type' => 'file',
                                                        'name' => 'userfile',
                                                        'value' => $attachment
                                                    );
                                                } else {
                                                    $attach_prop = array(
                                                        'type' => 'file',
                                                        'name' => 'userfile',
                                                        'value' => $attachment,
                                                        'class' => 'required'
                                                    );
                                                }
                                                ?>

                                                <?php echo form_upload($attach_prop); ?>
                                                <p class="help-block">
                                                    .ico images only (Example:favicon.ico)</p>
                                            </div>

                                        </div>

                                        <div class="form-actions"> 
                                            <?php
                                            echo form_submit('submit', 'Save', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                                            ?>
                                        </div>  


                                        <?php echo form_close(); ?>      
                                    </div>                             

                                </div> 
                                <div class="tab-pane" id="tab_1_3">                                

                                    <div class="col-md-12">  
                                        <?php
                                        echo validation_errors('<p style="color: red;">', '</p>');
                                        echo form_open_multipart('admin/settings/submit_logo', 'class="form-horizontal row-border" id="validate-1"');
                                        ?>                             


                                        <div class="form-group">

                                            <div class="col-md-3"> 
                                                <div class="list-group"> 
                                                    <li class="list-group-item no-padding"> 
                                                        <img src="<?php echo base_url(); ?>uploads/settings/<?php echo $cms['logo'] ?>" alt="logo"> 
                                                    </li> 
                                                </div> 
                                            </div> 


                                            <label class="col-md-2 control-label">Logo <span class="required">*</span></label> 
                                            <div class="col-md-7"> 
                                                <?php
                                                $update_id = 1;
                                                $attachment = '12.jpg';
                                                if (!empty($update_id)) {

                                                    $attach_prop = array(
                                                        'type' => 'file',
                                                        'name' => 'userfile',
                                                        'value' => $attachment
                                                    );
                                                } else {
                                                    $attach_prop = array(
                                                        'type' => 'file',
                                                        'name' => 'userfile',
                                                        'value' => $attachment,
                                                        'class' => 'required'
                                                    );
                                                }
                                                ?>

                                                <?php echo form_upload($attach_prop); ?>
                                                <p class="help-block">
                                                    .png images only (Example:logo.png)</p>
                                            </div>

                                        </div>

                                        <div class="form-actions"> 
                                            <?php
                                            echo form_submit('submit', 'Save', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                                            ?>
                                        </div>  


                                        <?php echo form_close(); ?>      
                                    </div> 

                                </div>
                                <div class="tab-pane" id="tab_1_4">                                 
                                    <div class="widget-content">
                                        <?php
                                        echo validation_errors('<p style="color: red;">', '</p>');
                                        echo form_open_multipart('admin/settings/submit_module_settings', 'class="form-horizontal row-border" id="validate-1"');
                                        ?>    
                                        <div id="ifModule" class="form-group">
                                            <label class="col-md-2 control-label">Module</label> 
                                            <div class="col-md-10"> 
                                                <?php
                                                $selected = $module_id;
                                                $options = $modulelist; //moduleslist array has moduleslists from tbl_moduleslists
                                                echo form_dropdown('module_id', $options, $selected, 'class="form-control" id="module_id"');
                                                ?>
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2"> </div>
                                            <div class="col-md-10"> 
                                                <small>The number of data to be shown in the module table.</small>
                                            </div>
                                            <label class="col-md-2 control-label">Entries per page </label> 
                                            <div class="col-md-10"> 
                                                <?php
                                                $page = $cms['per_page'] != NULL ? $cms['per_page'] : '';
                                                echo form_input('per_page', $page, 'class="form-control"');
                                                ?>
                                            </div> 
                                        </div>

                                        <?php
                                        foreach ($columns as $key => $value) {
                                            ?>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">In Place of <?php
                                                    $this->lang->load($module_name, 'english');
                                                    $val = $this->lang->line($key);
                                                    echo $key;
                                                    ?> show</label> 
                                                <div class="col-md-10"> 
                                                    <?php echo form_input($value, $val, 'class="form-control"'); ?>
                                                </div> 
                                            </div><?php
                                        }
                                        ?>

                                        <div class="form-actions"> 
                                            <?php
                                            echo form_submit('submit', 'Save', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                                            ?>
                                        </div>                 

                                        <?php echo form_close(); ?>                
                                    </div>
                                </div>
                                <script>
                                    var e = document.getElementById('module_id');
                                    var selectedModule = e.options[e.selectedIndex].value;
                                    $('#ifModule').change(function () {
                                        selectedModule = e.options[e.selectedIndex].value;
                                        $.ajax({
                                            type: 'POST',
                                            data: 'module_id=' + selectedModule,
                                            url: 'http://localhost/upvedacms_v3/admin/settings',
                                            success: function (data) {
                                                $('#replace').html(data);
                                            }
                                        });
                                    });

                                </script>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</div>