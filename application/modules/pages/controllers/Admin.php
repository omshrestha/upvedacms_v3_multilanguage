<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/pages";
    private $MODULE = "pages";
    private $group_id;
    private $model_name = 'Mdl_pages';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('Mdl_pages');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('pages'); //module name is pages here	
    }

    function index() {
       
        //table select parameters
        $main_table_params = 'id,title,description,attachment,status';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
//            var_dump($params);die;
        } else {
            $params = '';
        }
        $count = $this->Mdl_pages->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }

        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] = 5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_pages->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['columns'] = array('title', 'description', 'attachment', 'status');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
//            die('hh');
            $this->load->view('admin/new_table', $data);
        }
    }

    function get_data_from_post() {
//        var_dump($_POST); die;
         $data1['lang'] = $this->get_language_tab();
//         print_r($data1['lang']);die;
         
          $update_id = $this->input->post('update_id_english', TRUE);
            
                if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
                         foreach ($data1['lang'] as $key => $language) {
                         
                         
         $data['attachment_' . $language] = $this->input->post('userfile', TRUE);
        $data['title_'.$language] = $this->input->post('title_'.$language, TRUE);
        $data['slug_'.$language] = strtolower(url_title($data['title_english']));
        $data['description_'.$language] = $this->input->post('description_'.$language, TRUE);
        $data['option_'.$language] = $this->input->post('option_'.$language, TRUE);
       $data['search_keys_'.$language] = $this->input->post('search_keys_'.$language, TRUE); 
       $data['language_id_'.$language]=$this->input->post('language_'.$language,TRUE); 
        $data['meta_description_'.$language] = $this->input->post('meta_description_'.$language, TRUE);
        $data['status_'.$language] = $this->input->post('status_'.$language, TRUE);
        $data['update_id_'.$language]= $this->input->post('update_id_'.$language, TRUE);
//        if (is_numeric($update_id)) {
//            $attach = $this->get_attachment_from_db($update_id);
//            $data['attachment'] = $attach['attachment'];
//            $data['upd_date'] = date("Y-m-d");
//        } else {
//            $data['attachment'] = $this->input->post('userfile', TRUE);
//            $data['ent_date'] = date("Y-m-d");
//            $data['upd_date'] = NULL;
//        }
          }
//          var_dump($data);die;
        return $data;
//        var_dump($data);die;
   
}
    	
	function get_data_from_db($update_id)
	{
//            echo'hh';die;
                $data1['lang'] = $this->get_language_tab();
//                print_r($data1);die;
                
                $query = $this->Mdl_pages->get_where_dynamic($update_id);
//                var_dump($query->result());die;
		foreach($query->result() as $slugg)
		{
                    $slug = $slugg->slug;
		}
                foreach($data1['lang'] as $key=>$language){
                $langu_id = $key;
//                print_r($langu_id);die;
                $update_id = $this->update_id_for_module_edit($langu_id,$slug);
//                var_dump($update_id);die;
                
                if(isset($update_id)) {    
//                    die('here');
		$query = $this->Mdl_pages->get_where_dynamic($update_id);
//                var_dump($query->result());die;
                    foreach($query->result() as $row)
                    {
                        $language_id = $row->language_id;
                        $language = $this->get_language($language_id);
                        $data['title_'.$language] = $row->title;
                        $data['description_'.$language] = $row->description;
                        $data['status_'.$language] = $row->status;	
                        $data['language_id_'.$language] = $row->language_id;
                        $data['option_'.$language] = $row->option;
                        $data['search_keys_'.$language] = $row->search_keys;
                        $data['meta_description_'.$language] = $row->meta_description;
                        $data['attachment_'.$language] = $row->attachment;
                        $slug = $row->slug;
                    }
                    
//                    var_dump($data);die;
                     $data['update_id_'.$language] = $update_id;
                     
                }
             else{
                 
                        $language_id = $langu_id;
                        $language = $this->get_language($language_id);
                        $data['title_'.$language]=$datanew['title'] = $slug.'_'.$language;
                        $data['slug_'.$language]=$datanew['slug'] = $slug;
                        $data['description_'.$language]=$datanew['description'] = '';
                        $data['status_'.$language]=$datanew['status'] = 'live';	
                        $data['language_id_'.$language]=$datanew['language_id'] = $language_id;
                         $data['option_'.$language] = $this->input->post('option_'.$language, TRUE);
                        $data['search_keys_'.$language] = $this->input->post('search_keys_'.$language, TRUE);
                        $data['meta_description_'.$language] = $this->input->post('meta_description_'.$language, TRUE);
//                        $data['attachment_' . $language] = $datanew['attachment'] = $attachment . '_' . $language;
                        $data['attachment_'.$language] = $this->input->post('attachment_'.$language, TRUE);
                        $datanew['ent_date'] = date("Y-m-d");
                        $datanew['upd_date'] = NULL;
                        $update_id = $this->Mdl_pages->_insert($datanew);
                        $data['update_id_'.$language] = $update_id; 
             }
                }
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}

    function create() {
        $update_id = base64_decode($this->uri->segment(4));
//        var_dump($update_id);die;
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
//person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,description,meta_description,search_keys,ent_date,status,attachment,option';
//               $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
//                var_dump($select);die;
               $data=$this->get_data_from_db($update_id);
//                var_dump($data); die;
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['lang'] = $this->get_language_tab();
//        var_dump($data);die;
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
       // $this->load->model('mdl_pages');
        $delete_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        if ($delete_id != 1) {

            if (!isset($delete_id) || !is_numeric($delete_id)) {
                unset($delete_id);
                redirect('admin/pages');
            } else {

                if ($group_id != 1) {
                    if (is_numeric($view_id)) {
                if($permission['delete']==FALSE){
                    redirect('admin/dashboard');
                }
                else{
                     $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                }
                

               
                } else {
                     if ($permission['delete'] == false) {
                    redirect('admin/dashboard');
                    
                }
            }
                } else {
                    $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
                    $this->mdl_pages->_delete($delete_id);
                    $this->session->set_flashdata('operation', 'Deleted Successfully!!! ' . $message);
                    redirect('admin/pages');
                }
            }
        } else {
            redirect('admin/groups');
        }
    }

    function view() {
        $update_id = base64_decode($this->uri->segment(4));
        $view_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $permission = $this->common_functions->check_permission($this->group_id, $this->MODULE);
       $select = 'title,attachment,description,status,meta_description,option,search_keys';
        if ($group_id != 1) {
            if (is_numeric($view_id)) {
                if($permission['view']==FALSE){
                    redirect('admin/dashboard');
                }
                else{
                     $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                }
                

               
                } else {
                     if ($permission['view'] == false) {
                    redirect('admin/dashboard');
                    
                }
            }
        }
        $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
        $data['view_id'] = $view_id;
        $data['view_file'] = "admin/view";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
        $this->load->model('mdl_pages');
        $dataa = $this->get_data_from_post();
        $update_id = $this->input->post('update_id_english', TRUE);

        if (is_numeric($update_id)) {
            
         $query = $this->Mdl_pages->get_where_dynamic($update_id);

		foreach($query->result() as $slugg)
		{
                    $slug = $slugg->slug;
		}
             $data1['lang'] = $this->get_language_tab();

            foreach($data1['lang'] as $key=>$language){
                $langu_id = $key;

                 $update_id = $this->update_id_for_module_edit($langu_id,$slug);
                $attach[$language] = $this->get_attachment_from_db($update_id);
            }
           
             
            $uploadattachment = $this->common_functions->do_upload($update_id,$data1['lang'],$this->MODULE);

           /*we have defined i bcoz of the language id starting from 3*/
            $i=0;
            foreach ($data1['lang'] as $key => $value) {
                $data_attach['attachment_'.$value] = $uploadattachment[$i]['upload_data']['file_name']; 
             $attachment= explode('.', $data_attach['attachment_'.$value]);
              
                if( !isset($attachment[1])){
                    /*for updating the image without uploading after edit having next id except english*/
                $data_attach['attachment_'.$value]=$attach[$value]['attachment_'.$value];
                    
                }
                $i++;
            }
           
                
            $permission = $this->common_functions->check_permission($this->group_id,$this->MODULE);
            
            if (isset($permission['edit'])) {
                 foreach ($data1['lang'] as $key => $language) {

                     $data['title'] = $dataa['title_' . $language];
                     $data['description']=$dataa['description_'.$language];
                      $data['slug']=$dataa['slug_'.$language];
                       
                        $data['status']=$dataa['status_'.$language];	
                        $data['language_id']=$dataa['language_id_'.$language]; 
                         $data['option'] = $dataa['option_'.$language];
                        $data['search_keys'] =$dataa['search_keys_'.$language];
                        $data['meta_description'] =$dataa['meta_description_'.$language];
                      $data['attachment'] = $data_attach['attachment_' . $language];
                        $update_id = $dataa['update_id_' . $language];
                        
                     $this->mdl_pages->_update($update_id, $data);
//                     $i++;
                 }
                
            }


            $this->session->set_flashdata('operation', 'Updated Successfully!!!');
        } else {
            
            $permission = $this->common_functions->check_permission($this->group_id,$this->MODULE);
            $data1['lang'] = $this->get_language_tab();
         
            $nextid = $this->Mdl_pages->get_id();
//            print_r($nextid);die;
           
            $uploadattachment = $this->common_functions->do_upload($nextid, $data1['lang'], $this->MODULE);
//            print_r($uploadattachment);die;
           
//            var_dump( $uploadattachment);die;
            foreach ($uploadattachment as $key => $value) {
                     $data['attachment'] = $uploadattachment[$key]['upload_data']['file_name'];
                }
            $i=0;
            if (isset($permission['add'])) {
                foreach ($data1['lang'] as $key => $language) {
                     $data['title'] = $dataa['title_' . $language];
                     $data['description']=$dataa['description_'.$language];
                      $data['slug']=$dataa['slug_'.$language];
                        $data['language_id']=$key;
                        $data['status']=$dataa['status_'.$language];	
//                        $data['language_id']=$dataa['language_id_'.$language];
                         $data['option'] = $dataa['option_'.$language];
                        $data['search_keys'] =$dataa['search_keys_'.$language];
                        $data['meta_description'] =$dataa['meta_description_'.$language];
                        $data['attachment'] = $uploadattachment[$i]['upload_data']['file_name'];
                            $this->mdl_pages->_insert($data);
//                        print_r($data);die;
                
                
                $i++;
            }
//            print_r($data);die;
            


            $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
        }
        }

        redirect('admin/pages');
    
    }

     function get_attachment_from_db($update_id) {
        $query = $this->Mdl_pages->get_where_dynamic($update_id);
          $data1['lang'] = $this->get_language_tab();
          //die;
        foreach ($query->result() as $row) {
            $language_id = $row->language_id;
            $language = $this->get_language($language_id);
            //$data['attachment'] = $row->attachment;	
            $data['attachment_' . $language] = $row->attachment;
        }
         
        return $data;
    }
    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_id_from_modulename($modulename) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }
     function get_language_tab() {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language_tab();
//         print_r($query->result());die;
        return $query;
    }
    function get_language($language_id) {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language($language_id);
//        var_dump($query->result());die;
        foreach ($query as $row) {
            $language = $row->language;
        }
        return $language;
    }
     function update_id_for_module_edit($lang_id, $slug) {
        $this->load->model('Mdl_pages');
        $query = $this->Mdl_pages->update_id_for_module_edit($lang_id, $slug);
//        print_r($query->result());die;
        foreach ($query->result()as $row) {
            $update_id = $row->id;
            return $update_id;
        }
    }
}
