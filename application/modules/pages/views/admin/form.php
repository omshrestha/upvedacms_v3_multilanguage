<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce_4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
     
    tinymce.init({
      selector: "textarea",
      
      // ===========================================
      // INCLUDE THE PLUGIN
      // ===========================================
    	
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages",
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools"
      ],
    	
      // ===========================================
      // PUT PLUGIN'S BUTTON on the toolbar
      // ===========================================
    	
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | image",
      toolbar3: "print preview media | forecolor backcolor emoticons",
    	
      // ===========================================
      // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
      // ===========================================
      image_advtab: true,
	
      relative_urls: false
    	
    });
     
    </script>
<!-- /TinyMCE -->
            <?php $lang_count=0;
                    foreach($lang as $language)
                    {?>        

                    <?php $lang_count++; 
                    } ?>  

<div class="row"> 
    <div class="col-md-12"> 
        <div class="tabbable tabbable-custom">
              <!--                code by for Tab-->
            <ul class="nav nav-tabs">
             <?php $i=1;
                foreach($lang as $language)
                {?>   
                    <li id="<?php echo $i ?>" class="<?php echo($i==1)?'active':'';?>"><a href="#tab_1_<?php echo $i;?>" data-toggle="tab"><?php echo $language;?></a></li> 
                <?php $i++;   
                } ?>            
              <!-- end of Tab --> 
            </ul>
          <!--form start -->  
            </div>
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Pages</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?> 
                 <div class="tab-content">
              <?php $i=1; 
              foreach($lang as $key=>$language){ ?>
              
              <!-- content div with tab -->
            <div class="tab-pane <?php echo($i==1)?'active':'';?>" id="tab_1_<?php echo $i;?>">
                 <div class="form-group"> 
                
                        <?php // echo form_input(array('name'=>'language_'.$language,'value'=>$key,'class'=>'form-control required', 'id'=>'language_'.$language.''));?>
                        <?php echo form_hidden('language_'.$language,$key);
                     ?>
                    
                        <?php // echo base64_decode($this->uri->segment(4));
                       echo form_hidden('update_id_'.$language,${'update_id_'.$language});?>
                </div>
                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php if(!isset(${'title_'.$language})){ ${'title_'.$language} = '';}
                        echo form_input('title_'.$language, ${'title_'.$language}, 'class="form-control"'); ?>
                    </div> 
                </div>

                <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                             <?php if(!isset(${'description_'.$language})){$selected =${'description_'.$language}='';}?>
                        	<?php echo form_textarea(array('id' => 'elm'.$i,'name' =>'description_'.$language, 'value' => ${'description_'.$language},'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class="form-control required"'));?>
                        </div> 
		</div>
                <div class="form-group"> 
                        <label class="col-md-2 control-label">Meta Description</label> 
                       <div class="col-md-10">
                             <?php if(!isset(${'meta_description_'.$language})){$selected =${'meta_description_'.$language}='';}?>
                        	<?php echo form_textarea(array('id' => 'elm1'.$i,'name' =>'meta_description_'.$language, 'value' => ${'meta_description_'.$language},'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class="form-control required"'));?>
                        </div> 
		</div>
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Search Keys</label> 
                      <div class="col-md-10"> 
                        <?php if(!isset(${'search_keys_'.$language})){ ${'search_keys_'.$language} = '';}
                         echo form_input('search_keys_'.$language, ${'search_keys_'.$language}, 'class="form-control required"');
                        ?>
                    </div> 
		</div>
               
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Display Image</label> 
                    <div class="col-md-10"> 
                        <?php
                        $selected = ${'option_'.$language};
                        $options = array(
                            
                            'no' => 'no',
                            'yes' => 'yes',
                        );
                        echo form_dropdown('option_', $options, $selected, 'class="form-control" id="option"');
                        ?>
                    </div> 
                </div> 

 <div class="form-group"> 
                    	<label class="col-md-2 control-label">Image<span class="">*</span></label> 
                    	<div class="col-md-10"> 
                            <?php  if(!empty($update_id)){

                                                    $attach_prop = array(
                                                            'type' => 'file',
                                                            'name' => 'userfile[]',
                                                            'value' => ${'attachment_'.$language},
                                                            'multiple' => 'multiple'         
                                                            );
                                            }else{
                                                    $attach_prop = array(
                                                            'type' => 'file',
                                                            'name' => 'userfile[]',
                                                            'value' => ${'attachment_'.$language},
                                                           
                                                            'multiple' => 'multiple'        
                                                            );
                                            }
                            ?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Image Only (jpg/jpeg/png/gif)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){
                                if(!isset(${'attachment_'.$language})){${'attachment_'.$language}='';}?>
                            	<img src="<?php echo base_url();?>uploads/pages/<?php echo ${'attachment_'.$language};?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                    </div>
                <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                         <div class="col-md-10"> 
                        <?php $selected = ${'status_'.$language};$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                        echo form_dropdown('status_'.$language, $options, $selected,'class="form-control"');?>
                    </div> 
                    </div>

                <?php 
                    if($i==$lang_count){					
                        echo form_submit('submit','Save','class="btn btn-primary pull-right"'); //name,value...type is default submit 
                   
                        }
                ?>
                 </div>
                 <?php $i++;
                   
              } ?>
                 </div>
           
               
                   
              
        <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
    
<script>
    
    $("#option").change(function() 
    {
     if($('#option').val()=='no')
     {
         $('#attachmentimage').hide();
         return false;
     }
     else ($('#option').val()=='yes')
     {
         $('#attachmentimage').show();
         return false;
     }
    });
    
</script>
