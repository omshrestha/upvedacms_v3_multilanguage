<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('sub_catagories'); //module name is groups here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['title'] = $this->input->post('name', TRUE);
                $data['description'] = $this->input->post('description', TRUE);
                $data['slug'] = strtolower(url_title($data['title']));
                //$data['attachment'] = $this->input->post('attachment', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
                if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
//				$data['upd_date'] = date("Y-m-d");
                                var_dump($data);die;
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
                                //$data['attachment1'] = $this->input->post('userfile1', TRUE);
                                //echo $data['attachment']; die;
//				$data['ent_date'] = date("Y-m-d");
//				$data['upd_date'] = NULL;
                               // var_dump($data);die;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
                        $data['description'] = $row->description;
                        $data['attachment'] = $row->attachment;
                        $data['status'] = $row->status;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
        function get_data_from_db_1($update_id)
	{
		$query = $this->get_where_1($update_id);
                //var_dump($query);die;
		foreach($query->result() as $row)
		{
                   // var_dump($query);die;
			$data['name'] = $row->title;
                        $data['description'] = $row->description;
                        $data['attachment']= $row->attachment;
                       // $data['group_id'] = $row->group_id;
		}
		return $data;
        }
        function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;
		}
			return $data;
	}
	 function view()
        {
            //$group_id = $this->session->userdata['group_id'];//to set the permession of user group
		$update_id = $this->uri->segment(4);
		$data = $this->get_data_from_db_1($update_id);
		$data['view_file'] = "admin/view";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
//               // }
                }
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		//if($update_id != 1){
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
               // }
			
//		}else{
//		redirect('admin/sub_catagories');
//		}
	}
	
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		
                        $attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];
                                }
				
				$this->_update($update_id, $data);
                }
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_sub_catagories.title]'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
                        
		}
		else
		{
			$dataa = $this->get_data_from_post();
                        //var_dump($dataa) ; die;
			$nextid = $this->get_id();
                        $update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
			
                            $uploadattachment = $this->do_upload($update_id);
                                
                                    $data['title'] = $dataa['title'];
                                    $data['description'] = $dataa['description'];
                                    $data['slug'] = $dataa['slug'];
                                    $data['status'] = $dataa['status'];
                                    $data['attachment'] = $uploadattachment['upload_data']['file_name'];
                                    $update_id = $dataa['update_id'];
                                    $this->_update($update_id, $data);
                                    
                                
				
                        }
                        else {
                            $nextid = $this->get_id();
                            $uploadattachment = $this->do_upload($nextid);
                          
                          
                               
                                    $data['title'] = $dataa['title'];
                                    $data['description'] = $dataa['description'];
                                    $data['slug'] = $dataa['slug'];
                                    $data['status'] = $dataa['status'];
                                    $data['attachment'] = $uploadattachment['upload_data']['file_name'];
                                    $this->_insert($data);
                                
                            }	
			
			redirect('admin/sub_catagories');
		}
			
	}
function do_upload($id)
        {
            $this->load->library('upload');

            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {
                
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

                $this->upload->initialize($this->set_upload_options($id));
                $this->upload->do_upload();
                $datas[$i] = array('upload_data' => $this->upload->data());

            }

            return $datas;

}
private function set_upload_options($id)
{   
//  upload an image options
    $config = array();
    $config['upload_path'] = './uploads/sub_catagories/';
    $config['allowed_types'] = 'gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar';
    $config['max_size']      = '20480';
    $config['overwrite']     = FALSE;
    $config['max_width']     =   "1907"; 
    $config['max_height']    =   "1280";
    $config['file_name'] = $id;

    return $config;
}

//        function do_upload1($id) //for single attachment upload.
//	{ 
//
//	   $config['upload_path']   =   "./uploads/sub_catagories/"; 
//	   $config['file_name'] = $id;		   
//	   $config['overwrite'] = TRUE;
//	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
//	   $config['max_size']      =   "20480"; //that's 20MB
//	   $config['max_width']     =   "1907"; 
//	   $config['max_height']    =   "1280"; 
//
//	   $this->load->library('upload',$config);
//            
//	   
//		if ( ! $this->upload->do_upload())
//		{
//			echo 'File cannot be uploaded';
//			$datas = array('error' => $this->upload->display_errors());
//                    //$datas = array('upload_data' => $this->upload->data());	
//		}
//		else
//		{
//			echo 'File has been uploaded';
//			$datas = array('upload_data' => $this->upload->data());		
//			
//		}
//		
//		return $datas;
//	}
       
        function get_id(){
	$this->load->model('mdl_sub_catagories');
	$id = $this->mdl_sub_catagories->get_id();
	return $id;
	}
         function get_id1(){
	$this->load->model('mdl_sub_catagories');
	$id = $this->mdl_sub_catagories->get_id1();
	return $id;
	}
	function get($order_by){
	$this->load->model('mdl_sub_catagories');
	$query = $this->mdl_sub_catagories->get($order_by);
	return $query;
	}
	function get_where_1($id){
	$this->load->model('mdl_sub_catagories');
	$query = $this->mdl_sub_catagories->get_where_1($id);
	return $query;
	}
	function get_where($id){
	$this->load->model('mdl_sub_catagories');
	$query = $this->mdl_sub_catagories->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_sub_catagories');
	$this->mdl_sub_catagories->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_sub_catagories');
	$this->mdl_sub_catagories->_update($id, $data);
	}
        function delete()
	{	$this->load->model('mdl_sub_catagories');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/sub_catagories');
			}
		else
		{
			$this->mdl_sub_catagories->_delete($delete_id);
			redirect('admin/sub_catagories');
		}			
			
	}
}
