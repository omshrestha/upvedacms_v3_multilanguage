<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_users extends Mdl_crud {

    protected $_table = "up_users";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get($order_by) {
        $table = $this->_table;
        $this->db->select('up_users.*');
        $this->db->select('up_users_groups.name as group_name');
        $this->db->join('up_users_groups', 'up_users_groups.id = up_users.group_id');
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    function get_username_from_user_id($user_id) {
        $table = $this->_table;
        $this->db->select('display_name');
        $this->db->where($this->_primary_key, $user_id);
        $query = $this->db->get($table)->result();
        if (isset($query[0])) {
            return $query[0]->display_name;
        } else {
            return null;
        }
    }

//    function get_where($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $query = $this->db->get($table);
//        return $query;
//    }
//
//    function _insert($data) {
//        $table = $this->get_table();
//        $this->db->insert($table, $data);
//    }
//
//    function _update($id, $data) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->update($table, $data);
//    }
//
//    function _delete($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->delete($table);
//    }

    function chpwd_update($id, $chpwd) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->update($table, $chpwd);
    }

    function get_users_dropdown() {
        $table = $this->_table;
        $this->db->select('id, display_name');
        $this->db->order_by($this->_primary_key, 'DESC');
        $dropdowns = $this->db->get($table)->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->display_name;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

}
