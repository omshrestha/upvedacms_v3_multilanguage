<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_navigation extends Mdl_crud {

    protected $_table = "up_navigation";
    protected $_primary_key = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_page($slug) {
        $table = $this->_table;
        $this->db->where('slug', $slug);
        $query = $this->db->get($table);
        foreach ($query->result() as $row) {
            $data['navtype'] = $row->navtype;
            return $data;
        }
    }

    function get_navigation_dropdown($group_id) {
        $this->db->select('id, title');
        $this->db->where('parent_id', '0');
        $this->db->where('group_id', $group_id);
        $this->db->order_by('title', 'ASC');
        $dropdowns = $this->db->get('up_navigation')->result();
        $dropdownlist[0] = ''; //for making parent value null as default
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_parentnav_dropdown() {
        $this->db->select('id, title');
        $this->db->where('parent_id', '0');
        $this->db->order_by('title', 'ASC');
        $dropdowns = $this->db->get('up_navigation')->result();
        $dropdownlist[0] = ''; //for making parent value null as default
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_parent_name($group_id) {
        $this->db->select('id, title');
        $this->db->where('group_id', $group_id);
        $this->db->where('parent_id', '0');
        $this->db->order_by('title', 'ASC');
        $dropdowns = $this->db->get('up_navigation')->result();
        $dropdownlist[0] = ''; //no name for blank
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->title;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_ind_nav($group_id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->order_by('position', 'ASC');
        $query = $this->db->get($table);
        return $query;
    }

    function get_parentnav($group_id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->where('parent_id', 0);
        $this->db->order_by('position', 'ASC');
        $query = $this->db->get($table);
        $result = $query->result_array();
        if (empty($result)) {
            return NULL;
        }
        return $result;
    }

    function get_childnav($group_id, $parent_id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('position', 'ASC');
        $query = $this->db->get($table);
        $result = $query->result_array();
        if (empty($result)) {
            return NULL;
        }
        return $result;
    }

    function get_parentnav_for_frontend($group_id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->where('parent_id', 0);
        $this->db->where('status', 'live');
        $this->db->order_by('position', 'ASC');
        $query = $this->db->get($table);
        $result = $query->result_array();
        if (empty($result)) {
            return NULL;
        }
//        var_dump($result);die;
        return $result;
    }

    function get_childnav_for_frontend($group_id, $parent_id) {
        $table = $this->_table;

        $this->db->where('group_id', $group_id);
        $this->db->where('parent_id', $parent_id);
        $this->db->where('status', 'live');
        $this->db->order_by('position', 'ASC');
        $query = $this->db->get($table);
        $result = $query->result_array();
        if (empty($result)) {
            return NULL;
        }
        return $result;
    }

//        function get_childnav_for_frontend_navigation($slug){
//        $table = $this->get_table();
//        $this->db->select('n.*');
//        $this->db->join('up_navigation n2','n.parent_id=n2.id');
//        $this->db->join('up_pages','n2.page_id=up_pages.id');
//        $this->db->where('up_pages.slug',$slug);
//        $query=$this->db->get('up_navigation n');
//        return $query;
//        }
//        
//        function get_childnav_for_frontend_navigation_same_level($slug){
//        $table = $this->get_table();
//        $this->db->select('n.*');
//        $this->db->join('up_navigation n2','n.parent_id=n2.parent_id');
//        $this->db->join('up_pages','n2.page_id=up_pages.id');
//        $this->db->where('up_pages.slug',$slug);
//        $this->db->where('n2.parent_id <>',0);
//        $query=$this->db->get('up_navigation n');
//        return $query;
//        }

    function check_if_nav_belongs_to_group($group_id, $id) {
        $table = $this->_table;
        $this->db->where('group_id', $group_id);
        $this->db->where($this->_primary_key, $id);
        $query = $this->db->get($table);
        $arr = $query->result_array();
        if (empty($arr)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function _delete($id) {
        $table = $this->_table;
        $this->db->where($this->_primary_key, $id);
        $this->db->delete($table);

        $this->db->where('parent_id', $id);  //deleting all the child navigation of the 	
        $this->db->delete($table);    //particular navigation.
    }

    function get_id_of_parent_from_position_number($position) {
        $table = $this->_table;
        $this->db->select($this->_primary_key);
        $this->db->where('position', $position);
        $query = $this->db->get($table);
        $result = $query->result();
        return $result[0]->id;
    }

    function update_parent_id_from_position($parent_id, $position, $group_id) {
        $table = $this->_table;
        $this->db->where('position', $position);
        $this->db->where('group_id', $group_id);
        $this->db->update($table, array('parent_id' => $parent_id));
    }

    function update_nest($position_array) {
        $table = $this->_table;
        foreach ($position_array as $row) {
            $this->db->where('position', $row);
            $this->db->update($table, array('position' => 'li' . $row));
        }
        $i = 1;
        foreach ($position_array as $row) {
            $this->db->where('position', 'li' . $row);
            $this->db->update($table, array('position' => $i));
            $i++;
        }
    }

    function get_next_position($group_id) {
        $table = $this->_table;
        $this->db->select('position');
        $this->db->where('group_id', $group_id);
        $query = $this->db->get($table);
        $result = $query->result();
        if (empty($result)) {
            return 1;
        }//because if there is no any navigation of current group we'd like to set it 1(i.e. 1st)
        $i = 0;
        $new_array = array();
        foreach ($result as $row) {
            $new_array[$i] = $row->position;
            $i++;
        }
        $max_position = max($new_array);
        $next_position = $max_position + 1;
        return $next_position;
    }

    function get_navigation_from_navigation_name($navigation_name) {
        $navigation_id = $this->get_navigation_id_from_navigation_name($navigation_name);
        $table = $this->_table;
        $this->db->where('status', 'live');
        $this->db->where('group_id', $navigation_id);
        $query = $this->db->get($table);
        return $query;
    }

    function get_navigation_id_from_navigation_name($navigation_name) {
        $this->db->select($this->_primary_key);
        $this->db->like('title', $navigation_name);
        $query = $this->db->get('up_navigation_group')->result_array();
        return $query[0][$this->_primary_key];
    }

    function get_childnav_for_frontend_navigation($slug) {
        $table = $this->_table;
        $this->db->select('n.*');
        $this->db->join('up_navigation n2', 'n.parent_id=n2.id');
        $this->db->join('up_pages', 'n2.page_id=up_pages.id');
        $this->db->where('up_pages.slug', $slug);
        $query = $this->db->get('up_navigation n');
        return $query;
    }

    function get_childnav_for_frontend_navigation_same_level($slug) {
        $table = $this->_table;
        $this->db->select('n.*');
        $this->db->join('up_navigation n2', 'n.parent_id=n2.parent_id');
        $this->db->join('up_pages', 'n2.page_id=up_pages.id');
        $this->db->where('up_pages.slug', $slug);
        $this->db->where('n2.parent_id <>', 0);
        $this->db->order_by($this->_primary_key, 'ASC');
        $query = $this->db->get('up_navigation n');
        return $query;
    }

}
