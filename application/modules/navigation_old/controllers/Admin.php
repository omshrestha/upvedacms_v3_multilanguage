<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/navigation";
    private $group_id;
    private $model_name = 'Mdl_navigation_group';
    private $model_name1 = 'Mdl_navigation';
    private $MODULE = 'navigation';

    public function __construct() {
        parent::__construct();
        $this->group_id = $this->session->userdata('group_id');
        $this->load->library('pagination');
        $this->load->library('Up_pagination');
        $this->load->library('Common_functions');
        $this->load->model('Mdl_navigation_group');
        $this->load->model('pages/mdl_pages');
        $this->load->model('modules/mdl_moduleslist');
        $this->load->model('Mdl_navigation');
        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('navigation'); //module name is navigation here	
    }

    //-------------------start of group nav----------------------//	
    function index() {
        //table select parameters
        $main_table_params = 'id,title,slug';
        //search parameters
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
        } else {
            $params = '';
        }
        $count = $this->Mdl_navigation_group->count($params);

        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $config['per_page'] = 5;
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_navigation_group->get_where_dynamic('', $main_table_params, 'id', ($page - 1) * $config['per_page'], $config['per_page'], $params);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['column'] = 'title';
        $data['slug'] = 'slug';
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/group_table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }

    function create_group() {
        $update_id = base64_decode($this->uri->segment(4));
        if ($update_id != 1 && $update_id != 2) { //because 1 is header and 2 is footer which i don't want to change or edit or delete
            $submit = $this->input->post('submit', TRUE);

            if ($submit == "Submit") {
                //person has submitted the form
                $data = $this->get_data_from_post();
            } else {
                if (is_numeric($update_id)) {
                    $select = 'title,slug';
                    $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
                }
            }

            if (!isset($data)) {
                $data = $this->get_data_from_post();
            }
            $data['update_id'] = $update_id;
            $data['view_file'] = "admin/group_form";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            redirect('admin/navigation');
        }
    }

    function delete_group() {
        $delete_id = $this->uri->segment(4);

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/navigation');
        } else {
            $this->Mdl_navigation_group->_delete_group($delete_id);
            redirect('admin/navigation');
        }

        $data['view_file'] = "admin/group_table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function get_data_from_post() {

        $data['title'] = $this->input->post('title', TRUE);
        $data['slug'] = strtolower(url_title($data['title']));
        return $data;
    }

    function submit_group() {

        $update_id = $this->input->post('update_id', TRUE);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'required|xss_clean');

        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();

            if (is_numeric($update_id)) {
                $this->Mdl_navigation_group->_update($update_id, $data);
            } else {
                $this->Mdl_navigation_group->_insert($data);
            }

            redirect('admin/navigation');
        }
    }

    //-------------------end of group nav----------------------//
    //-------------------------------------------------start of individual navigation----------------------------------------------------//


    function group() {//to list out every navigation individually
        $manage = $this->uri->segment(5);
        if (empty($manage)) {
            $group_id = $this->uri->segment(4);
            $existence = $this->Mdl_navigation_group->check_group_existence($group_id);
            if ($existence == FALSE) {
                redirect('admin/navigation');
            }//checking if the group_id exist or not
            else {
                $data['query'] = $this->all_nav($group_id);
                $data['parent_name'] = $this->Mdl_navigation->get_parent_name($group_id);
                $data['group_id'] = $group_id;
                $data['group_title'] = $this->Mdl_navigation_group->get_group_title($group_id);
                $data['view_file'] = "admin/table";
                $this->load->module('template/admin_template');
                $this->admin_template->admin($data);
            }
        } else {
            $this->$manage();
        }
    }

    function all_nav($group_id) {//to get navigation list of specific group
        $data['parentnav'] = $this->Mdl_navigation->get_parentnav($group_id); //
        if ($data['parentnav'] == NULL) {
            return NULL;
        }
        $i = 0;
        foreach ($data['parentnav'] as $nav) {
            $children = $this->Mdl_navigation->get_childnav($group_id, $nav['id']);
            if ($children != NULL) {
                $nav['children'] = $children;
            }
            $navigation[$i] = $nav;
            $i++;
        }
        return $navigation;
    }

    function get_ind_nav($group_id) {//to get navigation list of specific group
        $query = $this->Mdl_navigation->get_ind_nav($group_id);
        return $query;
    }

    function create() {

        $update_id = $this->uri->segment(6);
        $group_id = $this->uri->segment(4);
        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_ind_data_from_post();
        } else {
            if (is_numeric($update_id)) {
//                $data=$this->get_data_from_post();

                $group_id = $this->uri->segment(4);
                $nav_id = $this->uri->segment(6);
                $check_status = $this->Mdl_navigation->check_if_nav_belongs_to_group($group_id, $nav_id);

                if ($check_status == TRUE) {
                    $select = 'title,slug,navtype,parent_id,group_id,module_id,page_id,site_uri,link_url,status';
                    $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name1);
                } else {
                    redirect('admin/navigation/group/' . $group_id);
                }
            }
        }

        if (!isset($data)) {
            $data = $this->get_ind_data_from_post();
        }
        $data['lang'] = $this->get_language_tab();
        $data['update_id'] = $update_id;
        $select = 'id,title';
        $params = array('parent_id' => 'integer=>"0"', 'group_id' => 'integer=>' . $group_id);
        $data['parent'] = $this->common_functions->get_dropdown('title', $select, $params, $this->model_name1);
        $data['modulelist'] = $this->common_functions->get_dropdown('title', $select, '', 'mdl_moduleslist');
        $data['pagelist'] = $this->common_functions->get_dropdown('title', $select, '', 'mdl_pages');
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $delete_id = $this->uri->segment(6);
        $group_id = $this->uri->segment(4);

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/navigation/group/' . $group_id);
        } else {
            $this->Mdl_navigation->_delete($delete_id);
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!');
            redirect('admin/navigation/group/' . $group_id);
        }
    }

    function submit() {
         $group_id = $this->uri->segment(4);
        $update_id = $this->input->post('update_id_english', TRUE);
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('title', 'Title', 'required|xss_clean');
//        if ($this->form_validation->run($this) == FALSE) {
//            $this->create();
//        
            $dataa = $this->get_ind_data_from_post();
            $data1['lang'] = $this->get_language_tab();
             foreach ($data1['lang'] as $key => $language) {
                 
                      
                      

            if ($dataa['navtype_'.$language] == 'Module_'.$language) {
                $dataa['page_id_'.$language] = 'NULL';
                $dataa['link_url_'.$language] = 'NULL';
            } elseif ($dataa['navtype_'.$language] == 'Page_'.$language) {
                $data['module_id_'.$language] = 'NULL';
                $data['link_url_'.$language] = 'NULL';
            } else {
                $data['module_id_'.$language] = 'NULL';
                $data['page_id_'.$language] = 'NULL';
            }
             }
             
            if (is_numeric($update_id)) {
                $data1['lang']=  $this->get_language_tab();
                $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);
                if (isset($permission['edit'])) {
                    foreach ($data1['lang'] as $key =>$language){
                        $new_position = $this->get_next_position($dataa['group_id_'.$language]);
                        $data['position']=$new_position;
                        $data['title']=$dataa['title_'.$language];
                        $data['slug']=$dataa['slug_'.$language];
                        $dataset = explode('_',$dataa['navtype_'.$language]);
                        
                        
                    }
                     
                     
                     
                    $this->Mdl_navigation->_update($update_id, $data);
//                    $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);
//                    if (isset($permission['edit'])) {
//                    }
                }
          //                        $this->Mdl_navigation->_update($update_id, $data);
          $this->session->set_flashdata('operation', 'Updated Successfully!!!');
                } else {
                    $data1['lang'] = $this->get_language_tab();
                      foreach ($data1['lang'] as $key => $language) {
                    $permission = $this->common_functions->check_permission($this->group_id,  $this->MODULE);
                      
                    $new_position = $this->Mdl_navigation->get_next_position($dataa['group_id_'.$language]); //becuz we have to set the position as well
                    $data['position'] = $new_position;
                    //and the next position will be be fore new nav
                    $data['title']=$dataa['title_'.$language];
                    $data['slug'] = $dataa['slug_' . $language];
                    $dateset = explode('_', $dataa['navtype_' . $language]);
                    $data['navtype'] = $dateset[0];
                    $data['parent_id'] = $dataa['parent_id_' . $language];
                    $data['module_id'] = $dataa['module_id_' . $language];
//                    var_dump( $data['module_id']); die;
                    $data['page_id'] = $dataa['page_id_' . $language];
                    $data['site_uri'] = $dataa['site_uri_' . $language];
                    $data['link_url'] = $dataa['link_url_' . $language];
                    $data['status'] = $dataa['status_' . $language];
                    $data['group_id'] = $dataa['group_id_' . $language];
//                                var_dump($data['group_id']);die('df');
                    $data['language_id'] = $dataa['language_id_' . $language];
//                    var_dump($data);die;
                    
                    if (isset($permission['add'])) {
                        
                        $this->Mdl_navigation->_insert($data);
                    }
                      }
                      
                    $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
                   // redirect('admin/navigation/group/' . $data['group_id']);
                }
                redirect('admin/navigation/group/' . $data['group_id']);
                die;
            }
//        }
        
    

    function submit_nest() {
        $new_array = $_POST;
        
        $group_id = $new_array['group_id'];
       
        $jsoncode = json_decode($new_array['jsoncode'], true, 64);
        $i = 1;
        $position_array = array();
        foreach ($jsoncode as $row) {
            $parent_id = 0;
            $position = $row['id'];
           
            $position_array[$i] = $row['id'];
            $i++;
            
            
            $this->Mdl_navigation->update_parent_id_from_position($parent_id, $position, $group_id);

            if (isset($row['children'])) {
                foreach ($row['children'] as $child) {
                    $parent_id_for_children = $this->Mdl_navigation->get_id_of_parent_from_position_number($row['id']);
                    $parent_id = $parent_id_for_children;
                   
                    $position = $child['id'];
                    $position_array[$i] = $child['id'];
                    $i++;
                    $this->Mdl_navigation->update_parent_id_from_position($parent_id, $position, $group_id);
                }
            }
        }
        $submit = $new_array['submit'];
        unset($new_array['group_id']);
        unset($new_array['submit']);
        $this->Mdl_navigation->update_nest($position_array);
        redirect('admin/navigation');
    }

    function get_ind_data_from_post() {
        $data1['lang']=  $this->get_language_tab();
        foreach ($data1['lang'] as $key=>$language){
        $data['title_'.$language] = $this->input->post('title_'.$language, TRUE);
        $data['slug_'.$language] = strtolower(url_title($data['title_english']));
        $data['navtype_'.$language] = $this->input->post('navtype_'.$language, TRUE);
        $data['parent_id_'.$language] = $this->input->post('parent_id_'.$language, TRUE);
        $data['group_id_'.$language] = $this->input->post('group_id_'.$language, TRUE);
        $data['module_id_'.$language] = $this->input->post('module_id_'.$language, TRUE);
        $data['page_id_'.$language] = $this->input->post('page_id_'.$language, TRUE);
        $data['site_uri_'.$language] = $this->input->post('site_uri_'.$language, TRUE);
        $data['link_url_'.$language] = $this->input->post('link_url_'.$language, TRUE);
        $data['status_'.$language] = $this->input->post('status_'.$language, TRUE);
        $data['language_id_'.$language]=  $this->input->post('language_id_'.$language,TRUE);
        $data['update_id_'.$language]=  $this->input->post('update_id_'.$language,TRUE);
        }
        return $data;
            
       
        
    }

     function get_language_tab() {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language_tab();
        return $query;
    }
    //-------------------------------------------------end of individual navigation----------------------------------------------------//
}

