<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Set Permissions</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/permissions/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>        

                <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
                    <thead> 
                        <tr> 
                            <th data-class="expand" colspan="12">Group</th> 
                        </tr> 
                    </thead> 
                    <tbody> 
                        <?php
                        $mystring = implode(" ", $permissions);
                        ?>

                        <?php foreach ($query->result() as $row) {
                            ?>
                            <tr> 
                                <td class="checkbox-column">
                                    <input type='checkbox' name='<?php echo $row->id; ?>' <?php
                                    if (isset($permissions[$row->id])) {
                                        echo "checked";
                                    }
                                    ?> type="checkbox" class="uniform"/>
                                </td> 

                                <td><?php echo Ucfirst($row->title); ?></td>
                                <td class="uniform">
                                    <div class="pull-right">
                                        <input type='checkbox' name='<?php echo 'd' . $row->id; ?>' <?php
                                           if ((strpos($mystring, 'd' . $row->id)) == true) {
                                               echo "checked";
                                           }
                                           ?> type="checkbox" class="uniform"/>
                                        <i class="">Delete</i>               
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-right">
                                        <input type='checkbox' name='<?php echo 'e' . $row->id; ?>' <?php
                                           if ((strpos($mystring, 'e' . $row->id)) == true) {
                                               echo "checked";
                                           }
                                           ?> type="checkbox" class="uniform"/>
                                        <i class="">Edit</i>                   
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>

                                    <div class="pull-right">
                                        <input type='checkbox' name='<?php echo 'v' . $row->id; ?>' <?php
                                           if ((strpos($mystring, 'v' . $row->id)) == true) {
                                               echo "checked";
                                           }
                                    ?> type="checkbox" class="uniform"/>
                                        <i class="">View</i> 
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-right">
                                        <input type='checkbox' name='<?php echo 'a' . $row->id; ?>' <?php
                            if ((strpos($mystring, 'a' . $row->id)) == true) {
                                echo "checked";
                            }
                            ?> type="checkbox" class="uniform"/>
                                        <i class="">Add</i> 
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </td>
                            </tr> 

                    <?php } ?>                
                    </tbody> 
                </table> 


                <div class="form-actions"> 
<?php
echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
$group_id = $this->uri->segment(4);
if (!empty($group_id)) {
    echo form_hidden('group_id', $group_id);
}
?>
                </div>                 

<?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>