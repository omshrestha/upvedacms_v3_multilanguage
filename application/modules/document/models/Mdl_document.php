<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_document extends Mdl_crud{
     protected $_table = "up_document";
    protected $_primary_key = 'id';
    

    function __construct() {
        parent::__construct();
    }
    
    function get_search_data($data1){
//            var_dump($data1); die;
           $category_id= $data1['category_id'];
           $document_type_id=$data1['document_type_id'];
           $title=$data1['title'];
//           die($title);
           $language_id=$data1['language_id'];
	$table = $this->_table;
        $this->db->select('up_document.title as document_title,up_document.attachment,up_document.meta_key');
        $this->db->select('up_category.title as category_title');
	$this->db->select('up_document_type.title as document_type_title');
	$this->db->join('up_category', 'up_category.id = up_document.category_id');
	$this->db->join('up_document_type', 'up_document_type.id = up_document.document_type_id');
        if (($data1['category_id']!='' && $data1['category_id']!=0 ))
        {
//            die('o');
        $this->db->where('up_document.category_id',$data1['category_id'] );
        }
        if (($data1['document_type_id']!='' && $data1['document_type_id']!=0))
        {
//            die('p');
        $this->db->where('up_document.document_type_id',$data1['document_type_id'] );

        }
        if (($data1['language_id']!='' || $data1['language_id']!=0)){
//            die('q');
        $this->db->where('up_document.language_id',$data1['language_id'] );
        }
        
        if (($data1['title']!='' || $data1['title']!=0)){
//            die('r');
           $this->db->like('up_document.title',$data1['title']);
          $this->db->or_like('up_document.meta_key',$data1['title']); 
        }
	$this->db->order_by('up_document.id','DESC');
	$query=$this->db->get($table)->result();
        
        return $query;    
        
	
	}

//    function get_table() {
//        $table = "up_document";
//        return $table;
//    }

//    function _delete($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->delete($table);
//    }

//    function get($order_by) {
//        $table = $this->get_table();
//        $this->db->order_by($order_by, 'ASC');
//        $query = $this->db->get($table);
//        return $query;
//    }

//    function get_where($id) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $query = $this->db->get($table);
//        return $query;
//    }

    

//    function _insert($data) {
//        $next_id = $this->get_id();
//
//        $table = $this->get_table();
//        $this->db->insert($table, $data);
//
//
//        $insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
//        $permission_table = 'up_permissions';
//        $this->db->insert($permission_table, $insert_null_permission_array);
//    }
//function get_details($category_id=null,$document_type_id=null){
//        $table = $this->_table;
//        $this->db->order_by('id', 'ASC');
//        
//        
//    
//    if($category_id!=null){
//        $this->db->where('category_id',$category_id);     
//         }
//         if($document_type_id!=null){
//        $this->db->where('document_type_id',$document_type_id);     
//         }
//         $query = $this->db->get($table)->result_array();
//         var_dump($query);die;
//         return $query;
//}
    function get_id() {
        $result = $this->db->query("SHOW TABLE STATUS LIKE 'up_document'");
        $row = mysql_fetch_array($result);
        $nextId = $row['Auto_increment'];
        return $nextId;
    }

//    function _update($id, $data) {
//        $table = $this->get_table();
//        $this->db->where('id', $id);
//        $this->db->update($table, $data);
//    }
//
//    function get_modules_dropdown() {
//        $this->db->select('id, name');
//        $this->db->order_by('name');
//        $dropdowns = $this->db->get('up_modules')->result();
//        foreach ($dropdowns as $dropdown) {
//            $dropdownlist[$dropdown->id] = $dropdown->name;
//        }
//        if (empty($dropdownlist)) {
//            return NULL;
//        }
//        $finaldropdown = $dropdownlist;
//        return $finaldropdown;
//    }

//    function get_groups_dropdown() {
//        $this->db->select('id, title');
//        //$this->db->where('id > 1');
//        $this->db->order_by('id', 'AESC');
//        $dropdowns = $this->db->get('up_document')->result();
//        foreach ($dropdowns as $dropdown) {
//            //$dropdownlist[0] = '-- Select Panchakarma --';    
//            $dropdownlist[$dropdown->id] = $dropdown->title;
//        }
//        if (empty($dropdownlist)) {
//            return NULL;
//        }
//        $finaldropdown = $dropdownlist;
//        return $finaldropdown;
//    }

    function get_document($order_by) {
        $table = $this->get_table();
        $this->db->order_by('id', 'ASC');
        $this->db->where('status', 'live');
        $query = $this->db->get($table)->result_array();
        return $query;
    }
     function update_id_for_module_edit($lang_id,$slug){
        $table = $this->_table;
	$this->db->where('language_id', $lang_id);
        $this->db->where('slug', $slug);  
        $query=$this->db->get($table);
//      var_dump($query->result());die;
	return $query;  
        }

}
