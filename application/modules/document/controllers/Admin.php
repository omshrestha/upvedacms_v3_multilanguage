<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    private $MODULE_PATH = "admin/document";
    private $group_id;
    private $MODULE = 'document';
    private $model_name = 'Mdl_document';

    public function __construct() {
        parent::__construct();
        $this->load->model('Mdl_document');
        $this->group_id = $this->session->userdata('group_id');
        $this->load->model('permissions/Mdl_permissions');
        $this->load->model('modules/Mdl_moduleslist');
        $this->load->module('admin_login/admin_login');
        $this->load->model('settings/Mdl_settings');
        $this->load->library('Common_functions');
        $this->load->library('pagination');
        $this->load->library('up_pagination');
        
        $this->admin_login->check_session_and_permission('document'); //module name is document here	
    }

    function login() {
        echo'Hello World';
    }

    function index() {  
        $main_table_params = 'id,title,status,attachment';
            $join_params['select_params'] = "up_category.title as category";
            $join_params['join_table'] = 'up_category';
            $join_params['join_condition'] = "up_category.id=up_document.category_id";
        if ($this->input->post('parameters')) {
            $params = json_decode($this->input->post('parameters'));
           
        } else {
            $params = '';
        }
        $count = $this->Mdl_document->count($params);
 
        if ($this->input->post('order_by')) {
            $order_by = $this->input->post('order_by');
        } else {
            $order_by = 'id';
        }
        $module_url = base_url() . $this->MODULE_PATH;
        $config = $this->up_pagination->set_pagination_config($count, $module_url);
        $entries = $this->common_functions->get_data_from_db('1', 'per_page', 'Mdl_settings');
        
        $config['per_page'] = $entries['per_page'];
        $page = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        $this->pagination->initialize($config);
        $data['query'] = $this->Mdl_document->get_where_dynamic('', $main_table_params, 'up_document.id', ($page - 1) * $config['per_page'], $config['per_page'], $params,$join_params);
//        $data['query'] = $this->Mdl_banner->get_all_for_pagination('id', ($page - 1) * $config['per_page'], $config['per_page']);
        $data['permission'] = $this->common_functions->check_permission($this->group_id, $this->MODULE);
        $data['columns'] = array('title', 'status','attachment','category');
        $data['page'] = $page;
        $data['total'] = $count;
        $data['total_page'] = ceil($count / $config['per_page']);
        $data['per_page'] = $config['per_page'];
        $data['theUrl'] = $module_url;
//         $data['group_array'] = $this->get_groups();
        $data['page_links'] = $this->pagination->create_links();
        if ($this->uri->segment(3) == '' && ($params == '')) {
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);
        } else {
            $this->load->view('admin/new_table', $data);
        }
    }
//     function get_language_list(){
//        $this->load->model('language/Mdl_language');
//	$query = $this->Mdl_language->get('id');	
//	return $query->result();   
//        }
//        function get_selected_language_id($selected_language){
//        $this->load->model('language/Mdl_language');
//        $language_id=  $this->Mdl_language->get_language_id($selected_language);
//        foreach($language_id -> result() as $id){
//        $selected_language_id = $id->id;}
//        return  $selected_language_id;
//        }
    function get_data_from_post() {
        $data1['lang'] = $this->get_language_tab();
        $update_id = $this->input->post('update_id_english', TRUE);
        if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
         foreach ($data1['lang'] as $key => $language) {
             $data['attachment_'.$language] = $this->input->post('userfile', TRUE);
             $data['category_'.$language] = $this->input->post('category_'.$language, TRUE);
             $data['document_type_'.$language] = $this->input->post('document_type_'.$language, TRUE);
        $data['title_'.$language] = $this->input->post('title_'.$language, TRUE);
//        $data['description_'.$language] = $this->input->post('description_'.$language, TRUE);
        $data['slug_' . $language] = $slug = strtolower(url_title($data['title_english']));
        $data['meta_description_'.$language] = $this->input->post('meta_description_'.$language, TRUE);
        $data['meta_key_'.$language] = $this->input->post('meta_key_'.$language, TRUE);
         $data['language_id_' . $language] = $this->input->post('language_' . $language, TRUE);
          $data['update_id_' . $language] = $this->input->post('update_id_' . $language, TRUE);
        $data['status_'.$language] = $this->input->post('status_'.$language, TRUE);
//        $update_id = $this->input->post('update_id', TRUE);
         }
//          var_dump($data);die;
        return $data;
    }

    function get_data_from_db($update_id) {
        $data1['lang'] = $this->get_language_tab();
        $query = $this->Mdl_document->get_where_dynamic($update_id);
        
        foreach ($query->result() as $slugg) {
            $slug = $slugg->slug;
            $data['title'] = $slugg->title;
             $data['category_id'] = $slugg->category_id;
             $data['document_type_id'] = $slugg->document_type_id;
//            $data['description'] = $slugg->description;
            $data['status'] = $slugg->status;
            $data['attachment'] = $slugg->attachment;
            $data['meta_description'] = $slugg->meta_description;
            $data['meta_key'] = $slugg->meta_key;
            $data['ent_date'] = $slugg->ent_date;
           
        }
//       var_dump($data1['lang']);die;
        foreach ($data1['lang'] as $key => $language) {
            $langu_id = $key;
            $update_id = $this->update_id_for_module_edit($langu_id, $slug);
//            var_dump($update_id);die;
            if (isset($update_id)) {
                $querys = $this->Mdl_document->get_where_dynamic($update_id);
//                var_dump($querys->result());
//                die;
                  foreach ($querys->result() as $row) {
                      
                    $language_id = $row->language_id;
                     $language = $this->get_language($language_id);
                     $data['title_'.$language] = $row->title;
                     $data['category_'.$language] = $row->category_id;
                      $data['document_type_'.$language] = $row->document_type_id;
                     $data['attachment_'.$language] = $row->attachment;
                      $data['status_'.$language] = $row->status;
                      $data['meta_key_'.$language] = $row->meta_key;
                       $data['meta_description_'.$language] = $row->meta_description;
                       $data['ent_date_'.$language] = $row->ent_date;
                  }
                  $data['update_id_' . $language] = $update_id;
                 
                  } 
//                  else {
//                $language_id = $langu_id;
//                $language = $this->get_language($language_id);
//                 $data['slug_' . $language] = $datanew['slug'] = $slug;
//                 $data['language_id_' . $language] = $datanew['language_id'] = $language_id;
//                 $datanew['ent_date'] = date("Y-m-d");
//                $datanew['upd_date'] = NULL;
//                $update_id = $this->_insert($datanew);
//                $data['update_id_' . $language] = $update_id;
//                  }
        }
       
        

        if (!isset($data)) {
            $data = "";
        }
//        var_dump($datas);die;
        return $data;
    }


      function create() {
        $update_id = base64_decode($this->uri->segment(4));
        $submit = $this->input->post('submit', TRUE);

        if ($submit == "Submit") {
//person has submitted the form
            $data = $this->get_data_from_post();
        } else {
            if (is_numeric($update_id)) {
                $select = 'title,attachment,meta_description,ent_date,status';
//               $data = $this->common_functions->get_data_from_db($update_id, $select, $this->model_name);
               $data=$this->get_data_from_db($update_id);
//                var_dump($data); die;
            }
        }

        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
               $data['document_type_array_english']=$this->get_english_document_type();
        $data['document_type_array_nepali']=$this->get_nepali_document_type();
         $data['category_array_english']=$this->get_english_category();
        $data['category_array_nepali']=$this->get_nepali_category();
        $data['lang'] = $this->get_language_tab();
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }
    function get_english_category(){
    $this->load->model('category/mdl_category');
    return $this->mdl_category->get_category_dropdown(3);
}
function get_nepali_category(){
    $this->load->model('category/mdl_category');
    return $this->mdl_category->get_category_dropdown(4);
}
 function get_english_document_type(){
    $this->load->model('document_type/mdl_document_type');
    return $this->mdl_document_type->get_dropdown(null,3);
}
function get_nepali_document_type(){
    $this->load->model('document_type/mdl_document_type');
    return $this->mdl_document_type->get_dropdown(null,4);
}

    function submit() {
//        $this->load->model('mdl_pa');
        $dataa = $this->get_data_from_post();
        $update_id = $this->input->post('update_id_english', TRUE);

        if (is_numeric($update_id)) {
            
         $query = $this->Mdl_document->get_where_dynamic($update_id);

		foreach($query->result() as $slugg)
		{
                    $slug = $slugg->slug;
		}
             $data1['lang'] = $this->get_language_tab();

            foreach($data1['lang'] as $key=>$language){
                $langu_id = $key;

                 $update_id = $this->update_id_for_module_edit($langu_id,$slug);
                $attach[$language] = $this->get_attachment_from_db($update_id);
            }
           
             
            $uploadattachment = $this->common_functions->do_upload($update_id,$data1['lang'],$this->MODULE);

           /*we have defined i bcoz of the language id starting from 3*/
            $i=0;
            foreach ($data1['lang'] as $key => $value) {
                $data_attach['attachment_'.$value] = $uploadattachment[$i]['upload_data']['file_name']; 
             $attachment= explode('.', $data_attach['attachment_'.$value]);
              
                if( !isset($attachment[1])){
                    /*for updating the image without uploading after edit having next id except english*/
                $data_attach['attachment_'.$value]=$attach[$value]['attachment_'.$value];
                    
                }
                $i++;
            }
           
                
            $permission = $this->common_functions->check_permission($this->group_id,$this->MODULE);
            
            if (isset($permission['edit'])) {
                 foreach ($data1['lang'] as $key => $language) {

                     $data['title'] = $dataa['title_' . $language];
                     $data['document_type_id']=$dataa['document_type_'.$language];
                      $data['slug']=$dataa['slug_'.$language];
                       
                        $data['status']=$dataa['status_'.$language];	
                        $data['language_id']=$dataa['language_id_'.$language]; 
                         $data['category_id'] = $dataa['category_'.$language];
                        $data['meta_key'] =$dataa['meta_key_'.$language];
                        $data['meta_description'] =$dataa['meta_description_'.$language];
                      $data['attachment'] = $data_attach['attachment_'.$language];
                        $update_id = $dataa['update_id_' . $language];
                        
                     $this->Mdl_document->_update($update_id, $data);
                     $i++;
                 }
                
            }


            $this->session->set_flashdata('operation', 'Updated Successfully!!!');
        } else {
            
            $permission = $this->common_functions->check_permission($this->group_id,$this->MODULE);
            $data1['lang'] = $this->get_language_tab();
         
            $nextid = $this->Mdl_document->get_max();
//            print_r($nextid);die;
           
            $uploadattachment = $this->common_functions->do_upload($nextid, $data1['lang'], $this->MODULE);
            
           
//            var_dump( $uploadattachment);die;
            foreach ($uploadattachment as $key => $value) {
                     $data['attachment'] = $uploadattachment[$key]['upload_data']['file_name'];
//                     print_r($data['attachment']);die;
                }
            $i=0;
            if (isset($permission['add'])) {
                foreach ($data1['lang'] as $key => $language) {
                     $data['title'] = $dataa['title_' . $language];
//                     $data['description']=$dataa['description_'.$language];
                      $data['slug']=$dataa['slug_'.$language];
                        $data['language_id']=$key;
                        $data['status']=$dataa['status_'.$language];	
                        $data['language_id']=$dataa['language_id_'.$language];
                         $data['category_id'] = $dataa['category_'.$language];
                         $data['document_type_id'] = $dataa['document_type_'.$language];
                        $data['meta_key'] =$dataa['meta_key_'.$language];
                        $data['meta_description'] =$dataa['meta_description_'.$language];
                        $data['attachment'] = $uploadattachment[$i]['upload_data']['file_name'];
                         if (is_numeric($update_id)) {
            $attach = $this->get_attachment_from_db($update_id);
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        
                            $this->Mdl_document->_insert($data);
//                        print_r($data);die;
                
                
                $i++;
            }
            
//            print_r($data);die;
            


            $this->session->set_flashdata('operation', 'Inserted Successfully!!!');
        }
        
        }

        redirect('admin/document');
    }

    function delete() {
        $delete_id = base64_decode($this->uri->segment(4));
        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/document');
        } else {
            $message = $this->common_functions->delete_attachment($delete_id, $this->model_name, $this->MODULE);
            $this->Mdl_document->_delete($delete_id);
            $this->session->set_flashdata('operation', 'Deleted Successfully!!!' . $message);
            redirect('admin/document');
        }
    }
    function get_language_tab() {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language_tab();
        return $query;
    }
    function get_language($language_id) {
        $this->load->model('language/Mdl_language');
        $query = $this->Mdl_language->get_language($language_id);
        foreach ($query as $row) {
            $language = $row->language;
        }
        return $language;
    }
function get_attachment_from_db($update_id) {
        $query = $this->Mdl_document->get_where_dynamic($update_id);
          $data1['lang'] = $this->get_language_tab();
          //die;
        foreach ($query->result() as $row) {
            $language_id = $row->language_id;
            $language = $this->get_language($language_id);
            //$data['attachment'] = $row->attachment;	
            $data['attachment_' . $language] = $row->attachment;
        }
         
        return $data;
    }
  function get_id() {
        $this->load->model('Mdl_document');
        $id = $this->Mdl_document->get_id();
        return $id;
    }
      function update_id_for_module_edit($lang_id, $slug) {
        $this->load->model('Mdl_document');
        $query = $this->Mdl_document->update_id_for_module_edit($lang_id, $slug);
//        print_r($query->result());die;
        foreach ($query->result()as $row) {
            $update_id = $row->id;
            return $update_id;
        }
    }

}
