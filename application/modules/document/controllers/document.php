<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class document extends MX_Controller
{

	function __construct() {
	parent::__construct();
        if($this->session->userdata('language') == "eng")
		{
			$this->lang->load('english', 'english');
		}
		else if($this->session->userdata('language')=="nep")
		{
			$this->lang->load('nepali', 'nepali');
		}
                else {
                $this->lang->load('english', 'english');
                }
	}
           function get_category($selected_language_id){
    $this->load->model('category/mdl_category');
    return $this->mdl_category->get_category_dropdown($selected_language_id);
}

 function get_document_type($selected_language_id){
    $this->load->model('document_type/mdl_document_type');
    return $this->mdl_document_type->get_dropdown(null,$selected_language_id);
}

	function index(){
//            die('hello');
            if(!isset($data['language_id'])){
              $data['language_id']=3;  
              
            }
//                
                $language_id=$data['language_id'];
//                  var_dump($language_id);die('df');
                 if(!$this->session->userdata('language')){
//                     die('hello');
                   $data['language']= $selected_language = 'eng';
//                   die('hello');
                }
                else{
                    
                    $data['language']=$selected_language=$this->session->userdata['language'];
                    
                }
                $selected_language_id = $this->get_selected_language_id($selected_language);
                $data['title']='';
                $data['category_id']='';
                $data['document_type_id']='';
               $data['language_id']= $selected_language_id;
//                var_dump($selected_language_id);die('fsd');
                $data['language_list']=$this->get_language_list();
                
                $data['document_type_array']=$this->get_document_type($selected_language_id);
                
//                  $data['lang'] = $this->get_language_tab();
                 $data['category_array']=$this->get_category($selected_language_id);
                 $category=$this->uri->segment(2);
                 if(isset($category)){
                 $data['category']=$category;
                 }
                 $slug=$this->uri->segment(3);
                    
              $data['query']=$this->get_search_data($data);
		$data['view_file']="searchView";
		$this->load->module('template');
		$this->template->front($data);
	}
        function search(){
//        die('slkdkjsfk');
        $this->load->model('Mdl_document');
//        $query=$this->Mdl_document->get_search_data($data);
        $data['category_id'] = $this->input->post('category_id', TRUE);
             $data['document_type_id'] = $this->input->post('document_type_id', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
         $data['language_id'] = $this->input->post('language_id', TRUE);
         $data['query']=$this->Mdl_document->get_search_data($data);
         $this->load->view('table',$data);
    }
        
        function details(){
//            die('hello');
               if(!isset($data['language_id'])){
              $data['language_id']=1;  
            }
//                
                $language_id=$data['language_id'];
//                  var_dump($language_id);die('df');
                 if(!$this->session->userdata('language')){
                   $data['language']= $selected_language = 'eng';
                }
                else{
                    $data['language']=$selected_language=$this->session->userdata['language'];
                }
                $selected_language_id = $this->get_selected_language_id($selected_language);
//                var_dump($selected_language_id);die('fsd');
                $data['language_list']=$this->get_language_list();
                $data['language_id']= $selected_language_id;
                $data['document_type_array']=$this->get_document_type($selected_language_id);
                 $data['category_array']=$this->get_category($selected_language_id);
                $document_type_id=$this->uri->segment(4);
//                die($offset);
                $slug = $this->uri->segment(3);
//                die($slug);
                if($slug!=""){
                $category_id=$this->get_category_id($slug,$selected_language_id);    
                $search['category_id']=$category_id;
                $data['category_id']=$category_id;
                $data['slug1']=$slug;
                $search['language_id']=$selected_language_id;
                $search['document_type_id']='';
                           $search['title']='';
                }
                if($document_type_id!=""){
                    
                $document_type_id=$this->get_document_type_id($document_type_id,$selected_language_id);
                $search['document_type_id']=$document_type_id;
                 $data['document_type_id']=$document_type_id;
                }
                if(is_numeric($category_id) && is_numeric($document_type_id)){
//                  $data['query']=$this->get_details($category_id,$document_type_id);  
                    $data['query']=$this->get_search_data($search);
                
                }else if(is_numeric($category_id)){
//                  $data['query']=$this->get_details($category_id,null);    
                  $data['query']=$this->get_search_data($search);    
                }
                
                $data['view_file']="searchView";
		$this->load->module('template');
		$this->template->front($data);
//                    $this->index();
                
        }
        function get_search_data($search){
        $this->load->model('mdl_document');
	$query = $this->mdl_document->get_search_data($search);
	return $query;    
        }
        function get_details($category_id=null,$document_type_id=null){
        $this->load->model('mdl_document');
	$query = $this->mdl_document->get_details($category_id,$document_type_id);
	return $query;
         
        }
        function get($order_by){
	$this->load->model('mdl_document');
	$query = $this->mdl_document->get($order_by);
	return $query;
	}
        function get_document_list($order_by){
	$this->load->model('mdl_document');
	$query = $this->mdl_document->get_document_list($order_by);
	return $query;
	}
        function get_category_id($slug,$selected_language_id){
        $this->load->model('category/mdl_category');
	$query = $this->mdl_category->get_category_id($slug,$selected_language_id);
	return $query;    
        }
        function get_document_type_id($slug,$selected_language_id){
	$this->load->model('document_type/mdl_document_type');
	$query = $this->mdl_document_type->get_document_type_id($slug,$selected_language_id);
	return $query;
	}

	function get_document_detail($slug){
        $this->load->model('mdl_document');
	$document_id = $this->mdl_document->get_document_id($slug);
        $this->load->model('document_comment/mdl_document_comment');
        $query = $this->mdl_document_comment->get_document($document_id);
        return $query;
        }
	function get_pages_if_live($col, $value) {
	$this->load->model('mdl_document');
	$query = $this->mdl_document->get_pages_if_live($col, $value);
	return $query;
	}

	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_document');
	$query = $this->mdl_document->get_where_custom($col, $value);
	return $query;
	}
         function get_selected_language_id($selected_language){
        $this->load->model('language/mdl_language');
        $language_id=  $this->mdl_language->get_language_id($selected_language);
//        var_dump($language_id->result());die('df');
        foreach($language_id -> result() as $id){
        $selected_language_id = $id->id;}
        return  $selected_language_id;
        }
         function get_language_list(){
        $this->load->model('language/mdl_language');
	$query = $this->mdl_language->get('id');	
	return $query->result();   
        }

}