
                        <table class="table table-striped table-bordered" id="myTable1">
                            
                        <thead>
                            <tr style="background-color: #ff6600; color:#fff; font-size:14px;">
                                <?php 
                                if($language_id==4){
                                $columns = array('sn','title', 'attachment','category','document');?>
                                <?php foreach ($columns as $column) { ?>
                        <th><?php
                            $this->lang->load('document', 'english');
                            echo $this->lang->line(implode(' ', explode('_', $column)));
                            ?></th>
                                <?php } }else{?>
                                
                                <th>S.No.</th>
                                <th>Document Title</th>
                                <th>Attachment</th>
                                <th>Category Title</th>
                                <th>Document Type Title</th>
                                <?php } ?>
                                
                            </tr>
                         </thead>
                         <tbody><?php $i=1;?>
                             <?php foreach($query as $row){?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $row->document_title;?></td>
                                            <td><?php echo $row->attachment;?></td>   
                                            <td><?php echo $row->category_title ?></td>
                                            <td><?php echo $row->document_type_title ?></td>  
                                        </tr>
                        <?php
                        $i++;                                    
                         } ?>
                                        </tbody>
                                          
                         </table>