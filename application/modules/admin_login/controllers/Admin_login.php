<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_login extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mdl_admin_login');
        $this->load->model('permissions/mdl_permissions');
        $this->load->model('modules/mdl_moduleslist');
        $this->load->model('settings/mdl_settings');
    }

    function index() {
        $this->check_session();
    }

    function login() {
        $data['site_settings'] = $this->get_site_settings();
        $data['view_file'] = "loginform";
        $this->load->module('template');
        $this->template->userlogin($data);
    }

    function get_site_settings() {
        $query = $this->mdl_settings->get_settings();
        $result = $query->result_array();
        return $result[0];
    }

    function submit() {

        $this->load->helper(array('form', 'url', 'security')); // security helper is required in codeigniter 3.0.4
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('pword', 'Password', 'required|max_length[100]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->login();
        } else {
            extract($_POST);
            $this->load->model('mdl_admin_login');
            $ids = $this->Mdl_admin_login->check_login($username, $pword);
            if (empty($ids)) {
                $user_id = $roup_id = '';
            } else {
                $user_id = $ids->id;
                $group_id = $ids->group_id;
            }

            if (!$user_id && !$group_id) {
                $this->session->set_userdata('login_error', TRUE);
                $this->login();
            } else {
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'user_id' => $user_id,
                    'group_id' => $group_id,
                    'permission' => $access
                ));
                $this->session->unset_userdata('login_error');
                redirect('admin/dashboard');
            }
        }
    }

    function logout() {

        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('group_id');
        redirect('admin');
    }

    function check_session() {
        if ($this->session->userdata('logged_in'))/* to check session and make sure that it has only admin privilage */ {
            redirect('admin/dashboard');
        } else {
            $this->login();
        }
    }

    function check_session_and_permission($modulename) {
        $group_id = $this->session->userdata("group_id");
        $access = $this->get_all_permissions($group_id);

        if ($this->session->userdata("logged_in")) {

            if ($access == 'granted') {
                
            } elseif ($access == 'denied') {
                redirect('admin');
            } else {
                $moduleid = $this->mdl_moduleslist->get_id_from_modulename($modulename);
                if (!isset($access[$moduleid])) {
                    redirect('admin/dashboard');
                }
            }
            //redirect('admin/dashboard');
        }
    }

    function get_all_permissions($group_id) {

        /* checkin permission */
        if ($group_id != 1) { //for non-admin
            $permission_status = $this->mdl_permissions->check_permission_existence($group_id); //finish this

            if ($permission_status == TRUE) {//finish this
                $access = $this->mdl_permissions->unserialize_role_array($group_id); //finish this
            } else {
                $access = 'denied';
            }//finish this                        					
        } else { //for admin
            $access = 'granted';
        }

        return $access;
    }

}
