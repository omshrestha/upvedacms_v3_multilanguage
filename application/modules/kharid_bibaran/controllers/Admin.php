<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('kharid_bibaran'); //module name is kharid_bibaran here	
    }

    function index() {
        $modulename = $this->uri->segment(2);
        $data['module_id'] = $this->get_id_from_modulename($modulename);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $data['permissions'] = $this->unserialize_role_array($group_id);
//            $this->load->model('budget/mdl_budget');
//            $data['budget_array']=  $this->mdl_budget->get_budget_dropdown(); 
//        $this->load->model('fiscal_year/mdl_fiscal_year');
//        $data['budget_year'] = $this->mdl_fiscal_year->get_fiscal_year();
        $data['query'] = $this->get('id');
        $data['view_file'] = "admin/table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function saveData() {
        $jasonvalue = $_POST['data'];
        $jsondata = $_POST['values'];
        $kharid_aadesh_no = $_POST['kharid_aadesh_no'];
        $values = json_decode($jsondata);
//            echo $kharid_aadesh_no;die('om');
        $data_value = json_decode($jasonvalue);
        if ($data_value != "") {
            foreach ($data_value as $row) {
                foreach ($row as $key => $value) {
                    if ($key == 'item_id') {
//                        $this->load->model('item/mdl_item');
//                        $data['item_id'] = $this->mdl_item->get_where($value);
                    }
                    if ($key != 'unit' && $key != 'status' && $key != 'item_id') {
                        $data["$key"] = $value;
                        $data['kharid_aadesh_no'] = $kharid_aadesh_no;
                    }
                }
//                 echo '<pre>',print_r($data),'</pre>';die('om');
                $this->_insert($data);
                echo 0;
            }
        } else {
            return 1;
        }
        if ($values != "") {
            foreach ($values as $row) {
                foreach ($row as $key => $value) {
                    $data_kharid_aadesh["$key"] = $value;
                    $data_kharid_aadesh['date'] = date("Y-m-d");
                }
//                echo '<pre>',print_r($data_kharid_aadesh),'</pre>';die('hello');
//                $this->load->model('kharid_aadesh/mdl_kharid_aadesh');
//                $this->mdl_kharid_aadesh->_insert($data_kharid_aadesh);
            }

            echo 0;
        } else {
            return 1;
        }
    }

    function findUnit() {
//            die('hello');
        $item_id = $_POST['item_id'];
        $this->get_item_unit($item_id);
    }

    function get_item_unit($item_id) {
        $this->load->model('mdl_kharid_bibaran');
        $this->mdl_kharid_bibaran->get_item_unit($item_id);
    }

    function get_data_from_post() {
        $data['name'] = $this->input->post('name', TRUE);
        $data['unit'] = $this->input->post('unit', TRUE);
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $data['upd_date'] = date("Y-m-d");
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
        }
        return $data;
    }

    function get_data_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
            $data['unit'] = $row->unit;
        }

        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }

    function create() {
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $update_id = $this->uri->segment(4);
//        $this->load->model('fiscal_year/mdl_fiscal_year');
//        $data['budget_year'] = $this->mdl_fiscal_year->get_fiscal_year();
        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        }
        if (is_numeric($update_id)) {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'e' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->get_data_from_db($update_id);
                }
            } else {
                $data = $this->get_data_from_db($update_id);
            }
        } else {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'a' . $module_id)) == false) {
                    redirect('admin/dashboard');
                }
            }
            //$data = $this->get_data_from_db($update_id);
        }


        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $this->load->model('mdl_kharid_bibaran');
        $delete_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/kharid_bibaran');
        } else {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'd' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $this->mdl_kharid_bibaran->_delete($delete_id);
                    redirect('admin/kharid_bibaran');
                }
            }
            $this->mdl_kharid_bibaran->_delete($delete_id);
            redirect('admin/kharid_bibaran');
        }
    }

    function view() {
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $view_id = $this->uri->segment(4);
        if ($group_id != 1) {
            if (is_numeric($view_id)) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'v' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->get_data_from_db($view_id);
                }
            }
        }
        $data = $this->get_data_from_db($view_id);
        $data['view_id'] = $view_id;
        $data['view_file'] = "admin/view";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
        }
        /* end of validation rule */


        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();

            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $this->_update($update_id, $data);
            } else {
                $this->_insert($data);
            }

            redirect('admin/kharid_bibaran');
        }
    }

    function get($order_by) {
        $this->load->model('mdl_kharid_bibaran');
        $query = $this->mdl_kharid_bibaran->get($order_by);
        return $query;
    }

    function get_where($id) {
        $this->load->model('mdl_kharid_bibaran');
        $query = $this->mdl_kharid_bibaran->get_where($id);
        return $query;
    }

    function _insert($data) {
        $this->load->model('mdl_kharid_bibaran');
        $this->mdl_kharid_bibaran->_insert($data);
    }

    function _update($id, $data) {
        $this->load->model('mdl_kharid_bibaran');
        $this->mdl_kharid_bibaran->_update($id, $data);
    }

    function _delete($id) {
        $this->load->model('mdl_kharid_bibaran');
        $this->mdl_kharid_bibaran->_delete($id);
    }

    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_id_from_modulename($modulename) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }

}
