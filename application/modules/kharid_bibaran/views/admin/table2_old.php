<html>
    <head>
        <style>
            table, th, td {
                border: 1px solid black;
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                                });


        </script>
    </head>
    <body>
        
            <div>
                <h4 align="center"> नेपाल सरकार </h4> 
                <h2 align="center"> गृह मन्त्रालय  </h2>
                <h3 align="center"> खरिद आदेश  </h3>
            </div>
            <div>
                <?php  $date = date("Y-m-d");?>
            <div class=" clearfix">
                <div class="col-lg-6">
                <h3> श्री </h3>    
                 <h3> ठेगाना: काठमान्डू   </h3>   
                  <h3> करदाता नं </h3> 
                  </div>
                
                    <div class="text-right">
                      
                        <div class="col-lg-6">
                   <h3> म. ले. प. फा. नं. ४५ </h3>
                    <h3> खरिद आदेश नं. </h3> 
                     <h3> मिति : <?php echo $date;?> </h3>
                    </div>    
            </div>
            </div>
          
            </div>
            <div>
                <h3>    देहाय बमोजिमका सामानहरु मिति:  **********    भित्र कार्यालयमा दाखिला गरेको विल इन्भ्वाईस प्रस्तुत गर्नु होला| </h3>
                
            </div>
          

       

                <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
                    <thead> 
                        <tr> 
                            <th class="checkbox-column" rowspan="2">S.No.</th> 
                            <th data-class="expand" rowspan="2">Budget Title No</th>  
                            <th data-class="expand" rowspan="2">Item Description</th>
                            <th data-class="expand" rowspan="2">Specification</th>
                            <th data-class="expand" rowspan="2">Item Type</th>
                            <th data-class="expand" rowspan="2">Quantity</th>
                            <th data-class="expand" rowspan="2">Unit</th>
                            <th data-class="expand" colspan="3" style="text-align:center;">Price</th> 
                            <th data-class="expand" rowspan="2">Manage</th>          
                        </tr>
                        <tr style="border-bottom: 1px solid black;">                     
                            <th data-class="expand">Rate</th>
                            <th data-class="expand">Total</th>
                            <th data-class="expand">Remarks</th>
                        </tr>
                    </thead>       

                    <?php $sno = 1; ?>
                    <tbody id="addbody" class="addbody1" data-level="0">
                        <tr> 
                            <td class="checkbox-column"><?php echo $sno;
                    $sno++; ?></td>  
                            <td width="10"><?php echo form_input('budget_title_no', '', 'class="form-control required budget_title_name persist" id="bud_drp"'); ?></td>
                            <td width="160"><?php echo form_input('item_id', '', 'class="form-control required persist itm_description_name " id="itm-drp"'); ?></td>
                            <td width="160"><?php echo form_input('specification', '', 'class="form-control itm_specification_name required persist" id="itm-spec[0]"'); ?></td>
                            <td width="100"><input type="radio" class="persist" name="status" value="Yes">Yes
            <input type="radio" class="persist" name="status" value="No">No
                            </td>
                            <td width="10"><?php echo form_input('quantity', '', 'class="form-control txt-quantity_name required persist" id="txt-quantity"'); ?></td>
                            <td width="80"><?php echo form_input('unit', '', 'class="form-control txt_unit_name required persist" id="txt-unit"'); ?></td>
                            <td width="80"><?php echo form_input('rate', '', 'class="form-control txt_rate_name required persist" id="txt-rate"'); ?></td>
                            <td width="80"><?php echo form_input('total', '', 'class="form-control txt_total_name required persist" id="txt-total"'); ?></td>
                            <td><?php echo form_input('remarks', '', 'class="form-control txt_remarks_name required persist" id="txt-remarks[0]"'); ?></td>
                            <td class="edit">
                 <button id="btn-add-new1" class="btn_add_new">Add New</button>                   
            </td>
                        </tr> 
                    </tbody>


                </table> 
       

        
        <div >
            <input class='btn-primary pull-right' type="button" name="Save" value="Save" id="btn-newentry">
        </div>
        <script>

//                    $('tbody').find('#txt-rate').blur(function() {
//                    var quantity = $('#txt-quantity').val();
//                    var rate = $('#txt-rate').val();
//                    var result=quantity * rate;
//                    if (quantity == "") {
//                        alert("Enter Quantity");
//                    }
//                    else if (rate == "") {
//                        alert("Enter Rate");
//                    } else {
//                        var myResult = quantity * rate;
//                       $('#txt-total').val(myResult);
//                    }
//                });
//                
//                $('tbody').find('#txt-quantity').blur(function() {
//                    var quantity = $('#txt-quantity').val();
//                    var rate = $('#txt-rate').val();
//                    var result=quantity * rate;
//                    
//                        var myResult = quantity * rate;
//                       $('#txt-total').val(myResult);
//                });
                
                $(document).on("blur", ".txt_rate_name", function(e) {
//                    $('tbody').find('.txt_rate_name').blur(function() {
                    var parent_id = $(e.target).closest('tbody').attr('id');            
                    var quantity = $("#" + parent_id).find('.txt-quantity_name').val();
                    var rate = $("#"+parent_id).find('.txt_rate_name').val();
                        var myResult = quantity * rate;
                       $("#"+parent_id).find('.txt_total_name').val(myResult);
                        $('.txt_total_name').trigger('change');
                        $('.txt_quantity_name').trigger('change');
                    
                    });
                $(document).on("blur", ".txt-quantity_name", function(e) {
//                    $('tbody').find('.txt_rate_name').blur(function() {
                    var parent_id = $(e.target).closest('tbody').attr('id');            
                    var quantity = $("#" + parent_id).find('.txt-quantity_name').val();
                    var rate = $("#"+parent_id).find('.txt_rate_name').val();
                   
                        var myResult = quantity * rate;
                       $("#"+parent_id).find('.txt_total_name').val(myResult);
                        $('.txt_total_name').trigger('change');
                        $('.txt_rate_name').trigger('change');
                  });
               
                $(document).on("click", ".btn_add_new", function(){
                    $(this).attr('disabled','disabled');
                    var parent = $(this).closest("tbody");
                    var parentlevel = parent.data('level');
                    var name = parseInt(parentlevel) + 1;
                   var sno=name + 1;
            var html= '<tbody class="addbody1" data-level='+name+' id=addbody' + name + '\
        <tr>\
         <td class="checkbox-column">'+sno+'</td>\
        <td><input type="text" class="form-control required budget_title_name persist" name="budget_title_no" ></td>\
        <td><input type="text" class="form-control required   persist itm_description_name" name="item_id" id="itm-drp[' + name + ']"></td>\
        <td><input type="text" class="form-control required persist" name="specification" id="itm-spec[' + name + ']"></td>\
        <td><input type="radio" class="persist" name="status" value="Yes">\Yes\
            <input type="radio" class="persist" name="status" value="No"> No </td>\
        <td><input type="text" class="form-control required txt-quantity_name persist" name="quantity" id="txt-quantity[' + name + ']"></td>\
        <td><input type="text" class="form-control required txt_unit_name persist" name="unit" id="txt-unit[' + name + ']"></td>\
        <td><input type="text" class="form-control txt_rate_name required persist" name="rate"  id="txt-rate[' + name + ']"></td>\
        <td><input type="text" class="form-control required txt_total_name persist" name="total" id="txt-total[' + name + ']"></td>\
        <td><input type="text" class="form-control required persist" name="remarks" id="txt-remarks[' + name + ']"></td>\
        <td class="edit"><button class="btn_add_new" id="btn-add-new['+name+']"> Add New </button></td></tr>\n\
        </tbody>';
         $('.addbody').data('level', name);

        var position = $(this).closest('tbody');
        $(position).after(html);
            
          $.ui.autocomplete.prototype._renderItem = function( ul, item){
            var term = this.term.split(' ').join('|');
            var re = new RegExp("(" + term + ")", "gi") ;
                var t = item.label.replace(re,"<strong>$1</strong>");
                return $( "<li></li>" )
            .data("item.autocomplete", item)
        .append( "<a>" + t + "</a>" )
        .appendTo( ul );
    };
    $(document).find(".itm_description_name").autocomplete({ 
    source:"item/get_items" // path to the get_o method
        });
        $(document).find(".budget_title_name").autocomplete({ 
    source:"budget/get_budget_no" // path to the get_o method
  });
                                    });
                                    
                       
                                    
//                                    
//                       $('tbody').find('#itm-drp').change(function(e) {
//                  //     e.preventDefault();
//                    var item_id = $('#itm-drp').val();
//                    var base_url = '<?php echo base_url(); ?>'
//                    $.ajax({
//                        url: base_url + "admin/kharid_bibaran/findUnit",
//                        type: 'POST',
//                        data: "item_id=" + item_id,
//                        success: function(result) {
//                    $('#txt-unit').val(result);
//                        }
//                    });
//                });
                   
                   $(document).on("blur", ".itm_description_name", function(e) {
                 var parent_id = $(e.target).closest('tbody').attr('id');
                 var item_id = $("#"+parent_id).find('.itm_description_name').val();
                    var base_url = '<?php echo base_url(); ?>'
                    $.ajax({
                        url: base_url + "admin/kharid_bibaran/findUnit",
                        type: 'POST',
                        data: "item_id=" + item_id,
                        success: function(result) {
                            $("#"+parent_id).find('.txt_unit_name').val(result);
                             $('.txt_unit_name').trigger('change');
                        }
                    });
                });
                </script>
                 <link href="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery.ui.css" rel="stylesheet" type="text/css" />  
  
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>

<script>
    $(function(){
                $.ui.autocomplete.prototype._renderItem = function( ul, item){
  var term = this.term.split(' ').join('|');
  var re = new RegExp("(" + term + ")", "gi") ;
  var t = item.label.replace(re,"<strong>$1</strong>");
  return $( "<li></li>" )
     .data("item.autocomplete", item)
     .append( "<a>" + t + "</a>" )
     .appendTo( ul );
    };
    $(document).find(".itm_description_name").autocomplete({ 
    source:"item/get_items" // path to the get_o method
  });
  $(document).find(".budget_title_name").autocomplete({ 
    source:"budget/get_budget_no" // path to the get_o method
  });
 });
    </script>
    <script>
    $(document).on('change','.persist',function(e){ 
                    var parent = $(this).closest("tbody");
                    var activityIndex = parent.data('level');
                    var data; 
                    console.log(activityIndex);

                   
        try{
            data = JSON.parse(localStorage['inventory-data']);
//            alert('data');
        }catch(e){
//            alert('testing');
            data = {};
//                console.log("No data");
        }

  if (!data[activityIndex])
            data[activityIndex]={};
        
        data[activityIndex][$(this).attr('name')]=$(this).val();
        localStorage.setItem('inventory-data', JSON.stringify(data));
    });
    
    $("#btn-newentry").click(function(){ 
               try{
             data = localStorage['inventory-data'];
             saveData(data);
                }catch(e){}
                localStorage.clear();
           });
           
           function saveData(data)
    { 
         var base_url = '<?php echo base_url(); ?>';
//       var quotation_no = $('.quotation_no').val();
        $.ajax({
                        url: base_url + "admin/kharid_bibaran/saveData",
                        type: 'POST',
                        data: "data=" + data,
                        success: function (result) {  
                             if (result==0){
                           localStorage.clear();  
                           alert('Data Saved Sucessfully');
                       }
                       else
                           alert('Data Could Not Saved Something Went Wrong');
                            
                       }
                       
                    });
    }
    
    </script>
    <script>
    try{
            data = JSON.parse(localStorage['inventory-data']);
            for( var k in data){
                for( var j in data[k]){
                    try{
                        $('tbody').find('[name='+j+']').val(data[k][j]);
                    }catch(e){
                        localStorage.clear();
                    }
                }
            }
         

        }catch(e){
//            console.log("Nothing to restore.");
        }
    try{
            data = JSON.parse(localStorage['inventory-data']);
            for( var k in data){
            if(k!=0){ 
              $('.btn_add_new').trigger('click');
           }   
                var activityRow = $.find('[data-level='+k+']');
               
                for( var j in data[k]){
                    try{
                        $(activityRow).find('[name='+j+']').val(data[k][j]);
                    }catch(e){}
                }
            }
        }catch(e){
//            console.log("Nothing to restore.");
        }
    </script>
    </body>
</html>