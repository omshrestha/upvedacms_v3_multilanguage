<div class="manage">
    <?php
    $group_id = $this->session->userdata['group_id'];
    if ($group_id != 1) {
        $mystring = implode(" ", $permissions);
        if ((strpos($mystring, 'a' . $module_id)) == true) {
            ?>
            <input type="button" value="Add Kharid bibaran" id="create" onclick="location.href = '<?php echo base_url() ?>admin/kharid_bibaran/create';"/>
            <?php
        }
    } else {
        ?>
        <input type="button" value="Add Kharid bibaran" id="create" onclick="location.href = '<?php echo base_url() ?>admin/kharid_bibaran/create';"/>
        <?php
    }
    echo form_open('admin/dakhila_pratibedan/get_pratibedan_faram', 'class="form-horizontal row-border" id="validate-1"');
    ?>

    <h4>खरिद आदेश नं</h4><input type="text" class="border persistance" name="kharid_aadesh_no" size="" class="" id="kharid_aadesh_no_id" value="" style="">   
    <div>              
        <h4>आर्थिक वर्ष</h4><?php
        if (!empty($budget_year)) {
            $options = $budget_year;
            echo form_dropdown('budget_year', $options, 0, 'id="datebirthnp"');
        }
        ?></div>
    <div>
        <button name="btn1" id="btnsubmit" >दाखिला प्रतिवेदन हेर्नुहोस</button>
         <!--<input type="button" value="pratibedan faram" class="" id="create" />--> 
                     <!--<input type="button" value="pratibedan faram" id="pratibedan_faram_id"/>-->

    </div>
    <?php echo form_close(); ?> 

</div>
<div class="widget box"> 

    <div class="widget-header"> 
        <h4><i class="icon-reorder"></i>खरिद विवरण  </h4> 
        <div class="toolbar no-padding"> 
            <div class="btn-group"> 
                <span class="btn btn-xs widget-collapse">
                    <i class="icon-angle-down"></i>
                </span> 
            </div> 
        </div> 
    </div>


    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">क्रम. सं.</th> 
                    <th data-class="expand">विवरण</th>  
                    <th data-class="expand">सामानको परिमाण</th> 
                    <th data-class="expand">दर</th>
                    <th data-class="expand">जम्मा रकम</th>
                    <th class="edit">ब्यबस्थापन गर्नुहोस् </th> 
                </tr> 
            </thead> 
            <tbody> 
                <?php $sno = 1; ?>
                <?php foreach ($query->result() as $row) { ?>
                    <tr> 
                        <td class="checkbox-column"><?php
                            echo $sno;
                            $sno++;
                            ?></td> 
                        <td><?php echo $row->name; ?></td> 
                        <td><?php echo $row->quantity; ?></td>
                        <td><?php echo $row->rate; ?></td>
                        <td><?php echo $row->total; ?></td>
                        <td class="edit">

                        <!--                            <a href="<?php echo base_url() ?>admin/kharid_aadesh/view/<?php echo $row->item_id; ?>"><?php
                            if ($group_id == 1) {
                                echo '<i class="icon-eye-open"> / </i>';
                            } elseif ((strpos($mystring, 'v' . $module_id)) == true) {
                                echo '<i class="icon-eye-open"> / </i>';
                            }
                            ?></a>                   -->
                            <!--<button class=" btn-primary btn_print"> Print </button>-->
                            <a href="<?php echo base_url() ?>admin/kharid_aadesh/print_aadesh_page/<?php echo $row->item_id; ?>"> <span class="glyphicon glyphicon-print"></span> / </a>
                            <a href="<?php echo base_url() ?>admin/kharid_aadesh/create/<?php echo $row->item_id; ?>"><?php
                                if ($group_id == 1) {
                                    echo '<i class="icon-pencil"> / </i>';
                                } elseif ((strpos($mystring, 'e' . $module_id)) == true) {
                                    echo '<i class="icon-pencil"> / </i>';
                                }
                                ?></a>                   

                            <a href="<?php echo base_url() ?>admin/kharid_aadesh/delete/<?php echo $row->item_id; ?>" onclick="return confirm('Are you sure, you want to delete it?');"><?php
                                if ($group_id == 1) {
                                    echo '<i class="icon-trash"></i>';
                                } elseif ((strpos($mystring, 'd' . $module_id)) == true) {
                                    echo '<i class="icon-trash"></i>';
                                }
                                ?></a>

                        </td>
                    </tr> 

                <?php } ?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->

<link href="<?php echo base_url(); ?>plugins/jquery-ui-1.11.4.custom/jquery.ui.css" rel="stylesheet" type="text/css" />  

<script type="text/javascript" src="<?php echo base_url(); ?>plugins/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script>

                                $(function () {
                                    var base_url = '<?php echo base_url(); ?>';
                                    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                                        var term = this.term.split(' ').join('|');
                                        var re = new RegExp("(" + term + ")", "gi");
                                        var t = item.label.replace(re, "<strong>$1</strong>");
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + t + "</a>")
                                                .appendTo(ul);
                                    };
                                    $(document).find("#kharid_aadesh_no_id").autocomplete({
                                        source: base_url + "admin/kharid_aadesh/get_aadesh_no" // path to the get_o method
                                    });
//                        $(document).find(".budget_title_name").autocomplete({
//                            source: base_url+"admin/budget/get_budget_no" // path to the get_o method
//                        });
                                });
</script>
<script>
//         $(document).on("click", "#pratibedan_faram_id", function(e) {
//    var aadesh_id = $("#kharid_aadesh_no_id").val();
//                var base_url = '<?php echo base_url(); ?>'
//                $.ajax({
//                    url: base_url + "admin/kharid_bibaran/get_pratibedan_faram",
//                    type: 'POST',
//                    data: "aadesh_id=" + aadesh_id,
//                    success: function(result) {
//                        
//                    }
//                });
//            });
</script>
