<html>
    <head>
        <style>
            table, th, td {
                border: 1px solid black;
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                                });


        </script>
    </head>
    <body>
       

       

<?php
    echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open('admin/kharid_aadesh/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>
            <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
                    <thead> 
                        <tr> 
                            <th class="checkbox-column" rowspan="2">S.No.</th> 
                            <th data-class="expand" rowspan="2">Budget Title No</th>  
                            <th data-class="expand" rowspan="2">Item Description</th>
                            <th data-class="expand" rowspan="2">Specification</th>
                            <th data-class="expand" rowspan="2">Quantity</th>
                            <th data-class="expand" rowspan="2">Unit</th>
                            <th data-class="expand" colspan="3" style="text-align:center;">Price</th> 
                            <th data-class="expand" rowspan="2">Manage</th>          
                        </tr>
                        <tr style="border-bottom: 1px solid black;">                     
                            <th data-class="expand">Rate</th>
                            <th data-class="expand">Total</th>
                            <th data-class="expand">Remarks</th>
                        </tr>
                    </thead>       

                    <?php $sno = 1; ?>
                    <tbody class="addbody" data-level="0">
                        <tr> 
                            <td class="checkbox-column"><?php echo $sno;
                    $sno++; ?></td>  
                            <td><?php $option = $budget_array;
                    echo form_dropdown('budget-title-name[0]', $option, 0, 'class="form-control budget_title_name" id="bud_drp"');
                    ?></td>
                            <td><?php echo form_input('itm-description-name[0]', '', 'class="form-control required itm_description_name persist" id="itm-drp"'); ?></td>
                            <td><?php echo form_input('itm-specification-name[0]', '', 'class="form-control itm_specification_name required persist" id="itm-spec[0]"'); ?></td>
                            <td><?php echo form_input('txt-quantity-name[0]', '', 'class="form-control txt_quantity_name required persist" id="txt-quantity"'); ?></td>
                            <td><?php echo form_input('txt-unit-name[0]', '', 'class="form-control txt_unit_name required persist" id="txt-unit"'); ?></td>
                            <td><?php echo form_input('txt-rate-name[0]', '', 'class="form-control txt_rate_name required persist" id="txt-rate"'); ?></td>
                            <td><?php echo form_input('txt-total-name[0]', '', 'class="form-control txt_total_name required persist" id="txt-total"'); ?></td>
                            <td><?php echo form_input('txt-remarks-name[0]', '', 'class="form-control txt_remarks_name required persist" id="txt-remarks[0]"'); ?></td>
                            <td class="edit">
                                <input type='button' id="btn-add-new1" class="btn_add_new" value="Add New">                   
            </td>
                        </tr> 
                    </tbody>
                </table> 
            
            <div class="col-md-2 col-md-offset-5" style="margin-bottom: 15px; margin-top:15px;"> 
            <?php
            echo form_submit('submit', 'नयाँ कर्मचारी थप्नुहोस्', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
            if (!empty($update_id)) {
                echo form_hidden('update_id', $update_id);
            }
            ?>
        </div>
        
        <script>

                    $('tbody').find('#txt-rate').blur(function() {
                    var quantity = $('#txt-quantity').val();
                    var rate = $('#txt-rate').val();
                    var result=quantity * rate;
                    if (quantity == "") {
                        alert("Enter Quantity")
                    }
                    else if (rate == "") {
                        alert("Enter Rate")
                    } else {
                        var myResult = quantity * rate;
                       $('#txt-total').val(myResult);
                    }
                });
                
                $(document).on("blur", ".txt_rate_name", function(e) {
//                    $('tbody').find('.txt_rate_name').blur(function() {
                    var parent_id = $(e.target).closest('tbody').attr('id');            
                    var quantity = $("#" + parent_id).find('.txt-quantity_name').val();
                    var rate = $("#"+parent_id).find('.txt_rate_name').val();
                    if (quantity == "") {
                        alert("Enter Quantity")
                    }
                    else if (rate == "") {
                        alert("Enter Rate")
                    } else {
                        var myResult = quantity * rate;
                       $("#"+parent_id).find('.txt_total_name').val(myResult);
                    }

                });
               
                $(document).on("click", ".btn_add_new", function() {
                    $(this).attr('disabled','disabled');
                    var parent = $(this).closest("tbody");
                    var parentlevel = parent.data('level');
                    var name = parseInt(parentlevel) + 1;
                   var sno=name + 1;
            var html= '<tbody class="addbody" data-level='+name+' id=addbody' + name + '\
        <tr>\
         <td class="checkbox-column">'+sno+'</td>\
        <td><select class="form-control budget_title_name" name="budget-title-name[' + name + ']"><?php foreach ($budget_array as $key => $value) { ?><option value = "<?php echo $key; ?>"><?php echo $value; ?></option><?php } ?></select></td>\n\
        <td><input type="text" class="form-control required itm_description_name persist" name="itm-description-name[' + name + ']" id="itm-drp[' + name + ']"></td>\
        <td><input type="text" class="form-control required persist" name="itm-specification-name[' + name + ']" id="itm-spec[' + name + ']"></td>\
        <td><input type="text" class="form-control required txt-quantity_name persist" name="txt-quantity-name[' + name + ']" id="txt-quantity[' + name + ']"></td>\
        <td><input type="text" class="form-control required txt_unit_name persist" name="txt-unit-name[' + name + ']" id="txt-unit[' + name + ']"></td>\
        <td><input type="text" class="form-control txt_rate_name required persist" name="txt-rate-name[' + name + ']"  id="txt-rate[' + name + ']"></td>\
        <td><input type="text" class="form-control required txt_total_name persist" name="txt-total-name[' + name + ']" id="txt-total[' + name + ']"></td>\
        <td><input type="text" class="form-control required persist" name="txt-remarks-name[' + name + ']" id="txt-remarks[' + name + ']"></td>\
        <td class="edit"><input type="button" value="Add New" class="btn_add_new" id="btn-add-new['+name+']"> </td></tr>\n\
        </tbody>';
         $('.addbody').data('level', name);

        var position = $(this).closest('tbody');
        $(position).after(html);
            
          $.ui.autocomplete.prototype._renderItem = function( ul, item){
            var term = this.term.split(' ').join('|');
            var re = new RegExp("(" + term + ")", "gi") ;
                var t = item.label.replace(re,"<strong>$1</strong>");
                return $( "<li></li>" )
            .data("item.autocomplete", item)
        .append( "<a>" + t + "</a>" )
        .appendTo( ul );
    };
    $(document).find(".itm_description_name").autocomplete({ 
    source:"item/get_items" // path to the get_o method
        });
                                    });
                                    
                       
                                    
                                    
                       $('tbody').find('#itm-drp').blur(function() {
                    var item_id = $('#itm-drp').val();
                    var base_url = '<?php echo base_url(); ?>'
                    $.ajax({
                        url: base_url + "admin/kharid_bibaran/findUnit",
                        type: 'POST',
                        data: "item_id=" + item_id,
                        success: function(result) {
                    $('#txt-unit').val(result);
                        }
                    });
                });
                   
                   $(document).on("blur", ".itm_description_name", function(e) {
                 var parent_id = $(e.target).closest('tbody').attr('id');
                 var item_id = $("#"+parent_id).find('.itm_description_name').val();
                    var base_url = '<?php echo base_url(); ?>'
                    $.ajax({
                        url: base_url + "admin/kharid_bibaran/findUnit",
                        type: 'POST',
                        data: "item_id=" + item_id,
                        success: function(result) {
                            $("#"+parent_id).find('.txt_unit_name').val(result);
                        }
                    });
                });
                </script>
                 <link href="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery.ui.css" rel="stylesheet" type="text/css" />  
  
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>

<script>
    $(function(){
                $.ui.autocomplete.prototype._renderItem = function( ul, item){
  var term = this.term.split(' ').join('|');
  var re = new RegExp("(" + term + ")", "gi") ;
  var t = item.label.replace(re,"<strong>$1</strong>");
  return $( "<li></li>" )
     .data("item.autocomplete", item)
     .append( "<a>" + t + "</a>" )
     .appendTo( ul );
    };
    $(document).find(".itm_description_name").autocomplete({ 
    source:"item/get_items" // path to the get_o method
  });
 });
    </script>
    <script>
    $(".persist").change(function(){
        alert('change called');
        var test=$(this).attr('name');
        console.log($(this).val());
        console.log(test);
        var data;
        try{
          data = JSON.parse(localStorage['inventory']);  
        }catch(e){
           data = {}; 
        }
        var activityIndex=0;
        if(!data[activityIndex])
            data[activityIndex]={};
        data[activityIndex][$(this).attr('name')]=$(this).val();
        data[activityIndex][$('.txt_unit_name').attr('name')]=$(.txt_unit_name).val();
        
        localStorage.setItem('inventory',JSON.stringify(data));
        
    });
    </script>
    </body>
</html>