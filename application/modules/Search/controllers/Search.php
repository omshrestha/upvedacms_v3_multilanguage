<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function index(){
            //var_dump($_POST); die;

            if(!isset($_POST['key'])){
                $searchkey = '  ';
            }
            else if(($_POST['key'])==null){
                $searchkey = '  ';
            }
            elseif(($_POST['key'])==' '){                
                $searchkey = '  ';
            }else{
            $searchkey = $_POST['key'];
            }
            $data['query_search_pages'] = $this->search_pages($searchkey);
            $data['query_search_modules'] = $this->search_modules($searchkey);
            $data['searchkey'] = $searchkey;
            $this->load->module('template');
            $this->template->front($data);
	}

	function search_pages($key){            
            $this->load->model('mdl_search');       
            $query = $this->mdl_search->search_pages($key);
            return $query;
	}
        function search_modules($key){
            $this->load->model('mdl_search');
            $query = $this->mdl_search->search_modules($key); 
            return $query;
	}
        
	function get_pages_if_live($col, $value) {
	$this->load->model('mdl_pages');
	$query = $this->mdl_pages->get_pages_if_live($col, $value);
        
	return $query;
	}
        function list_related_pages($search_keys, $title)
        {
          //echo $search_keys;
            $words = explode(',',$search_keys);
            $pages = $this->get_related_pages();
            $related_pages=array();
            foreach ($pages->result() as $key) 
                {
                    foreach($words as $row) 
                   { if($key->title != $title)
                   {
                      
                       if($row!=NULL)
                       {
                       if (strpos(strtolower(str_replace(' ', '', $key->search_keys)),strtolower(str_replace(' ', '', $row))) !== false) {
                           $related_pages[strtolower(url_title($key->title))]=$key->title;
                          break;
                       }
                   }}
                   }
                }
//                print_r($related_pages); die;
           return $related_pages;
        }

	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_pages');
	$query = $this->mdl_pages->get_where_custom($col, $value);
	return $query;
	}
        
        function get_related_pages()
        {
            
          $this->load->model('mdl_pages');
	$query = $this->mdl_pages->get_related_pages();
	return $query;
        }

}