<?php
if (!isset($message)) {
    $message = '';
} else {
    $message = $message;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php if (isset($title)) {
            ?>
            <title><?php echo $title; ?></title>
            <meta name="title" content="<?php echo $title; ?>">
            <meta name="keywords" content="<?php echo $search_keys; ?>">
            <meta name="description" content="<?php echo $meta_description; ?>">
        <?php } ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>uploads/settings/<?php echo $site_settings['favicon']; ?>">

        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/main_front.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/responsive-slider.css">
        <link href="<?php echo base_url(); ?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>  
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/admin/css/fontawesome/font-awesome.min.css">
        <!-- start of elastislide slider -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>design/frontend/css/elastislide.css">

        <!-- end of elastislide slider -->

        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="fb-root">        
        </div>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <script>
            window.twttr = (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);

                t._e = [];
                t.ready = function(f) {
                    t._e.push(f);
                };

                return t;
            }(document, "script", "twitter-wjs"));
        </script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>


        <div class="container">
            <?php
            require('front_header.php');
            ?><!--header part ------------------------------------------------------------>

            <div class="clearfix"></div>
            <!--navigation bar------------------------------------------------------------>
            <?php
            require('front_navigation.php');
            ?>
            <!--banner-------------------------------------------------------------------->
            <?php
            $first_bit = $this->uri->segment(1);
            $second_bit = $this->uri->segment(2);
            //echo $first_bit;
            //echo $second_bit;


            if ($first_bit == "" || $first_bit == "home") {
                ?>


                <div class="clearfix"></div>
                <!--============================================start of responsive slider============================-->

                <div class="col-lg-12" style="margin-bottom:10px;">
                    <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                        <div class="slides" data-group="slides">
                            <ul>
                                <?php foreach ($banner as $row) { ?> 
                                    <li>
                                        <div class="col-lg-4 content_home responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                            <div class="slide-body" data-group="slide">
                                                <h2><strong><?php echo ucwords($row['title']) ?></strong></h2>
                                                <p> <?php // echo word_limiter($row['sub_title'], 90); ?></p>
                                                <a href="<?php echo base_url() ?>banner/index/<?php echo $row['slug']; ?>" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                            <div class="slide-body" data-group="slide">
                                                <img src="<?php echo base_url(); ?>uploads/banner/<?php echo $row['attachment'] ?>" style="width:1140px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                </div>

                <!--==============================================end of responsive slider============================-->   

                <!-- start of multiple slider portion -->
                <div class="featured_and_multiple_slider">
                    <div class="col-lg-4">
                        <div class="featured">
                            <div class="head">
                                <h2><strong>About Us</strong></h2>
                            </div>
                            <div class="details">
                                <?php $i = 1; ?>
                                <?php foreach ($about_us as $row) { ?> 
                                    <div class="featured_details">

                                        <h5><strong><?php echo ucwords($row['title']); ?></strong></h5>
                                        <div class="individual_destination" style="text-align:justify;"> 
                                            <img src="<?php echo base_url(); ?>uploads/about_us<?php echo $i++; ?>.jpg" width="90px" style="float:left; padding:10px;">
                                            <?php echo word_limiter($row['description'], 50); ?>
<!--                                            <p><a href="<?php echo base_url() ?>destinations/visit/<?php echo $row['slug']; ?>" style="float:right; color:#06F; margin-right:12px;">Read More...</a></p>-->
                                        </div>
                                    </div>
                                    <!--                                    <div class="hrline"></div>-->
                                <?php } ?>
                                  
                            </div>
                        </div>
                        <div class="media local-stories-pad">
                                 <?php foreach($local_stories as $row){ ?>
                                <div class="col-md-12">
                                    
                                    <div class="col-md-6 col-sm-6">
                                <div class="media-left">
                                    <center>
                                        <img src="<?php echo base_url();?>uploads/local_stories/<?php echo $row['attachment'];?>" style=" border: 4px solid #C7B0B0;" class="img-responsive">
                                    </center>
                                </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                <div class="media-body local-stories">
                                    <h2><?php echo $row['title'];?></h2>
                                    <p><?php echo $row['description'];?></p>
                                </div>
                                </div>
                                    </div>
                                    <div>
                                        <div style="height: 20px; overflow: hidden; width: 100%;"></div>
                                    <hr class="styled-hr" style="width:100%;">
                                    <div style="height: 20px; overflow: hidden; width: 100%;"></div>
                                </div> 
                                      <?php }?>
                                
                                </div>
                    </div>
                    
                    
                </div>

               
                <?php
            } else {
                ?>
                
                <!-- start of the body content ------------------------------------------>

                <?php
                if ($this->uri->segment(1) == 'sub_pages') {
                    require ('sub_pages.php');
                } else if ($this->uri->segment(1) == 'custom_private_trips') {
                    require 'custom_private_trips_form.php';
                } else if ($this->uri->segment(1) == 'booknow_form') {
                    require 'contact_us_form.php';
                } else if ($this->uri->segment(1) == 'banner') {
                    require 'banner.php';
                } else if ($this->uri->segment(1) == 'inquiry') {
                    require 'inquiry_form.php';
                } else if ($this->uri->segment(1) == 'destinations') {
                    require 'destinations.php';
                } else if ($this->uri->segment(1) == 'testimonails') {
                    require 'testimonials.php';
                } else if ($this->uri->segment(1) == 'news') {
                    require 'news.php';
                } else if ($this->uri->segment(1) == 'contactus') {
                    require 'contactus.php';
                }
                 else if ($this->uri->segment(1) == 'tours') {
                    require 'tours.php';
                 }
                 else if ($this->uri->segment(1) == 'treks') {
                    require 'treks.php';
                 }
//                 else if ($this->uri->segment(1) == 'our-team') {
//                    require 'our-team.php';
//                 }
                 else {
                    require ('front_mainbody.php');
                }
                ?>

            <?php } ?>
            <!--------------end-content---------------------------------------------------------------------------->
            <!--Start of footer-->
            <?php
            require('front_footer.php');
            ?><!--end of footer -->

        </div><!--end of container-->
        <script src="<?php echo base_url(); ?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url(); ?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url(); ?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url(); ?>design/frontend/js/responsive-slider.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
        <script type="text/javascript">
            $( '#carousel' ).elastislide();
            $( '#carouse2' ).elastislide();
            $( '#carouse3' ).elastislide();
        </script>
        <!-- start of code for clickable dropdown menu bar  -->
        <script>
            jQuery(function($) {
                $('.navbar .dropdown').hover(function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

                }, function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

                });

                $('.navbar .dropdown > a').click(function(){
                    location.href = this.href;
                });

            });
        </script>
        <!-- end of code for clickable dropdown menu bar  -->
        <script>
            (function($) {
                var allPanels = $('.accordion > dd').hide();
                //$('.accordion > dd:first-of-type').show();
                //    $('.accordion > dt:first-of-type').addClass('accordion-active');
                jQuery('.accordion > dt').on('click', function() {
                    $this = $(this);
                    $target = $this.next(); 
                    if(!$this.hasClass('accordion-active')){
                        $this.parent().children('dd').slideUp();

                        jQuery('.accordion > dt').removeClass('accordion-active');
                        $this.addClass('accordion-active');
                        $target.addClass('active').slideDown();

                    }

                    return false;
                });

            })(jQuery);
            $('#botton').click(function()
            {
      
                $('#validate-1').submit();

            });
        </script> 
    </body>
</html>