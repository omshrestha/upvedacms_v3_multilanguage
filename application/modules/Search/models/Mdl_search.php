<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_search extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}
	
	function get_table() {
	$table = "up_pages";
	return $table;
	} 
        function get_mtable() {
	$table = "up_modules";
	return $table;
	}

        function search_modules($key){
            $mtable = $this->get_mtable();
//            $this->db->select('up_modules.*');
            $this->db->like('title', $key);
            
            $query=$this->db->get($mtable);
  
            return $query;            
        }
        
        function search_pages($key){
            $table = 'up_pages';
            $this->db->select('up_pages.*'); 
            $this->db->like('title', $key); 
            $this->db->where('status','live');
            $query=$this->db->get($table);  
            return $query;            
        }
        
}